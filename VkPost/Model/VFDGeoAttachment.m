//
//  VFGeoAttachment.m
//  VK-Feed
//
//  Created by Dmitriy Kluev on 15.07.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "VFDGeoAttachment.h"

@implementation VFDGeoAttachment

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if(self)
    {
        self.type = [dictionary objectForKey:@"type"];
        self.coordinates = [dictionary objectForKey:@"coordinates"];
        
        if ([dictionary objectForKey:@"place"]) {
            NSDictionary *place = [dictionary objectForKey:@"place"] ;
            self.idx =  place[@"id"];
            self.title = place[@"title"];
            self.country = place[@"country"];

#warning ???
            self.city = place[@"city"];
            self.latitude = place[@"city"];
            self.longitude = place[@"city"];
            
        }
    
    }
    
    return self;
}


@end
