//
//  VFDoc.m
//  VK-Feed
//
//  Created by Dmitriy Kluev on 08.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "VFDDocAttachment.h"

@implementation VFDDocAttachment

+ (NSString *)primaryKey {
    return @"idx";
}

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if(self)
    {
        self.idx = [dictionary objectForKey:@"id"];
        self.real_idx = [dictionary objectForKey:@"id"];
        self.owner_id = [dictionary objectForKey:@"owner_id"];
        self.real_owner_id = [dictionary objectForKey:@"owner_id"];
        self.title = [dictionary objectForKey:@"title"];
        self.ext = [dictionary objectForKey:@"ext"];
        self.size = [dictionary objectForKey:@"size"];
        self.url = [dictionary objectForKey:@"url"];
        self.date = [dictionary objectForKey:@"date"];
        self.accessKey = [dictionary objectForKey:@"accessKey"];
        self.file_size = self.size;
        self.autoPlay = NO ;
        self.type = [dictionary objectForKey:@"type"];
        self.attachmentSize = [[VFDAttachmentSize alloc] init];
    
        self.attachmentSize.width = @150 ;
        self.attachmentSize.height = @100 ;
        self.attachmentSize.vFrame = [[VFDRect alloc]init] ;
        
        if ([dictionary objectForKey:@"preview"]) {
            NSDictionary *preview = dictionary[@"preview"] ;
            
            if ([preview objectForKey:@"video"]) {
                self.video_src = preview[@"video"][@"src"] ;
                self.file_size = preview[@"video"][@"file_size"];
            }
            if ([preview objectForKey:@"photo"]) {
                VKPhotoSizes *sizes = [[VKPhotoSizes alloc] initWithArray:preview[@"photo"][@"sizes"]];
                [self setPhotos:sizes];
            }
            
        }
#warning  motherfuck
        if([dictionary objectForKey:@"photo_100"]) {
            
        self.photo_s = [dictionary objectForKey:@"photo_100"];
        }
    
    }
    
    return self;
}


-(void)setPhotos:(VKPhotoSizes *)sizes{
    
    for (int i = 0 ; i < sizes.count ; i++) {
        
        VKPhotoSize *size = sizes[i];
        
        if ([size.type isEqualToString:@"m"]) {
            self.photo_m = size.src ;
        }
        if ([size.type isEqualToString:@"s"]){
            self.photo_s = size.src ;
        }
        if ([size.type isEqualToString:@"x"]){
            self.photo_x = size.src ;
        }
        if ([size.type isEqualToString:@"y"]){
            self.photo_y = size.src ;
        }
        if ([size.type isEqualToString:@"z"]){
            self.photo_z = size.src ;
        }
        if ([size.type isEqualToString:@"o"]){
            self.photo_o = size.src ;
        }
    }
}


- (NSNumber *)width {
    return self.attachmentSize.width;
}

- (NSNumber *)height {
    return self.attachmentSize.height;
}

- (VFDRect*)frame {
    return self.attachmentSize.vFrame;
}

@end
