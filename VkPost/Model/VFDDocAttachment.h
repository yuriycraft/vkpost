//
//  VFDoc.h
//  VK-Feed
//
//  Created by Dmitriy Kluev on 08.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VFDAttachmentSize.h"
#import "VKSDK.h"

static NSInteger kDocumentImageType = 4;
static NSInteger kDocumentGifType = 3;


#warning uncomment
//@interface VFDDocAttachment : RLMObject <VFDAttachmentSizeProtocol>
@interface VFDDocAttachment : NSObject <VFDAttachmentSizeProtocol>
@property VFDAttachmentSize *attachmentSize;

//@property NSNumber<RLMInt> *idx;
//@property NSNumber<RLMInt> *real_idx;
//@property NSNumber<RLMInt> *owner_id;
//@property NSNumber<RLMInt> *real_owner_id;
//@property NSNumber<RLMInt> *date;
@property NSNumber *idx;
@property NSNumber *real_idx;
@property NSNumber *owner_id;
@property NSNumber *real_owner_id;
@property NSNumber *date;

@property NSString *title;
@property NSString *ext;
//@property NSNumber<RLMInt> *size;
@property NSNumber *size;

@property NSString *url;
@property NSString *video_src;
//@property NSNumber<RLMInt> *file_size;
@property NSNumber *file_size;

@property BOOL autoPlay;

@property NSString *photo_m;
@property NSString *photo_s;
@property NSString *photo_x;
@property NSString *photo_y;
@property NSString *photo_z;
@property NSString *photo_o;

@property NSString *accessKey;
@property NSNumber *type;


//@property(nonatomic, strong) VKPhotoSizes *sizes;

-(id)initWithDictionary:(NSDictionary *)dictionary ;

@end
