//
//  YCVKPollModel.h
//  VkPost
//
//  Created by book on 18.01.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YCVKPollModel : NSObject

@property(nonatomic, strong) NSNumber* idx;

@property(nonatomic, assign) NSInteger created;
@property(nonatomic, assign) NSInteger votes;

@property(nonatomic, strong) NSNumber* answer_id;

@property(nonatomic, strong) NSString* question;
@property(nonatomic, assign) BOOL is_anonymous;
@property(nonatomic, strong) NSNumber* owner_id;
@property(nonatomic, strong) NSMutableArray *answersArray;

- (instancetype)initWithDictionary:(NSDictionary*) responseDict;


@end
