//
//  YCVKVideoUploadingModel.h
//  VkPost
//
//  Created by book on 28.01.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface YCVKVideoUploadingModel : NSObject

@property(nonatomic, strong) NSString* name;

@property(nonatomic, strong) NSString *descriptionVideo;

@property(nonatomic, assign) BOOL is_private;

@property(nonatomic, assign) BOOL wallpost;

@property(nonatomic, strong) NSString* link;

@property(nonatomic, strong) NSNumber* group_id;

@property(nonatomic, strong) NSNumber* album_id;

@property(nonatomic, assign) BOOL no_comments;

@property(nonatomic, assign) BOOL repeat;

@property(nonatomic, strong) NSNumber* video_id;

@property(nonatomic, strong) NSNumber* owner_id;

@property(nonatomic, assign) NSTimeInterval duration;


- (void)initUploadingInfoWithDictionary:(NSDictionary*) responseDict;

@end
