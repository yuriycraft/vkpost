//
//  VFDAttachmentSize.m
//  VK-Feed
//
//  Created by Dmitriy Kluev on 26.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "VFDAttachmentSize.h"

@implementation VFDAttachmentSize

@end

@implementation VFDRect

+ (VFDRect *)rectWithCGRect:(CGRect)frame {
    VFDRect *rect = [[VFDRect alloc] init];
    rect.x = frame.origin.x;
    rect.y = frame.origin.y;
    rect.width = frame.size.width;
    rect.height = frame.size.height;
    
    return rect;
}

- (CGRect)CGRect {
    return CGRectMake(self.x, self.y, self.width, self.height);
}

@end
