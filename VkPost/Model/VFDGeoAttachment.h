//
//  VFGeoAttachment.h
//  VK-Feed
//
//  Created by Dmitriy Kluev on 15.07.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>

#warning uncomment
@interface VFDGeoAttachment : NSObject//RLMObject

//@property NSNumber<RLMInt> *idx;
@property NSNumber *idx;
@property NSString *coordinates;
@property NSString *type;

@property NSString *title;
@property NSString *country;
@property NSString *city;

//@property NSNumber<RLMFloat> *latitude;
//@property NSNumber<RLMFloat> *longitude;
@property NSNumber*latitude;
@property NSNumber*longitude;




-(id)initWithDictionary:(NSDictionary *)dictionary ;

@end
