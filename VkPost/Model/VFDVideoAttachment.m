//
//  VFVideo.m
//  VK-Feed
//
//  Created by Dmitriy Kluev on 08.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "VFDVideoAttachment.h"

#warning uncomment
//#import "VFDLikesCounters.h"

@implementation VFDVideoAttachment


-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if(self)
    {
        
        self.idx = [dictionary objectForKey:@"id"];
        self.owner_id = [dictionary objectForKey:@"owner_id"];
        self.title = [dictionary objectForKey:@"title"];
        self.duration = [dictionary objectForKey:@"duration"];
        self.descr = [dictionary objectForKey:@"descr"];
        self.date = [dictionary objectForKey:@"date"];
        self.views = [dictionary objectForKey:@"views"];
        self.comments = [dictionary objectForKey:@"comments"];
        self.photo_130 = [dictionary objectForKey:@"photo_130"];
        self.photo_320 = [dictionary objectForKey:@"photo_320"];
        self.photo_640 = [dictionary objectForKey:@"photo_640"];
        self.photo_800 = [dictionary objectForKey:@"photo_800"];
        self.access_key = [dictionary objectForKey:@"access_key"];
        self.can_edit = [[dictionary objectForKey:@"can_edit"] boolValue] ;
        self.can_add = [[dictionary objectForKey:@"can_add"] boolValue] ;
        self.player = [dictionary objectForKey:@"player"];
        
        self.attachmentSize = [[VFDAttachmentSize alloc] init];
        
        self.attachmentSize.width = @200 ;
        self.attachmentSize.height = @112 ;
        self.attachmentSize.vFrame = [[VFDRect alloc]init] ;

//        if ([dictionary objectForKey:@"likes"]) {
//            self.likes = [[VFDLikesCounters alloc] initWithDictionary:[dictionary objectForKey:@"likes"]];
//        }
        
    }
    
    return self;
}

- (NSNumber *)width {
    return self.attachmentSize.width;
}

- (NSNumber *)height {
    return self.attachmentSize.height;
}

- (VFDRect*)frame {
    return self.attachmentSize.vFrame;
}


@end
