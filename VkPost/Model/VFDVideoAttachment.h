//
//  VFVideo.h
//  VK-Feed
//
//  Created by Dmitriy Kluev on 08.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VFDAttachmentSize.h"
#warning uncomment
//#import "VFDLikesCounters.h"

@interface VFDVideoAttachment : NSObject <VFDAttachmentSizeProtocol> //RLMObject <VFDAttachmentSizeProtocol>

@property VFDAttachmentSize *attachmentSize;

@property NSNumber *idx ;//<RLMInt> *idx ;
@property NSNumber *owner_id ;//<RLMInt> *owner_id ;
@property NSString *title ;
@property NSNumber *duration ;//<RLMInt> *duration ;
@property NSString *descr ;
@property NSNumber *date ;//<RLMInt> *date ;
@property NSNumber *views ;//<RLMInt> *views ;
@property NSNumber *comments;//<RLMInt> *comments;
@property NSString *photo_130;
@property NSString *photo_320;
@property NSString *photo_640;
@property NSString *photo_800;
@property NSString *access_key;
@property BOOL can_edit;
@property BOOL can_add ;
@property NSString *player;

//@property VFDLikesCounters *likes ;

-(id)initWithDictionary:(NSDictionary *)dictionary ;

@end
