//
//  YCVKUploadingAttachment.m
//  VkPost
//
//  Created by book on 25.09.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "YCVKUploadingAttachment.h"
#import "VFPhotoAttachment.h"
#import "VFDVKService+Photo.h"
#import "VFDVKService+UploadFiles.h"
#import "YCVKPollModel.h"
#import "YCVKVideoUploadingModel.h"
#import "VFDDocAttachment.h"
#import "VFDGeoAttachment.h"
#import "VFDVideoAttachment.h"



static const CGFloat kAttachmentsViewSizeYC = 100.0f;

@implementation YCVKUploadingAttachment


- (void)initWithAnotherAttachments:(id) anotherObj withPreviewImage:(UIImage*)previewImage andExtensionString:(NSString*)extension   andUploadingData:(NSData*)uploadingData {
    
    if([anotherObj isKindOfClass:[VFPhotoAttachment class]]) {
        
        [self initAttachmentWithVFPhotoAttachment:anotherObj];
    }
    
    else if([anotherObj isKindOfClass:[VKUploadImage class]]) {
        
        [self initAttachmentWithVKUploadImage:anotherObj];
        
    }
    else if([anotherObj isKindOfClass:[YCVKPollModel class]]) {
        
        [self initAttachmentWithYCVKPollModel:anotherObj];
        
    }
    else if([anotherObj isKindOfClass:[YCVKVideoUploadingModel class]]) {
        
        [self initAttachmentWithYCVKVideoModel:anotherObj andPreviewImage:previewImage andUploadData:uploadingData andExtention:extension];
        
    }
    else if([anotherObj isKindOfClass:[VFDDocAttachment class]]) {
        
        [self initAttachmentWithVFDDocAttachment:anotherObj andUploadData:uploadingData andPreviewImage:previewImage] ;
        
        
    } else if([anotherObj isKindOfClass:[VFDGeoAttachment class]]){
        
        [self initAttachmentWithVFDGeoAttachment:anotherObj andPreviewImage:previewImage];
        
    } else if([anotherObj isKindOfClass:[VFDVideoAttachment class]]){
        
        [self initAttachmentWithVFDVideoAttachment:anotherObj];
    }
    
    dispatch_async( dispatch_get_main_queue(), ^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompleteCreateAttachDone object:self];
    });
    
}

- (void)initUploadingInfoWithDictionary:(id) responseDict {
    
    self.upload_url = [responseDict objectForKey:@"upload_url"];
    
   // NSLog(@"%@",responseDict);
    
    if(self.type == kVideoType && self.video) {
        
        [self.video initUploadingInfoWithDictionary:responseDict];
        
        self.attachmentString =  [NSString stringWithFormat:@"video%@_%@", self.video.owner_id, self.video.video_id];
        
    }
}


-(void)initAttachmentWithVFDVideoAttachment:(VFDVideoAttachment *) videoVK

{
    self.uid = [self uuid];
    
    self.type = kVideoVKType;
    
    self.attachSize =
    
    CGSizeMake(kAttachmentsViewSizeYC, kAttachmentsViewSizeYC);
    
    self.attachmentString =  [NSString stringWithFormat:@"video%@_%@", videoVK.owner_id, videoVK.idx];
    
    NSURL *url = [NSURL URLWithString:videoVK.photo_130];
    
    self.status = kDoneUploading;
    
    if(!self.preview) {
        
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            [VKSERVICE getPhotoWithUrl:url
                            completion:^(UIImage *image, NSError *error) {
                                
                                if(image) {
                                    
                                    image =
                                    [image vks_roundCornersImage:0
                                                      resultSize:CGSizeMake(kAttachmentsViewSizeYC,
                                                                            kAttachmentsViewSizeYC)];
                                    
                                    self.preview = image;
                                    
                                    [[NSNotificationCenter defaultCenter] postNotificationName:PHOTO_LOADING_DID_END_NOTIFICATION
                                                                                        object:self];
                                }
                                
                            }];
        });
    }
    
}


-(void)initAttachmentWithVFPhotoAttachment:(VFPhotoAttachment *) photo

{
    self.uid = [self uuid];
    
    self.type = kPhotoVkType;
    
    self.attachSize =
    
    CGSizeMake(kAttachmentsViewSizeYC, kAttachmentsViewSizeYC);
    
    self.attachmentString =  [NSString stringWithFormat:@"photo%@_%@", photo.owner_id, photo.idx];
    
    NSURL *url = [NSURL URLWithString:photo.photo_130];
    
    self.status = kDoneUploading;
    
    if(!self.preview){
        
        [VKSERVICE getPhotoWithUrl:url
         
                        completion:^(UIImage *image, NSError *error) {
                            
                            if(image) {
                                
                                image =
                                [image vks_roundCornersImage:0
                                                  resultSize:CGSizeMake(kAttachmentsViewSizeYC,
                                                                        kAttachmentsViewSizeYC)];
                                
                                
                                self.preview = image;
                                
                                [[NSNotificationCenter defaultCenter] postNotificationName:PHOTO_LOADING_DID_END_NOTIFICATION
                                                                                    object:self];
                            }
                            
                        }];
    }
    
}

- (void)initAttachmentWithVKUploadImage:(VKUploadImage *)uploadImage {
    
    self.uid = [self uuid];
    
    self.type = kPhotoType;
    
    CGSize size = uploadImage.sourceImage.size;
    
    size = CGSizeMake(MAX(floorf(size.width * kAttachmentsViewSizeYC / size.height), 50.f),
                      kAttachmentsViewSizeYC);
    
    self.attachSize = size;
    
    self.preview =
    [uploadImage.sourceImage vks_roundCornersImage:0.0f resultSize:size];
    
    self.uploadingImage = uploadImage;
    
    
    
    self.status = kUploading;
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        self.uploadingRequest = [VKSERVICE uploadRequestPhotoOrPollWithAttach:self];
    });
}

- (void)initAttachmentWithVFDDocAttachment:(VFDDocAttachment *)document andUploadData:(NSData*)uploadData andPreviewImage:(UIImage*)previewImage {
    
    self.uid = [self uuid];
    
    self.type = kDocumentType;
    
    self.document = document;
    
    if(uploadData) {
        
        self.attachSize =  CGSizeMake(kAttachmentsViewSizeYC, kAttachmentsViewSizeYC);;
        
        self.uploadData = uploadData;
        
        self.preview = previewImage;
        
       // self.status = kUploading;
        
        self.ext = document.ext;
          dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [VKSERVICE  getServerAdressForVideoOrDocumentUploadWithAttach:self];
          });
    } else {
        
        CGSizeMake(kAttachmentsViewSizeYC, kAttachmentsViewSizeYC);
        
        self.attachmentString =  [NSString stringWithFormat:@"doc%@_%@", document.owner_id, document.idx];
        
        NSURL *url = [NSURL URLWithString:document.photo_s];
        
        self.status = kDoneUploading;
        if([document.type integerValue]== kDocumentImageType ||[document.type integerValue] == kDocumentGifType ){
            
            if(!self.preview) {
                
                dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    
                    [VKSERVICE getPhotoWithUrl:url
                                    completion:^(UIImage *image, NSError *error) {
                                        
                                        if(image) {
                                            
                                            image =
                                            [image vks_roundCornersImage:0
                                                              resultSize:CGSizeMake(kAttachmentsViewSizeYC,
                                                                                    kAttachmentsViewSizeYC)];
                                            
                                            
                                            
                                            self.status = kDoneUploading;
                                            
                                            self.preview = image;
                                            
                                            dispatch_async( dispatch_get_main_queue(), ^{
                                                
                                                [[NSNotificationCenter defaultCenter] postNotificationName:PHOTO_LOADING_DID_END_NOTIFICATION
                                                                                                    object:self];
                                            });
                                        }
                                        
                                    }];
                });
            }
        }
    }
}


- (void)initAttachmentWithYCVKPollModel:(YCVKPollModel*) pollObj {
    
    self.uid = [self uuid];
    
    self.type = kPollType;
    
    self.poll = pollObj;
    
    self.status = kUploading;
    
    self.uploadingRequest = [VKSERVICE uploadRequestPhotoOrPollWithAttach:self];
    
}

- (void)initAttachmentWithVFDGeoAttachment:(VFDGeoAttachment*) geoObj andPreviewImage:(nullable UIImage*)previewImage {
    
    self.uid = [self uuid];
    
    self.type = kGeoType;
    
    self.geo = geoObj;
    
    self.preview = previewImage;
    
    self.status = kDoneUploading;
    
}

- (void)initAttachmentWithYCVKVideoModel:(YCVKVideoUploadingModel*) videoObj andPreviewImage:(nullable UIImage*)previewImage andUploadData:(nullable NSData*)uploadData andExtention:(NSString*)extension{
    
    self.attachSize = CGSizeMake(kAttachmentsViewSizeYC, kAttachmentsViewSizeYC);
    
    self.video = videoObj;
    
    self.uploadData = uploadData;
    
    self.preview = previewImage;
    
    self.uid = [self uuid];
    
    self.type = kVideoType;
    
    self.ext = extension;
    
    [VKSERVICE getServerAdressForVideoOrDocumentUploadWithAttach:self];
    
}

-(NSInteger)uuid
{
    static NSString* lastID = @"lastID";
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger identifier = [defaults integerForKey:lastID] + 1;
    
    [defaults setInteger:identifier forKey:lastID];
    
    [defaults synchronize];
    
    return identifier;
}


- (NSString *)mimeType {
    
    NSLog(@"EXT = %@",self.ext);
    
    if([self.ext isEqualToString:@"GIF"]) {
        
        return @"image/gif";
    }
    else  if([self.ext isEqualToString:@"JPEG"] || [self.ext isEqualToString:@"JPG"]) {
        
        return @"image/jpeg";
    }
    else    if([self.ext isEqualToString:@"PNG"] || [self.ext isEqualToString:@"png"]) {
        
        return @"image/png";
    }
    else if([self.ext isEqualToString:@"MOV"] || [self.ext isEqualToString:@"mov"]) {
        
        return @"video/quicktime";
    }
    else  if([self.ext isEqualToString:@"MP4"] || [self.ext isEqualToString:@"mp4"]) {
        
        return @"video/mp4";
    }
    return @"";
}



@end
