//
//  YCVKPollModel.m
//  VkPost
//
//  Created by book on 18.01.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "YCVKPollModel.h"
#import "VKResponse.h"

@implementation YCVKPollModel

- (instancetype)initWithDictionary:(NSDictionary*) responseDict
{
    self = [super init];
    if (self) {
        
        if(responseDict) {
            
            self.idx = [responseDict objectForKey:@"id"];
            
            NSNumber *createdNumber = [responseDict objectForKey:@"created"];
            
            self.created = [createdNumber integerValue];
              NSNumber *votesNumber = [responseDict objectForKey:@"votes"];
            
            self.votes = [votesNumber integerValue];
            
            self.answer_id = [responseDict objectForKey:@"answer_id"];
            
            self.question = [responseDict objectForKey:@"question"];
            
             NSNumber *anonymousNumber = [responseDict objectForKey:@"anonymous"];
            
            self.is_anonymous = [anonymousNumber boolValue];
            
            self.owner_id = [responseDict objectForKey:@"owner_id"];
            
            self.answersArray = [responseDict objectForKey:@"answers"];
        }
    }
    return self;
}
@end
