//
//  YCVKVideoUploadingModel.m
//  VkPost
//
//  Created by book on 28.01.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "YCVKVideoUploadingModel.h"

@implementation YCVKVideoUploadingModel

- (void)initUploadingInfoWithDictionary:(NSDictionary*) responseDict
{
    
    if(responseDict) {
        
        self.owner_id = [responseDict objectForKey:@"owner_id"];
        
        self.video_id = [responseDict objectForKey:@"video_id"];
        
    }
    
}

@end
