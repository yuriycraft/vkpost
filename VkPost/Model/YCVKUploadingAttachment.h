//
//  YCVKUploadingAttachment.h
//  VkPost
//
//  Created by book on 25.09.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "VKObject.h"
#import "VKSdk.h"
#import <Foundation/Foundation.h>

typedef enum AttachmentStatus : NSUInteger {
    
    kUploading      = 1,
    kDoneUploading  = 2,
    kErrorUploading = 3
    
} AttachmentStatus;


typedef enum AttachmentType : NSUInteger {
    
    kPhotoVkType =    1,// Фотография из альбома (type = photo)
    
    kPhotoType   =    2,// Фотография, загруженная из галереи (type = posted_photo)
    
    kVideoType   =    3,// Видеозапись (type = video)
    
    kVideoVKType =    4,// Видео из альбома вк

    kDocumentType =   5,// Документ (type = doc)
    
    kDocumentVkType = 6,// Документ VK (type = doc)

    kPollType =       7,// Опрос (type = poll)
    
    kGeoType =        8 //  Местоположение
    
} AttachmentType;

// Notifications
#define ATTACHMENT_LOADING_DID_END_NOTIFICATION @"ATTACHMENT_LOADING_DID_END_NOTIFICATION"
#define ATTACHMENT_PROGRESS_NOTIFICATION @"ATTACHMENT_PROGRESS_NOTIFICATION"
#define ATTACHMENT_UPLOAD_ERROR_NOTIFICATION @"ATTACHMENT_UPLOAD_ERROR_NOTIFICATION"

static NSString * kCompleteCreateAttachDone = @"completeAttachDone";

@class VFPhotoAttachment,YCVKPollModel,YCVKVideoUploadingModel,VFDDocAttachment,VFDGeoAttachment;

@interface YCVKUploadingAttachment : VKObject

@property(nonatomic, assign) NSInteger uid;
@property(nonatomic, assign) NSUInteger status;
@property(nonatomic, assign) NSUInteger type;
@property(nonatomic, assign) CGSize attachSize;
@property(nonatomic, strong) NSString *attachmentString;
@property(nonatomic, strong) UIImage *preview;
@property(nonatomic, weak)   id uploadingRequest;
@property(assign, nonatomic) CGFloat progressUploading;
@property(nonatomic, strong) VKUploadImage * uploadingImage;
@property(nonatomic, strong) YCVKPollModel* poll;
@property(nonatomic, strong) YCVKVideoUploadingModel* video;
@property(nonatomic, strong) VFDDocAttachment* document;
@property(nonatomic, strong) VFDGeoAttachment* geo;
@property(nonatomic, strong) NSString* upload_url;
@property(nonatomic, strong) NSString *ext;
@property(nonatomic, strong) UIImage *previewImage;
@property(nonatomic, strong) NSData *uploadData;
@property(nonatomic, strong) VKUploadImage *uploadImage;


- (NSString *)mimeType;

- (void)initUploadingInfoWithDictionary:(id) responseDict;

- (void)initWithAnotherAttachments:(id) anotherObj
                  withPreviewImage:(UIImage*)previewImage
                andExtensionString:(NSString*)extension
                  andUploadingData:(NSData*)uploadingData;
@end



