//
//  VFDAttachmentSize.h
//  VK-Feed
//
//  Created by Dmitriy Kluev on 26.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGGeometry.h>
#warning uncomment
//#import <Realm/Realm.h>

@interface VFDRect : NSObject//RLMObject
@property NSInteger width;
@property NSInteger height;
@property NSInteger x;
@property NSInteger y;

+ (VFDRect*)rectWithCGRect:(CGRect)frame;
- (CGRect)CGRect;

@end


@interface VFDAttachmentSize : NSObject//RLMObject

@property VFDRect *vFrame ;

@property VFDRect *portretFrame ;
@property VFDRect *landscapeFrame ;

//@property NSNumber<RLMFloat> *width;
//@property NSNumber<RLMFloat> *height;
@property NSNumber*width;
@property NSNumber*height;

@end

@protocol VFDAttachmentSizeProtocol

@property VFDAttachmentSize *attachmentSize;

-(NSNumber*)width;
-(NSNumber*)height;
-(VFDRect*)frame;

@end
