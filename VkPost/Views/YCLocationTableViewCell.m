//
//  YCLocationTableViewCell.m
//  VkPost
//
//  Created by book on 21.09.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "YCLocationTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation YCLocationTableViewCell

- (void)initWithDictionary:(NSDictionary *)locationDict {
    
    if (locationDict) {
        
        NSString *string =
        [NSString stringWithFormat:@"%@ м, %@", locationDict[@"distance"],
         locationDict[@"address"]? : @""];
        self.locationDescriptionLabel.text = string;
        self.locationTitleLabel.text = locationDict[@"title"];
        self.locationTitleLabel.textColor = [UIColor blackColor];
        self.locationCheckinCountLabel.text =
        [NSString stringWithFormat:@"%@", locationDict[@"checkins"]];
        
        if (locationDict[@"group_photo"]) {
            
            [self.locationImageView
             sd_setImageWithURL:[NSURL
                                 URLWithString:locationDict[@"group_photo"]]];

        } else {
            
            self.locationImageView.image = [UIImage imageNamed:@"Artboard 27"] ;
        }
    }
}

-(void)initWithCurrentPosition {
    
      self.locationImageView.image = [UIImage imageNamed:@"7_newpost_panel_icon_location_set"];

    self.locationTitleLabel.text = @"Текущее местоположение";
    self.locationTitleLabel.textColor = [UIColor blueColor];
    self.locationDescriptionLabel.hidden = YES;
    self.locationCheckinCountLabel.hidden = YES;
    self.countUsersImageView.hidden = YES;
    self.detailTextLabel.hidden = YES;
}

@end
