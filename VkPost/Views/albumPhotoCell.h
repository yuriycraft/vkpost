//
//  albumPhotoCell.h
//  LangAloud
//
//  Created by Dmitriy on 02.04.15.
//  Copyright (c) 2015 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCSelectedCollectionViewCell.h"

@interface albumPhotoCell : YCSelectedCollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoOutlet;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

- (IBAction)selectButtonAction:(id)sender;

@end
