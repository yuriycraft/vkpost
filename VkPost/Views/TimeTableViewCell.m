//
//  timeTableViewCell.m
//  VkPost
//
//  Created by book on 11.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "TimeTableViewCell.h"

@implementation TimeTableViewCell


- (void)setLabelDate:(NSDate *)date {

  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  formatter.dateStyle = NSDateFormatterShortStyle;
  formatter.timeStyle = NSDateFormatterShortStyle;

  formatter.doesRelativeDateFormatting = YES;

  self.timeLabel.text = [formatter stringFromDate:date];
}



@end
