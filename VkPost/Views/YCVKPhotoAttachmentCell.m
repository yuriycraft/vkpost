//
//  YCVKPhotoAttachmentCell.m
//  VkPost
//
//  Created by book on 29.08.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "YCVKPhotoAttachmentCell.h"
#import "YCVKUploadingAttachment.h"
#import "VFDAttachmentsCollectionView.h"
#import "YCVKPollModel.h"
#import "YCVKVideoUploadingModel.h"
#import "VFDDocAttachment.h"
#import <QuartzCore/QuartzCore.h>

@implementation YCVKPhotoAttachmentCell{
    BOOL animating;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    _loadingIndicator.userInteractionEnabled = NO;
    _loadingIndicator.thicknessRatio = 0.15;
    _loadingIndicator.roundedCorners = NO;
    _loadingIndicator.hidden = NO;
    _loadingIndicator.roundedCorners = YES;
    
    self.errorLabel.hidden = YES;
    self.errorLabel.userInteractionEnabled = NO;
    self.documentDescriptionView.hidden = YES;
    
    
    self.attachImageView.layer.cornerRadius = 15.f;
    
    self.maskView.alpha = 0.5f;
    
    // Listen for photo loading notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setProgressFromNotification:)
                                                 name:ATTACHMENT_PROGRESS_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handlePhotoLoadingDidEndNotification:)
                                                 name:ATTACHMENT_LOADING_DID_END_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showErrorUploadingLabel:)
                                                 name:ATTACHMENT_UPLOAD_ERROR_NOTIFICATION
                                               object:nil];
    
}

-(void)configUloadingCellWithAttachment:(YCVKUploadingAttachment*) attach andVFDAttachmentsCollectionView:(VFDAttachmentsCollectionView*) target andGestureDelegate:(id)delegate andIndexPath:(NSIndexPath*)indexPath
{
    
    [self configTypeWithYCVKUploadingAttachment:attach];
    
    //Tap for remove attach
    UITapGestureRecognizer *tapCloseGesture = [[UITapGestureRecognizer alloc]
                                               initWithTarget:target
                                               action:@selector(removeAttachCollectionViewAtIndexPath:)];
    self.closeView.tag = indexPath.item;
    
    tapCloseGesture.delegate = delegate;
    
    [self.closeView addGestureRecognizer:tapCloseGesture];
    
    //Tap for reUpload if error
    
    UITapGestureRecognizer *tapReUploadGesture = [[UITapGestureRecognizer alloc]
                                                  initWithTarget:target
                                                  action:@selector(tapGestureReUploadAction:)];
    self.errorLabel.tag = indexPath.item;
    
    tapReUploadGesture.delegate = delegate;
    
    [self.errorLabel addGestureRecognizer:tapReUploadGesture];
    
}

-(void)configTypeWithYCVKUploadingAttachment:(YCVKUploadingAttachment*) attach {
    
    [self configStatusWithYCVKUploadingAttachment:attach];
    
}

-(void)configStatusWithYCVKUploadingAttachment:(YCVKUploadingAttachment*) attach {
    
    if([self checkAttachmentForCell:attach]){
                dispatch_async(dispatch_get_main_queue(), ^{
        
        self.attachImageView.image = attach.preview;
        
        switch (attach.status) {
                
            case kErrorUploading:
                [self configErrorUploadingWithAttach:attach];
                
                break;
                
            case kUploading:
                [self configUploadingWithAttach:attach];
                
                
                break;
                
            case kDoneUploading:
                [self configDoneUploadingWithAttach:attach];
                
                break;
                
            default:
                //[self configUploadingWithAttach:attach];
                break;
        }
         });
    }
}


#pragma mark - Notifications

- (void)setProgressFromNotification:(NSNotification *)notification {
    
    YCVKUploadingAttachment* attach = [notification object];
    
    if([self checkAttachmentForCell:attach] ){
        
        [self configUploadingWithAttach:attach];
        
        self.timeLabel.hidden = YES;
        
        if((attach.type == kDocumentType )&& attach.preview && attach.document){
            
            self.documentDescriptionView.hidden = NO;
            
            self.documentDescriptionLabel.text =
            [NSString stringWithFormat:@"%@, %@" ,[NSByteCountFormatter stringFromByteCount:attach.uploadData.length countStyle:NSByteCountFormatterCountStyleFile],attach.document.ext];
        } else {
            
            self.documentDescriptionView.hidden = YES;
            
        }
    }
}


- (void)handlePhotoLoadingDidEndNotification:(NSNotification *)notification {
    
    YCVKUploadingAttachment* attach = [notification object];
    
    if( [self checkAttachmentForCell:attach]){
        
        [self configDoneUploadingWithAttach:attach];
        
    }
    
}

- (void)showErrorUploadingLabel:(NSNotification *)notification {
    
    YCVKUploadingAttachment* attach = [notification object];
    
    if([self checkAttachmentForCell:attach]){
        
        
        [self configErrorUploadingWithAttach:attach];
        
        self.timeLabel.hidden = YES;
        
        
    }
    
}

#pragma mark - Actions config

// Add new type attachment
-(void)configDoneUploadingWithAttach:(YCVKUploadingAttachment*)attach {
    
    if([self checkAttachmentForCell:attach]){
        
        switch (attach.type) {
                
            case kPhotoType:
                
                [self configCellPhotoWithAttach:attach];
                
                break;
                
            case kPollType:
                [self configCellPollWithAttach:attach];
                break;
                
            case kPhotoVkType:
                
                [self configCellPhotoWithAttach:attach];
                break;
                
            case kVideoVKType:
                
                [self configCellVideoVKWithAttach:attach];
                break;
                
            case kVideoType:
                [self configCellVideoWithAttach:attach];
                break;
                
            case kDocumentType:
                [self configCellDocumentWithAttach:attach];
                break;
                
            case kGeoType:
                [self configCellGeoWithAttach:attach];
                break;
                
            default:
                break;
        }
    }
    
}

-(void)doneUploading {
    
    [self stopSpin];
    
    self.maskView.alpha = 0.f;
    self.loadingIndicator.progress = 0.f;
    self.loadingIndicator.hidden = YES;
    self.errorLabel.hidden = YES;
    self.errorLabel.userInteractionEnabled = NO;
    
}

-(void)configUploadingWithAttach:(YCVKUploadingAttachment*)attach {
    
    if([self checkAttachmentForCell:attach]) {
        
        [self startSpin];
        self.maskView.alpha = 0.5f;
        //  self.loadingIndicator.progress = attach.progressUploading;
        [_loadingIndicator setProgress:MAX(MIN(1, attach.progressUploading ? : 0.f), 0) animated:NO];
        self.loadingIndicator.hidden = NO;
        self.errorLabel.hidden = YES;
        self.errorLabel.userInteractionEnabled = NO;
        self.timeLabel.hidden = YES;
        self.videoVKImageView.hidden = YES;
        
        
        if(attach.type == kDocumentType) {
            
            self.documentDescriptionView.hidden = NO;
            
        }
    }
}

-(void)configErrorUploadingWithAttach:(YCVKUploadingAttachment*)attach {
    
    if([self checkAttachmentForCell:attach]) {
        
        [self stopSpin];
        self.loadingIndicator.hidden = YES;
        self.loadingIndicator.progress = 0.f;
        self.maskView.alpha = 1.f;
        self.errorLabel.hidden = NO;
        self.errorLabel.userInteractionEnabled = YES;
        self.timeLabel.hidden = YES;
        self.documentDescriptionView.hidden = YES;
        self.videoVKImageView.hidden = YES;
        
    }
}

#pragma mark - Poll

-(void)configCellPollWithAttach:(YCVKUploadingAttachment*)attach {
    
    if([self checkAttachmentForCell:attach]){
        
        self.titleTextLabel.text = @"ОПРОС";
        
        self.subtitleTextLabel.text = @"";
        
        self.descriptionTextLabel.text = attach.poll.question;
        
        self.textBackgroundView.hidden = NO;
        
        self.attachImageView.image= nil;
        
        self.timeLabel.hidden = YES;
        
        self.videoVKImageView.hidden = YES;
        
        self.documentDescriptionView.hidden = YES;
        
        if(attach.status == kDoneUploading ){
            
            [self doneUploading];
        }
    }
}

#pragma mark - Geo

-(void)configCellGeoWithAttach:(YCVKUploadingAttachment*)attach {
    
    if([self checkAttachmentForCell:attach]) {
        
        self.attachImageView.image = attach.preview;
        
        self.textBackgroundView.hidden = YES;
        
        self.documentDescriptionView.hidden = YES;
        
        self.videoVKImageView.hidden = YES;
        
        self.timeLabel.hidden = YES;
        
        
        if(attach.status == kDoneUploading) {
            
            [self doneUploading];
            
        }
    }
}

#pragma mark - Photo and PhotoVk

-(void)configCellPhotoWithAttach:(YCVKUploadingAttachment*)attach {
    
    if([self checkAttachmentForCell:attach]) {
        
        self.maskView.alpha = 0.5f;
        
        self.loadingIndicator.progress = attach.progressUploading;
        
        self.attachImageView.image = attach.preview;
        
        self.textBackgroundView.hidden = YES;
        
        self.documentDescriptionView.hidden = YES;
        
        self.videoVKImageView.hidden = YES;
        
        self.timeLabel.hidden = YES;
        
        [self configCellStatusWithAttach:attach];
        
    }
}

#pragma mark - Document

-(void)configCellDocumentWithAttach:(YCVKUploadingAttachment*)attach {
    
    if([self checkAttachmentForCell:attach] ) {
        
        self.videoVKImageView.hidden = YES;
   
        //Uploading image
        if(attach.uploadData) {
            
            self.maskView.alpha = 0.5f;
            
            self.loadingIndicator.progress = attach.progressUploading;
            
            self.attachImageView.image = attach.preview;
            
            self.documentDescriptionView.hidden = NO;
            
            self.documentDescriptionLabel.text =
            [NSString stringWithFormat:@"%@, %@" ,[NSByteCountFormatter stringFromByteCount:attach.uploadData.length countStyle:NSByteCountFormatterCountStyleFile],attach.document.ext];
            
        }
        //Если фото или гиФ
        else  if([attach.document.type integerValue] == kDocumentImageType || [attach.document.type integerValue]== kDocumentGifType) {
            
            self.maskView.alpha = 0.f;
        self.textBackgroundView.hidden = YES;
        self.attachImageView.image = attach.preview;
        self.videoVKImageView.hidden = YES;
        self.documentDescriptionView.hidden = YES;
    
        }
    
    
       else  if(![attach uploadData] && ![attach preview]) {
            
            self.maskView.alpha = 0.f;
            self.attachImageView.image = attach.preview;
            self.textBackgroundView.hidden = NO;
            self.titleTextLabel.text = attach.document.ext;
            self.subtitleTextLabel.text = attach.document.title;
            self.videoVKImageView.hidden = YES;
             self.descriptionTextLabel.text = [NSByteCountFormatter stringFromByteCount:[attach.document.file_size longLongValue]
                                                                             countStyle:NSByteCountFormatterCountStyleFile];

        
    
    }
        [self configCellStatusWithAttach:attach];
        if(attach.status == kDoneUploading) {
            
            self.timeLabel.hidden = YES;
        }
    }
}


#pragma mark - Video

-(void)configCellVideoWithAttach:(YCVKUploadingAttachment*)attach {
    
    if([self checkAttachmentForCell:attach]) {
        
        self.maskView.alpha = 0.5f;
        
        self.loadingIndicator.progress = attach.progressUploading;
        
        self.attachImageView.image = attach.preview;
        
        self.textBackgroundView.hidden = YES;
        
        self.documentDescriptionView.hidden = YES;
        
        self.timeLabel.hidden = YES;
        
        self.videoVKImageView.hidden = YES;
        
        
        if(attach.status == kDoneUploading) {
            
            [self doneUploading];
            
            NSDate* d = [NSDate dateWithTimeIntervalSince1970:attach.video.duration];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
            [dateFormatter setDateFormat:@"mm:ss"];
            
            self.timeLabel.text = [dateFormatter stringFromDate:d];
            self.timeLabel.hidden = NO;
            
        }
        else if(attach.status == kUploading) {
            
            [self configUploadingWithAttach:attach];
            self.timeLabel.hidden = YES;
            self.documentDescriptionView.hidden = YES;
            
        } else if (attach.status == kErrorUploading) {
            
            [self configErrorUploadingWithAttach:attach];
            self.timeLabel.hidden = YES;
            self.documentDescriptionView.hidden = YES;
        }
    }
}

-(void)configCellVideoVKWithAttach:(YCVKUploadingAttachment*)attach {
    
    if([self checkAttachmentForCell:attach]) {
        
        self.maskView.alpha = 0.5f;
        
        self.loadingIndicator.progress = attach.progressUploading;
        
        self.attachImageView.image = attach.preview;
        
        self.textBackgroundView.hidden = YES;
        
        self.documentDescriptionView.hidden = YES;
        
        self.videoVKImageView.hidden = NO;
        
        
        [self configCellStatusWithAttach:attach];
        
    }
}

-(void)configCellStatusWithAttach:(YCVKUploadingAttachment*)attach {
    
    if(attach.status == kDoneUploading) {
        
        [self doneUploading];
        
    }
    else if(attach.status == kUploading) {
        
        [self configUploadingWithAttach:attach];
        
    } else if (attach.status == kErrorUploading) {
        
        [self configErrorUploadingWithAttach:attach];
    }
    
}

-(BOOL)checkAttachmentForCell:(YCVKUploadingAttachment*)attach {
    
    if(attach.uid == self.maskView.tag) {
        
        return YES;
    }
    return NO;
}

- (void) spinWithOptions: (UIViewAnimationOptions) options {
    // this spin completes 360 degrees every 2 seconds
    [UIView animateWithDuration: 0.5f
                          delay: 0.0f
                        options: options
                     animations: ^{
                         self.loadingIndicator.transform = CGAffineTransformRotate(_loadingIndicator.transform, M_PI / 2);
                     }
                     completion: ^(BOOL finished) {
                         
                         if (finished) {
                             
                             if (animating) {
                                 
                                 // if flag still set, keep spinning with constant speed
                                 [self spinWithOptions: UIViewAnimationOptionCurveLinear];
                                 
                             } else if (options != UIViewAnimationOptionCurveEaseOut) {
                                 
                                 // one last spin, with deceleration
                                 [self spinWithOptions: UIViewAnimationOptionCurveEaseOut];
                             }
                         }
                     }];
}

- (void) startSpin {
    
    if (!animating) {
        
        animating = YES;
        [self spinWithOptions: UIViewAnimationOptionCurveEaseIn];
    }
}

- (void) stopSpin {
    
    // set the flag to stop spinning after one last 90 degree increment
    animating = NO;
}
@end
