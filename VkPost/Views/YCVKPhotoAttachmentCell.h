//
//  YCVKPhotoAttachmentCell.h
//  VkPost
//
//  Created by book on 29.08.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DACircularProgress/DACircularProgressView.h>



@class YCVKUploadingAttachment,VFDAttachmentsCollectionView;

@interface YCVKPhotoAttachmentCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (nonatomic, weak) IBOutlet UIImageView *attachImageView;
@property (weak, nonatomic) IBOutlet UIView *closeView;
@property (weak, nonatomic) IBOutlet DACircularProgressView *loadingIndicator;
@property (weak, nonatomic) IBOutlet UIView *maskView;

//This Outlets for poll and audio type
@property (weak, nonatomic) IBOutlet UIView *textBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

//This Outlets for document type
@property (weak, nonatomic) IBOutlet UIView *documentDescriptionView;
@property (weak, nonatomic) IBOutlet UILabel *documentDescriptionLabel;
//This Outlets for videoVk type
@property (weak, nonatomic) IBOutlet UIImageView *videoVKImageView;


-(void)configUloadingCellWithAttachment:(YCVKUploadingAttachment*) attach andVFDAttachmentsCollectionView:(VFDAttachmentsCollectionView*) target andGestureDelegate:(id)delegate andIndexPath:(NSIndexPath*)indexPath;

- (void) startSpin ;
@end
