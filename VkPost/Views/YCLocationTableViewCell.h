//
//  YCLocationTableViewCell.h
//  VkPost
//
//  Created by book on 21.09.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCLocationTableViewCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *locationTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *locationDescriptionLabel;
@property(weak, nonatomic) IBOutlet UIImageView *locationImageView;
@property(weak, nonatomic) IBOutlet UILabel *locationCheckinCountLabel;
@property(weak, nonatomic) IBOutlet UIImageView *countUsersImageView;

- (void)initWithDictionary:(NSDictionary *)locationDict;

-(void)initWithCurrentPosition ;

@end
