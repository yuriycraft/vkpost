//
//  AlbumOpenedCV.m
//  LangAloud
//
//  Created by Dmitriy on 02.04.15.
//  Copyright (c) 2015 Dmitriy. All rights reserved.
//

#import "AlbumOpenedCV.h"
#import "albumPhotoCell.h"
#import "UIBarButtonItem+Badge.h"

#warning  unComment
//#import "PhotoPageViewController.h"
//#import "commonUtillites.h"
#import "VFPhotoAttachment.h"
#import "VFDVKService.h"
#warning ToComment
#import <SDWebImage/UIImageView+WebCache.h>
#import "YCAlbumModel.h"

#define CELL_HEIGHT 100
#define REQUESTING_PHOTOS_COUNT 100
#define CELL_IDENTIFIER @"albumPhotoCell"
#define COUNT_CELLS_IN_LINE 4

@interface AlbumOpenedCV ()<YCPhotoCollectionViewCellDelegate>

@end

@implementation AlbumOpenedCV {
    
    NSInteger countCellInLine;
}
@synthesize  canDelete ;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if(self.isPicker) {
        
        self.toolbarHeightConstraint.constant = HEIGHT_TOOLBAR_CONSTANT;
        
        countCellInLine = PICKER_COUNT_CELLS_IN_LINE;
        self.toolBar.hidden = NO;
   
        //[self checkSelectedCount];
        
    } else {
        
        countCellInLine = COUNT_CELLS_IN_LINE;
    }
 
    self.navigationItem.title = _albumModel.title ? _albumModel.title : @"Альбом";
    
    self.navigationItem.hidesBackButton = YES;
    
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@"Назад" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonAction:)];
    
    self.navigationItem.leftBarButtonItem = newBackButton;
    
    [self getPhotos];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden: NO animated:NO];

}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    !self.isPicker ? :[self checkSelectedCount];
}

- (void) backButtonAction:(UIBarButtonItem *)sender {

    [self.backFinishPickingDelegate backButtonFinishPickingMediaWithIndexAlbumModel:self.modelIndexPathRow];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark CollectionView

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1 ;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return _albumModel.photosArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    albumPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    
    VFPhotoAttachment *item = [_albumModel.photosArray objectAtIndex:indexPath.item];
    
    [self makePickerWithCell:cell photoModel:item indexPath:indexPath];

#warning uncomment
    
//    [UTILLITES loadImageToImageView:cell.photoOutlet withUrlString:item.photo_604];
    

    if ([self needMorePhotos:indexPath])
        [self getPhotos];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(albumPhotoCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
        VFPhotoAttachment *item =  [_albumModel.photosArray objectAtIndex:indexPath.item];
    
#warning to comment
    [cell.photoOutlet sd_setImageWithURL:[NSURL URLWithString:item.photo_604]
                               completed:^(UIImage *image, NSError *error,
                                           SDImageCacheType cacheType, NSURL *imageURL){
                                   
                               }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(([UIApplication sharedApplication].delegate.window.frame.size.width - 6)/countCellInLine,
                      ([UIApplication sharedApplication].delegate.window.frame.size.width - 6)/countCellInLine);
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self openPhotoWithIndexPath:indexPath];
}

#pragma mark - навигация

-(void)openPhotoWithIndexPath:(NSIndexPath *)indexPath {
#warning uncomment
//    [NAVIGATION presentPhotoFromController:self
//                                 photos:_photosArray
//                        firstPhotoIndex:indexPath.row
//                              canDelete:NO
//                        photoViewerType:self.album_id.integerValue > 0?AlbumPhotoViewerType:AllPhotoViewerType
//                               owner_id:self.owner_id
//                               album_id:self.album_id];
//
}


#pragma mark - работа с сервером


-(void)getPhotos {
    
    isLoadingPhotos = YES ;
    
    if([self.albumModel.album_id isEqual:@"-1"]) { // Если альбом все фото
        
//        [VKSERVICE photosGetAllWithOwnerId:_albumModel.owner_id offset:_albumModel.offset count:REQUESTING_PHOTOS_COUNT completion:^(NSNumber *count, NSMutableArray *items, NSError *error) {
//            
//                  [self initWithArray:items];
//          
//        }];
        return;
    }
    
    
//    [[VFDVKService sharedInstance]photosGetWithAlbumId:_albumModel.album_id owner_id:_albumModel.owner_id count:REQUESTING_PHOTOS_COUNT offset:_albumModel.offset rev:YES completion:^(NSNumber *count, NSMutableArray *items, NSError *error) {
//       
//        [self initWithArray:items];
//        
//    }];
    
}


#pragma mark -  YCPhotoCollectionViewCellDelegate;

- (void)setSelectedItemWithCell:(albumPhotoCell *)cell {
    
     [cell changeReverseSelectedStatus: cell.selectButton.selected ? NO : YES];
    
    if (self.seletedImageArray.count > self.maxNumberOfSelections) { // если колиество уже выбранных превышает
        
        [cell changeReverseSelectedStatus:NO];
        
       [self.doneToolBarButton shakeBageAnimation];
        
        return;
    }
   
}

#pragma mark - удаление фото

-(void)photoEdit:(NSNotification*)notification{
    
    //[_photos removeObjectAtIndex:0];
    //[self.collectionView reloadData];
    
}


#pragma mark - вспомогательные

-(BOOL)needMorePhotos:(NSIndexPath *)indexPath {

     if (indexPath.row < _albumModel.photosArray.count - REQUESTING_PHOTOS_COUNT/3 || isLoadingPhotos || isAllPhotosLoaded)
         
        return NO ;
    else
        
        return YES ;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self.collectionView reloadData];
}

#pragma mark - Actions

-(void)initWithArray:(NSMutableArray*)items {
    
    if(items) {
        
        [_albumModel.photosArray addObjectsFromArray:items];
        
        [self.collectionView reloadData];
        
        _albumModel.offset += REQUESTING_PHOTOS_COUNT ;
        
        isLoadingPhotos = NO ;
        
        if (REQUESTING_PHOTOS_COUNT > items.count) {
            
            isAllPhotosLoaded = YES ;
        }
        
    }
    
}

-(void)checkSelectedCount {
    
    self.doneToolBarButton.enabled = self.seletedImageArray.count > 0 ? YES : NO;

    self.doneToolBarButton.badgeValue = [NSString
                                         stringWithFormat:@"%lu", (unsigned long)self.seletedImageArray.count];
        NSLog(@"%@",self.doneToolBarButton.badge);


}



-(void)makePickerWithCell:(albumPhotoCell*)cell
               photoModel:(VFPhotoAttachment*)attach
                indexPath:(NSIndexPath*)indexPath
{
    if(self.isPicker) {
        
        VFPhotoAttachment *photoModel = _albumModel.photosArray[indexPath.item];
        
        cell.cellDelegate = self;
        cell.selectButton.hidden = NO;
        cell.selectButton.enabled = YES;
        cell.selectedStatus = photoModel.selectedStatus;
        
        // Select button block
        [cell buttonSelectBlock:^(BOOL seletedStatus) {
            
            photoModel.selectedStatus = seletedStatus;
            
            if (photoModel.selectedStatus) {
                
                [self.seletedImageArray addObject:photoModel];
                
                
            } else {
                
                [self.seletedImageArray removeObject:photoModel];
                
            }
            
            [self checkSelectedCount];
    
        }];
    }
}



- (IBAction)cancelToolBarButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)doneToolBarButtonAction:(id)sender {
    
    [self.imagePickerDelegate ycPhotoPicker:self
        didFinishPickingMediaWithImageArray:self.seletedImageArray];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - set photos size

-(void)setPhotosSize:(NSMutableArray *)photos{
    
    NSMutableArray *photoRow = [NSMutableArray new];
    
    for (VFPhotoAttachment *photo in photos) {
        
        if (photoRow.count < 4) {
            [photoRow addObject:photo];
        }else{
            [self setPortretPhotoRow:photoRow];
            [self setLandscapePhotoRow:photoRow];
            photoRow = [NSMutableArray new];
        }
    }
}

-(void)setPortretPhotoRow:(NSMutableArray *)photoRow{
    
    NSLog(@"photoRow.count - %i", photoRow.count);
    
    //ширина ряда без пробелов
    NSInteger row_width = [UTILLITES getScreenSizeWithRotation:NO].width - 4 ;
    
    //сумма соотношений ширины фото к высоте
    float summRowRatio = [self getRowSummRatio:photoRow];
    
    //присваеваем ширину фото
    for (VFAttachmentSize *photo in photoRow) {
        float photoRatio = [photo.width floatValue]/[photo.height floatValue] ;
        NSInteger width = (int)row_width * photoRatio/summRowRatio ;
        //NSLog(@"width - %li", (long)width);
        
        photo.portretFrame = CGRectMake(0, 0, width, CELL_HEIGHT) ;
    }
}

-(void)setLandscapePhotoRow:(NSMutableArray *)photoRow{
    
    //ширина ряда без пробелов
    NSInteger row_width = [UTILLITES getScreenSizeWithRotation:NO].height - 6 ;
    
    //сумма соотношений ширины фото к высоте
    float summRowRatio = [self getRowSummRatio:photoRow];
    
    //присваеваем ширину фото
    for (VFAttachmentSize *photo in photoRow) {
        float photoRatio = [photo.width floatValue]/[photo.height floatValue] ;
        NSInteger width = (int)row_width * photoRatio/summRowRatio ;
        photo.landscapeFrame = CGRectMake(0, 0, width, CELL_HEIGHT) ;
    }
}

-(float)getRowSummRatio: (NSMutableArray *)photoRow{
    
    float summRowRatio = 0.0 ;
    
    for (VFAttachmentSize *photo in photoRow) {
        summRowRatio = summRowRatio + [photo.width floatValue]/[photo.height floatValue] ;
    }
    
    return summRowRatio ;
}

*/



/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/


/*
NSDictionary *param = @{@"owner_id" : owner_id,
                        @"album_id" : album_id,
                        @"count" : @"1000",
                        @"rev": @"1",
                        };

VKRequest *photosGetAll = [VKRequest requestWithMethod:@"photos.get" andParameters:param andHttpMethod:@"POST"];
photosGetAll.attempts = 3;

[photosGetAll executeWithResultBlock:^(VKResponse * response) {
    
    NSMutableDictionary *res = [response json];
    NSMutableArray *items = res[@"items"];
    
    for (NSDictionary *item in items) {
        VFPhotoAttachment *photo = [[VFPhotoAttachment alloc]initWithDictionary:item] ;
        [_photos addObject:photo];
    }
    
    [self.collectionView reloadData];
    
    
} errorBlock:^(NSError * error) {
    if (error.code != VK_API_ERROR) {
        [error.vkError.request repeat];
    }
    else {
        NSLog(@"VK error: %@", error);
    }
}];
*/


@end
