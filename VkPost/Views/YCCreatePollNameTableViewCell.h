//
//  YCCreatePollNameTableViewCell.h
//  VkPost
//
//  Created by book on 09.02.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCCreatePollNameTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
