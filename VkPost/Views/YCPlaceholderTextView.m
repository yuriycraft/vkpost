//
//  YCPlaceholderTextView.m
//  VkPost
//
//  Created by book on 30.08.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "VKUtil.h"
#import "YCPlaceholderTextView.h"

@implementation YCPlaceholderTextView


- (void)awakeFromNib {

  [super awakeFromNib];
  if (!self.placeholder) {
      
    [self setPlaceholder:@""];
  }

  if (!self.placeholderColor) {
      
    [self setPlaceholderColor:[UIColor lightGrayColor]];
  }

  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(textChanged:)
             name:UITextViewTextDidChangeNotification
           object:nil];
}

- (id)initWithFrame:(CGRect)frame {

  if ((self = [super initWithFrame:frame])) {
      
    [self setPlaceholder:@""];
    [self setPlaceholderColor:[UIColor lightGrayColor]];
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(textChanged:)
               name:UITextViewTextDidChangeNotification
             object:nil];
  }
  return self;
}

- (void)textChanged:(NSNotification *)notification {

  if (notification.object != self)
    return;
  if (!self.placeholder.length) {
      
    return;
  }
  [self.self.placeholderLabel setHidden:self.text.length != 0];
}

- (void)setText:(NSString *)text {

  [super setText:text];
  [self.placeholderLabel setHidden:self.text.length != 0];
}

- (void)setPlaceholder:(NSString *)placeholder {

  _placeholder = placeholder;
  if (placeholder.length) {
      
    if (self.placeholderLabel == nil) {
        
      UIEdgeInsets inset = self.contentInset;
#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnavailableInDeploymentTarget"
      if ([VKUtil isOperatingSystemAtLeastIOS7]) {
        inset = self.textContainerInset;
      }
#pragma clang diagnostic pop
      self.placeholderLabel = [[UILabel alloc]
          initWithFrame:CGRectMake(inset.left + 4, inset.top,
                                   self.bounds.size.width - inset.left -
                                       inset.right,
                                   0)];
      self.placeholderLabel.font = self.font;
      self.placeholderLabel.hidden = YES;
      self.placeholderLabel.textColor = self.placeholderColor;
      self.placeholderLabel.lineBreakMode = NSLineBreakByWordWrapping;
      self.placeholderLabel.numberOfLines = 0;
      self.placeholderLabel.backgroundColor = [UIColor clearColor];
      [self addSubview:self.placeholderLabel];
    }

    self.placeholderLabel.text = placeholder;
    [self.placeholderLabel sizeToFit];
    [self sendSubviewToBack:self.placeholderLabel];
  }

  if (self.text.length == 0 && placeholder.length) {
      
    [self.placeholderLabel setHidden:NO];
  }
}

/**
 * iOS 7 text view measurement.
 * Author: tarmes;
 * Source: http://stackoverflow.com/a/19047464/1271424
 */
- (CGFloat)measureHeightOfUITextView {

  UITextView *textView = self;
#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnavailableInDeploymentTarget"
  if ([VKUtil isOperatingSystemAtLeastIOS7]) {
    CGRect frame = textView.bounds;

    // Take account of the padding added around the text.

    UIEdgeInsets textContainerInsets = textView.textContainerInset;
    UIEdgeInsets contentInsets = textView.contentInset;

    CGFloat leftRightPadding = textContainerInsets.left +
                               textContainerInsets.right +
                               textView.textContainer.lineFragmentPadding * 2 +
                               contentInsets.left + contentInsets.right;
    CGFloat topBottomPadding = textContainerInsets.top +
                               textContainerInsets.bottom + contentInsets.top +
                               contentInsets.bottom;

    frame.size.width -= leftRightPadding;
    frame.size.height -= topBottomPadding;

    NSString *textToMeasure =
        textView.text.length ? textView.text : _placeholder;
    if ([textToMeasure hasSuffix:@"\n"]) {
        
      textToMeasure = [NSString stringWithFormat:@"%@-", textView.text];
    }

    // NSString class method: boundingRectWithSize:options:attributes:context is
    // available only on ios7.0 sdk.

    NSMutableParagraphStyle *paragraphStyle =
        [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];

    NSDictionary *attributes = @{
      NSFontAttributeName : textView.font,
      NSParagraphStyleAttributeName : paragraphStyle
    };

    CGRect size = [textToMeasure
        boundingRectWithSize:CGSizeMake(CGRectGetWidth(frame), MAXFLOAT)
                     options:NSStringDrawingUsesLineFragmentOrigin
                  attributes:attributes
                     context:nil];

    CGFloat measuredHeight = ceilf(CGRectGetHeight(size) + topBottomPadding);
    return measuredHeight;
  }
#pragma clang diagnostic pop
  else {
      
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    if (self.text.length && textView.contentSize.height > 0) {
        
      return MAX(textView.contentSize.height + self.contentInset.top +
                     self.contentInset.bottom,
                 36.0f);
    }
    return [self.placeholder
                    sizeWithFont:self.font
               constrainedToSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX)
                   lineBreakMode:NSLineBreakByWordWrapping]
               .height +
           self.contentInset.top + self.contentInset.bottom;
#endif
  }
  return 0;
}

@end
