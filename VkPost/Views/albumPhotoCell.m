//
//  albumPhotoCell.m
//  LangAloud
//
//  Created by Dmitriy on 02.04.15.
//  Copyright (c) 2015 Dmitriy. All rights reserved.
//

#import "albumPhotoCell.h"

@implementation albumPhotoCell


- (void)setSelectedStatus:(BOOL)selectStatus {
    
    [_selectButton setSelected:selectStatus ? YES : NO];
    
    self.photoOutlet.alpha = selectStatus ? 0.9f : 1.f;
    
    [self animateButtonOnSelect:_selectButton];
    
}

- (IBAction)selectButtonAction:(id)sender {
    
      [self.cellDelegate setSelectedItemWithCell:self];
}
@end
