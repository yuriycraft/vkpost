//
//  TimePickerTableViewCell.m
//  VkPost
//
//  Created by book on 11.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "TimePickerTableViewCell.h"

@implementation TimePickerTableViewCell

- (void)setMinimumDateForDatePicker {

  NSDate *nowDate = [NSDate date];
    
    //nowDate + five minutes
    NSDate *datePlusFiveMinute = [nowDate dateByAddingTimeInterval:300];
 
    //datePlusFiveMinute rounded
    NSTimeInterval seconds = ceil([datePlusFiveMinute timeIntervalSinceReferenceDate]/300.0)*300.0;
    NSDate *rounded = [NSDate dateWithTimeIntervalSinceReferenceDate:seconds];
    
      [self.datePicker setMinimumDate:rounded];
}


@end
