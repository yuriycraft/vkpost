//
//  CustomPopoverView.m
//  VkPost
//
//  Created by book on 17.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "CustomPopoverView.h"

#define kArrowHeight 4

@implementation CustomPopoverView

- (void)drawRect:(CGRect)rect {
    
  CGContextRef context = UIGraphicsGetCurrentContext();

  UIBezierPath *fillPath = [UIBezierPath bezierPath];
  UIColor *color = [UIColor colorWithRed:3.f / 255.f
                                   green:11.f / 255.f
                                    blue:34.f / 255.f
                                   alpha:1.f];
  [color set];

  CGRect rectAnnotation = CGRectMake(0, 0, self.bounds.size.width,
                                     self.bounds.size.height - kArrowHeight);
  UIBezierPath *pathAnnotation =
      [UIBezierPath bezierPathWithRoundedRect:rectAnnotation
                                 cornerRadius:22.5f];

  [fillPath moveToPoint:CGPointMake(self.bounds.size.width / 2 -
                                        (kArrowHeight + 2 / 2),
                                    self.bounds.size.height - kArrowHeight)];
  [fillPath addLineToPoint:CGPointMake(self.bounds.size.width / 2,
                                       self.bounds.size.height)];
  [fillPath addLineToPoint:CGPointMake(self.bounds.size.width / 2 +
                                           (kArrowHeight + 2 / 2),
                                       self.bounds.size.height - kArrowHeight)];
  CGContextAddPath(context, fillPath.CGPath);
  CGContextSetFillColorWithColor(context, color.CGColor);
  [pathAnnotation fill];
  CGContextFillPath(context);
}

@end
