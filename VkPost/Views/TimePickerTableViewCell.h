//
//  TimePickerTableViewCell.h
//  VkPost
//
//  Created by book on 11.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimePickerTableViewCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UIDatePicker *datePicker;

- (void)setMinimumDateForDatePicker;

@end
