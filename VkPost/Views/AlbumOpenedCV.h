//
//  AlbumOpenedCV.h
//  LangAloud
//
//  Created by Dmitriy on 02.04.15.
//  Copyright (c) 2015 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCPhotoPickerViewController.h"

@class YCAlbumModel,AlbumOpenedCVm, YCPhotoPickerViewController;


@protocol backAlbumOpenedCVDelegate <NSObject>

@optional

- (void)backButtonFinishPickingMediaWithIndexAlbumModel:(NSInteger)index;

@end



@interface AlbumOpenedCV : YCPhotoPickerViewController <UICollectionViewDelegate, UICollectionViewDataSource> {
    
    //NSInteger offset ;
    BOOL isLoadingPhotos ;
    BOOL isAllPhotosLoaded ;
    
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) YCAlbumModel * albumModel;
//@property (nonatomic, strong) NSMutableArray *_photosArray;
@property (nonatomic, assign) bool canDelete;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightBarButton;
@property (assign, nonatomic) NSInteger modelIndexPathRow;
@property (nonatomic, weak, readwrite) id <backAlbumOpenedCVDelegate> backFinishPickingDelegate;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneToolBarButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toolbarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;


- (IBAction)cancelToolBarButtonAction:(id)sender;
- (IBAction)doneToolBarButtonAction:(id)sender;



@end
