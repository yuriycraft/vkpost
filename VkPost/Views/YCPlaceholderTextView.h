//
//  YCPlaceholderTextView.h
//  VkPost
//
//  Created by book on 30.08.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCPlaceholderTextView : UITextView

@property(nonatomic, strong) NSString *placeholder;
@property(nonatomic, strong) UIColor *placeholderColor;
@property(nonatomic, strong) UILabel *placeholderLabel;

- (void)textChanged:(NSNotification *)notification;
- (void)setText:(NSString *)text;
- (void)setPlaceholder:(NSString *)placeholder;
- (CGFloat)measureHeightOfUITextView;
@end
