//
//  timeTableViewCell.h
//  VkPost
//
//  Created by book on 11.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeTableViewCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *timeLabel;

- (void)setLabelDate:(NSDate *)date;

@end
