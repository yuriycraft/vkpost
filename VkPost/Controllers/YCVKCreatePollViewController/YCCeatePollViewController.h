//
//  YCCeatePollViewController.h
//  VkPost
//
//  Created by book on 08.02.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YCVKPollModel;

@interface YCCeatePollViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic)YCVKPollModel * poll;

@property(nonatomic, weak) id delegate;

@end

@protocol YCCeatePollViewControllerDelegate <NSObject>

- (void)setupYCVKPollModel:(YCVKPollModel*) pollModel;
@end


