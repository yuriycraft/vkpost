//
//  YCCeatePollViewController.m
//  VkPost
//
//  Created by book on 08.02.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "YCCeatePollViewController.h"
#import "YCVKPollModel.h"
#import "YCCreatePollNameTableViewCell.h"

static NSString* kPollSectionThemeName = @"ТЕМА ОПРОСА";
static NSString* kPollSectionResponseOptions = @"ВАРИАНТЫ ОТВЕТА";
static NSInteger kPollQuestionTextViewId = 888;
static NSString *kPollTextViewCellIdentifier = @"YCCreatePollNameTableViewCell";
static NSString *kPollSwithCellIdentifier = @"SwitchCell";
static NSString *kPollSwithCellTitle = @"Анонимное голосование";
static NSString *kPollAddQuestionButtonCellIdentifier = @"AddQuestionCell";
static NSString *kPollAddQuestionButtonTitle = @"Добавить вариант";
static NSString *kPollNavigationBarTitle = @"Новый опрос";

@interface YCCeatePollViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UIGestureRecognizerDelegate>

@property(strong,nonatomic)UIBarButtonItem *doneButton;
@property(strong,nonatomic)UIBarButtonItem *cancelButton;

@end

@implementation YCCeatePollViewController {
    
    BOOL isNotEmty;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if(!self.poll) {
        
        self.poll = [YCVKPollModel new];
        self.poll.answersArray = [NSMutableArray array];
        
    }
    [self configNavigationBar];
    [self checkDoneButton];
    [self configTableView];
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];
    
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Configs

-(void)configTableView {
    
    [_tableView registerNib:[UINib nibWithNibName:@"YCCreatePollNameTableViewCell" bundle:nil]
     forCellReuseIdentifier:@"YCCreatePollNameTableViewCell"];
    
    self.tableView.estimatedRowHeight = 44.0f ;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.tableView.allowsSelection = NO;
    
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tableViewTapped:)];
    tgr.delegate = self;
    
    [self.tableView addGestureRecognizer:tgr];
}

-(void)configNavigationBar {
    
    self.navigationItem.title = kPollNavigationBarTitle;
    
    self.doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Готово" style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonAction)];
    
    self.cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Отмена" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonAction)];
    
    self.navigationItem.rightBarButtonItem =  self.doneButton;
    self.navigationItem.leftBarButtonItem =  self.cancelButton;
    
}

#pragma mark - Actions

-(void)checkDoneButton {
    
    if(self.poll.question.length && self.poll.answersArray.count > 0) {
        
        self.doneButton.enabled = YES;
        
    } else {
        
        self.doneButton.enabled = NO;
    }
}

-(void)tableViewTapped:(UITapGestureRecognizer*)recognizer{
    
    [self.view endEditing:YES];
}

-(void)cancelButtonAction {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)doneButtonAction {
    
    [self.view endEditing:YES];
    [self.delegate setupYCVKPollModel:self.poll];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)insertQuestionButtonAction:(UIButton*)sender {
    
    if(self.poll.answersArray.count > 0) {
        
        YCCreatePollNameTableViewCell * cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.poll.answersArray.count -1 inSection:1]];
        
        
        isNotEmty = !cell.textView.text.length ? YES :NO;
        
        [cell.textView becomeFirstResponder];
    }
    
    if(self.poll.answersArray.count <= 9 && !isNotEmty) {
        
        [self.poll.answersArray  addObject:@""];
        
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.poll.answersArray.count -1 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.poll.answersArray.count inSection:1] atScrollPosition:UITableViewScrollPositionBottom animated:NO];

    }
    
    
}

-(void)switchChange:(UISwitch*)sender {
    
    self.poll.is_anonymous = sender.isOn;
    
    
}


#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification *)notification {
    
    CGSize keyboardSize =
    [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey]
     CGRectValue]
    .size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait(
                                         [[UIApplication sharedApplication] statusBarOrientation])) {
        
        contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0.f,
                                         (keyboardSize.height), 0.f);
        
        
    } else {
        contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0.f,
                                         (keyboardSize.width), 0.f);
    }
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    
    UIEdgeInsets contentInsets =
    UIEdgeInsetsMake(self.tableView.contentInset.top, 0.f, 0.f, 0.f);
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

#pragma - mark UITextViewDelegate methods

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    return YES;
}


- (void)textViewDidEndEditing:(UITextView *)textView  {
    
    isNotEmty = !textView.text.length ? YES : NO;
    
    NSInteger index = textView.tag < 99 ? :textView.tag - 100;
    
    if(!textView.text.length) {
        
        [self.poll.answersArray  removeObjectAtIndex:index];
        
        [self.tableView  deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index  inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }else if(textView.text.length && textView.tag != kPollQuestionTextViewId) {
        
        [self.poll.answersArray  replaceObjectAtIndex:index withObject:textView.text];
        NSLog(@"%@",self.poll.answersArray);
    }
    else if(textView.tag == kPollQuestionTextViewId) {
        
        self.poll.question = textView.text;
    }
    
}
- (void)textViewDidChange:(UITextView *)textView {
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    [self checkDoneButton];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return 2;
        
    } else if (section == 1) {
        
        return self.poll.answersArray.count + 1;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}


- (nullable NSString *)tableView:(UITableView *)tableView
         titleForHeaderInSection:(NSInteger)section {
    
    switch (section) {
            
        case 0:
            return kPollSectionThemeName;
            break;
            
        case 1:
            return kPollSectionResponseOptions;
            break;
            
        default:
            break;
    }
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 0) {
        
        if(indexPath.row == 0) {
            
            return [self configTextViewCellWithTableView:tableView];
            
        } else if (indexPath.row == 1) {
            
            return [self configSwithViewCellWithTableView:tableView];
                   }
        
    } else if(indexPath.section == 1){
        
        if(indexPath.row >= self.poll.answersArray.count)  {
            
            return [self crateAndConfigAddQuestionButtonCellWithTableView:tableView];
        }  else if(self.poll.answersArray.count >= 1) {
            
            return [self createAndConfigAnswerCellWithTableView:tableView andIndexPath:indexPath];
        }
        
    }
    return nil;
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return UITableViewAutomaticDimension;
    
}

#pragma mark - Config Cells
-(YCCreatePollNameTableViewCell*)configTextViewCellWithTableView:(UITableView*)tableView {
    
    YCCreatePollNameTableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:kPollTextViewCellIdentifier];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:kPollTextViewCellIdentifier owner:self options:nil];
        cell = (YCCreatePollNameTableViewCell *)[nib objectAtIndex:0];
        cell.textView.translatesAutoresizingMaskIntoConstraints = NO;
        cell.translatesAutoresizingMaskIntoConstraints = NO;
        
    }
    cell.textView.delegate = self;
    
    cell.textView.text = self.poll.question;
    
    cell.textView.tag = 888;
    
    return cell;
    
}

-(UITableViewCell*)configSwithViewCellWithTableView:(UITableView*)tableView {
    
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:kPollSwithCellIdentifier];
    
    UISwitch *switchView;
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kPollSwithCellIdentifier];
        cell.textLabel.text = kPollSwithCellTitle;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
        
        cell.accessoryView = switchView;
        
        [switchView addTarget:self
                       action:@selector(switchChange:)
             forControlEvents:UIControlEventValueChanged];
        
    }
    else {
        
        switchView = (UISwitch *)cell.accessoryView;
    }
    [switchView setOn:self.poll.is_anonymous];
    
    return cell;
    
}

-(UITableViewCell*)crateAndConfigAddQuestionButtonCellWithTableView:(UITableView*)tableView {
    
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:kPollAddQuestionButtonCellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kPollAddQuestionButtonCellIdentifier];
        
        UIButton * button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.translatesAutoresizingMaskIntoConstraints = NO;
        [button setTitle:kPollAddQuestionButtonTitle forState:UIControlStateNormal];
        
        button.titleLabel.textColor = [UIColor redColor];
        
        button.enabled = self.poll.answersArray.count != 9 ;
        
        
        [button addTarget:self action:@selector(insertQuestionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:button];
        
        
        NSLayoutConstraint *top = [NSLayoutConstraint
                                   constraintWithItem:button
                                   attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:cell.contentView
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0f
                                   constant:0.f];
        
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:button
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:cell.contentView
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        
        NSLayoutConstraint *trailing = [NSLayoutConstraint
                                        constraintWithItem:button
                                        attribute:NSLayoutAttributeTrailing
                                        relatedBy:NSLayoutRelationEqual
                                        toItem:cell.contentView
                                        attribute:NSLayoutAttributeTrailing
                                        multiplier:1.0f
                                        constant:0.f];
        
        NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:button
                                                                            attribute:NSLayoutAttributeHeight
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:nil
                                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                                           multiplier:1.0
                                                                             constant:44.f];
        
        [cell.contentView addConstraint:heightConstraint];
        [cell.contentView addConstraint:trailing];
        [cell.contentView addConstraint:top];
        [cell.contentView addConstraint:leading];
        
    }
    return cell;
    
}

-(YCCreatePollNameTableViewCell*)createAndConfigAnswerCellWithTableView:(UITableView*)tableView andIndexPath:(NSIndexPath*)indexPath {
    
    YCCreatePollNameTableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:kPollTextViewCellIdentifier];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:kPollTextViewCellIdentifier owner:self options:nil];
        cell = (YCCreatePollNameTableViewCell *)[nib objectAtIndex:0];
        
        cell.textView.translatesAutoresizingMaskIntoConstraints = NO;
        cell.translatesAutoresizingMaskIntoConstraints = NO;
        
    }
    cell.textView.tag = indexPath.row +100;
    NSInteger index = indexPath.row;
    cell.textView.text = self.poll.answersArray[index];
    cell.textView.delegate = self;
    
    [cell.textView becomeFirstResponder];
    
    return cell;
}
@end
