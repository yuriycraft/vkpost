//
//  MapViewController.m
//  VkPost
//
//  Created by book on 16.09.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "CustomPopoverView.h"
#import "DetailMapViewController.h"
#import "MapViewController.h"
#import "VKSdk.h"

#import "YCLocationTableViewCell.h"
#import <MapKit/MapKit.h>

#import "VFDVKService+Location.h"
@import CoreLocation;

static NSString * kPositionCellId = @"positionCell";
static NSString * kDetailMapViewControllerId = @"DetailMapViewController";
static NSString * kMainStoryboardName = @"Main";

@interface MapViewController () <CLLocationManagerDelegate, MKMapViewDelegate,
UITableViewDelegate, UITableViewDataSource,
UISearchBarDelegate,
UIViewControllerTransitioningDelegate,
UIPopoverPresentationControllerDelegate>

@property(strong, nonatomic) CLLocationManager *locationManager;
@property(assign, nonatomic) BOOL isLoad;
@property(assign, nonatomic) BOOL isUserLocation;
@property(assign, nonatomic) BOOL isKeyboardShow;
@property(strong, nonatomic) NSMutableArray *sourceArray;
@property(strong, nonatomic) NSMutableArray *searchArray;

@end

@implementation MapViewController

@synthesize delegate;

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:
(UIPresentationController *)controller {
    
    return UIModalPresentationNone;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.popoverView.hidden = YES;
    
    _isLoad = YES;
    _isKeyboardShow = NO;
    _isUserLocation = NO;
    
    self.sourceArray = [NSMutableArray array];
    self.searchArray = [NSMutableArray array];
    self.mapView.delegate = self;
    
    [self setupLocationManager];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];
    
    
}

- (void)dealloc {
 
#if DEBUG
        // Xcode8/iOS10 MKMapView bug workaround
        static NSMutableArray* unusedObjects;
        if (!unusedObjects)
            unusedObjects = [NSMutableArray new];
        [unusedObjects addObject:_mapView];
#endif
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification *)notification {
    
    _isKeyboardShow = YES;
    CGSize keyboardSize =
    [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey]
     CGRectValue]
    .size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait(
                                         [[UIApplication sharedApplication] statusBarOrientation])) {
        
        contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0.f,
                                         (keyboardSize.height), 0.f);
    } else {
        contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0.f,
                                         (keyboardSize.width), 0.f);
    }
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    [self.searchBar setShowsCancelButton:YES animated:NO];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    _isKeyboardShow = NO;
    UIEdgeInsets contentInsets =
    UIEdgeInsetsMake(self.tableView.contentInset.top, 0.f, 0.f, 0.f);
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Config Location Manager

- (void)setupLocationManager {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    // for iOS 8, specific user level permission is required,
    // "when-in-use" authorization grants access to the user's location
    if ([self.locationManager
         respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    if (![CLLocationManager locationServicesEnabled] &&
        [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        
        [self showError];
    }
    
    [self.locationManager startUpdatingLocation];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {

     if (self.searchBar.text.length  || _isKeyboardShow) {
         
            return self.searchArray.count;
        }
    
      NSInteger countRows = 1;
    countRows += self.sourceArray.count;
    
    return countRows;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    YCLocationTableViewCell *cell;

    cell =
    [tableView dequeueReusableCellWithIdentifier:kPositionCellId];
   
    if (indexPath.row == 0 && (!self.searchBar.text.length ||
                               !_isKeyboardShow)) {
        
        [cell initWithCurrentPosition];
        
        return cell;
    }
    
    
    NSDictionary *dictLocation;

    if (self.searchBar.text.length && _isKeyboardShow ) {
        
        dictLocation = [self.searchArray objectAtIndex:indexPath.row];
        
    } else {
         
        NSInteger indexPathRow = indexPath.row - 1;
        dictLocation = [self.sourceArray objectAtIndex:indexPathRow];
        
    }
    
    [cell initWithDictionary:dictLocation];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0 && (!_isKeyboardShow ||
        !self.searchBar.text.length) && self.locationManager.location) {
        
        // CurrentPosition
        
        [VKSERVICE  getAddressFromLocation:self.locationManager.location
                     complationBlock:^(NSString *address) {
                         
                         if (address) {
                             
        [self.delegate setStringLocation:address
                              andCLLocation:self.locationManager.location];
                             
                             [self dismissViewControllerAnimated:YES
                                                      completion:^{
                                                          
                                                      }];
                         }
                     }];
        
    } else if (self.locationManager.location) { // Places
        
        DetailMapViewController *contentController =
        [[UIStoryboard storyboardWithName:kMainStoryboardName bundle:nil]
         instantiateViewControllerWithIdentifier:kDetailMapViewControllerId];
        contentController.delegate = self.delegate;
        NSDictionary *dictLocation;
        
        if (self.searchBar.text.length > 0 ||
            (_isKeyboardShow && self.searchBar.text.length == 0)) {
            
            dictLocation = [self.searchArray objectAtIndex:indexPath.row];
            
        } else if (self.searchBar.text.length == 0 && !_isKeyboardShow) { // Search
            
            dictLocation = [self.sourceArray objectAtIndex:indexPath.row];
        }
        contentController.sourceDict = dictLocation;
        
        [self.navigationController pushViewController:contentController
                                             animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - MKMapViewDelegate

- (void)mapViewWillStartLocatingUser:(MKMapView *)mapView {
    
    // Check authorization status (with class method)
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // User has never been asked to decide on location authorization
    if (status == kCLAuthorizationStatusNotDetermined) {
        
        NSLog(@"Requesting when in use auth");
        [self.locationManager requestWhenInUseAuthorization];
    }
    // User has denied location use (either for this app or for all apps
    else if (status == kCLAuthorizationStatusDenied) {
        
        NSLog(@"Location services denied");
        // Alert the user and send them to the settings to turn on location
    }
}

- (void)mapView:(MKMapView *)mapView
didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    if (!CLLocationCoordinate2DIsValid(userLocation.coordinate)) {
        
        NSLog(@"User location is not valid 2d coordinates. maybe called in "
              @"background");
        
    } else {
        
        [self.locationManager stopUpdatingLocation];
        
        if (self.searchBar.text.length > 0) {
            
            [self searchLocationWithString:self.searchBar.text];
        } else {
            
            [self searchLocationWithString:@""];
        }
    }
    
    
    if(!_isUserLocation) {
        
        MKCoordinateRegion mapRegion;
        mapRegion.center = mapView.userLocation.coordinate;
        mapRegion.span.latitudeDelta = 0.005;
        mapRegion.span.longitudeDelta = 0.005;
        
        [mapView setRegion:mapRegion animated:YES];
        
        //показать пин только когда карта приблизится
        double delayInSeconds = 1.3f;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            
            _isUserLocation = YES;
            self.pinBackgroundView.hidden = NO;
            [self mapView:self.mapView regionDidChangeAnimated:YES];
        });
        
        
    }
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    MKAnnotationView *aV;
    
    // Hide annotation view of current location
    for (aV in views) {
        if ([aV.annotation isKindOfClass:[MKUserLocation class]]) {
            
            MKAnnotationView *annotationView = aV;
            annotationView.canShowCallout = NO;
        }
    }
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    
    self.popoverView.alpha = 0.0f;
    self.popoverView.hidden = YES;
    self.popoverView.userInteractionEnabled = NO;
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    if (self.pinBackgroundView.hidden == NO) {
        
        self.popoverView.hidden = NO;
        self.popoverView.userInteractionEnabled = YES;
        
        
        
        self.popoverView.alpha = 1.0f;
        
        
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    if (searchBar.text.length > 0) {
        
        [self searchLocationWithString:searchBar.text];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [self.searchBar setShowsCancelButton:NO animated:NO];
    self.searchBar.text = @"";
    self.isKeyboardShow = NO;
    [self.searchBar endEditing:YES];
    
    self.tableView.tableHeaderView = self.myHeaderView;
    
    [self.tableView reloadData];
    
    [self.searchArray removeAllObjects];
}

#pragma mark - Actions

- (IBAction)refreshButtonAction:(id)sender {
    
    if (self.searchBar.text.length > 0) {
        
        [self searchLocationWithString:self.searchBar.text];
    } else {
        
        [self searchLocationWithString:@""];
    }
}

- (IBAction)cancelButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)searchLocationWithString:(NSString *)searchString {
    
    _isLoad = YES;

[VKSERVICE searchLocationWithString:searchString currentLocation:self.locationManager.location :^(NSArray *itemsArray, NSError *error) {
    if(itemsArray){
        
        if (!searchString.length) {
            
            self.sourceArray = [NSMutableArray arrayWithArray:itemsArray];
            
        } else {
            
            self.searchArray = [NSMutableArray arrayWithArray:itemsArray];
        }

        self.tableView.tableHeaderView =
        [searchString isEqualToString:@""] ? self.myHeaderView : nil;
        [self.tableView reloadData];
       
        
    }
     _isLoad = NO;
}];

}

- (IBAction)popoverViewButtonAction:(id)sender { //Нажатие на пин
    
    CLLocationCoordinate2D center = self.mapView.centerCoordinate;
    CLLocation *location = [[CLLocation alloc] initWithLatitude:center.latitude
                                                      longitude:center.longitude];
    [VKSERVICE  getAddressFromLocation:location
                 complationBlock:^(NSString *address) {
                     
                     if (address) {
                         NSLog(@"Address:: %@ ", address);
                         [self.delegate setStringLocation:address
                                            andCLLocation:location];
                         
                         [self dismissViewControllerAnimated:YES
                                                  completion:^{
                                                      
                                                  }];
                     }
                 }];
}

-(void)showError {
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:@"Пожалуйста включите "
                                @"геолокацию!"
                                
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *settingsButton = [UIAlertAction
                                     actionWithTitle:@"Настройки геолокации"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action) {
                                         
                                             [[UIApplication sharedApplication]
                                              openURL:[NSURL URLWithString:
                                                       UIApplicationOpenSettingsURLString]];
                      
                                     }];
    [alert addAction:settingsButton];
    
    UIAlertAction *cancelButton =
    [UIAlertAction actionWithTitle:@"Отмена"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *action){
                               
                           }];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


@end
