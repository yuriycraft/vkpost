//
//  DetailMapViewController.h
//  VkPost
//
//  Created by book on 26.09.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MKMapView;

@interface DetailMapViewController : UITableViewController


@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *placeImageView;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIButton *stickButton;
@property (strong , nonatomic) NSDictionary *sourceDict;
@property(nonatomic, weak) id delegate;

- (IBAction)stickButtonAction:(id)sender;

@end


@class DetailMapViewController, CLLocation;

@protocol DetailMapViewControllerDelegate <NSObject>

- (void)setStringLocation:(NSString *)stringLocation
            andIdPlaces:(NSNumber *)place_id;
@end

