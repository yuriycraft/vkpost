//
//  DetailMapViewController.m
//  VkPost
//
//  Created by book on 26.09.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "DetailMapViewController.h"
#import "YCAnnotation.h"
#import <MapKit/MapKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

static NSString *identifier = @"Annotation";

@interface DetailMapViewController () <CLLocationManagerDelegate,
MKMapViewDelegate>



@property(strong, nonatomic) CLLocationManager *locationManager;
@property(strong, nonatomic) CLLocation *locationPlace;

@end

@implementation DetailMapViewController

@synthesize delegate;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initWithDictionary];
}

#pragma mark - Config

- (void)initWithDictionary {
    
    CLLocationCoordinate2D coord =
    CLLocationCoordinate2DMake([self.sourceDict[@"latitude"] floatValue],
                               [self.sourceDict[@"longitude"] floatValue]);
    self.locationPlace = [[CLLocation alloc] initWithLatitude:coord.latitude
                                                    longitude:coord.longitude];
    YCAnnotation *annotation = [[YCAnnotation alloc] init];
    
    annotation.coordinate = coord;
    
    MKCoordinateRegion mapRegion;
    mapRegion.center = coord;
    mapRegion.span.latitudeDelta = 0.0009;
    mapRegion.span.longitudeDelta = 0.0009;
    [self.mapView setRegion:mapRegion animated:YES];
    [self.mapView addAnnotation:annotation];
    [self.mapView setCenterCoordinate:coord];
    [self zoomPin];
    self.placeLabel.text =
    self.sourceDict[@"title"] ? self.sourceDict[@"title"] : @"";
    
    self.countLabel.text =
    [NSString stringWithFormat:@"%@", self.sourceDict[@"checkins"]];
    
    self.titleLabel.text =
    self.sourceDict[@"address"] ? self.sourceDict[@"address"] : @"";
    
    if (self.sourceDict[@"group_photo"]) {
        
        [self.placeImageView
         sd_setImageWithURL:[NSURL
                             URLWithString:self.sourceDict[@"group_photo"]]];
    } else {
        
        [self.placeImageView
         setImage:[UIImage imageNamed:@"Artboard 27"]];
    }
}

#pragma mark - Actions

- (IBAction)stickButtonAction:(id)sender {
    
    [self.delegate setStringLocation:self.sourceDict[@"title"]
                         andIdPlaces:self.sourceDict[@"id"]];
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}
- (void)zoomPin {
    
    MKMapRect zoomRect = MKMapRectNull;
    
    for (id<MKAnnotation> annotation in self.mapView.annotations) {
        
        CLLocationCoordinate2D coord = annotation.coordinate;
        MKMapPoint center = MKMapPointForCoordinate(coord);
        static double delta = 10000;
        MKMapRect rect =
        MKMapRectMake(center.x - delta, center.y - delta, delta * 2, delta * 2);
        zoomRect = MKMapRectUnion(zoomRect, rect);
    }
    zoomRect = [self.mapView mapRectThatFits:zoomRect];
    [self.mapView setVisibleMapRect:zoomRect];
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView
            viewForAnnotation:(id<MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        
        return nil;
    }

    MKPinAnnotationView *pin = (MKPinAnnotationView *)[mapView
                                                       dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if (!pin) {
        
        pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                              reuseIdentifier:identifier];
    } else {
        
        pin.annotation = annotation;
    }
    
    pin.animatesDrop = YES;
    pin.canShowCallout = YES;
    pin.draggable = YES;
    [pin setSelected:YES animated:YES];
    
    return pin;
}

@end
