//
//  MapViewController.h
//  VkPost
//
//  Created by book on 16.09.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MKMapView,CustomPopoverView;

static NSString *const kMapViewController = @"MapViewController";

@interface MapViewController : UIViewController

@property(weak, nonatomic) IBOutlet MKMapView *mapView;
@property(weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property(weak, nonatomic) IBOutlet UIView *myHeaderView;
@property(weak, nonatomic)
    IBOutlet NSLayoutConstraint *headerViewHeightConstraint;
@property(nonatomic, weak) id delegate;
@property (weak, nonatomic) IBOutlet UIImageView *pinImageView;
@property (weak, nonatomic) IBOutlet UIView *pinBackgroundView;
@property (weak, nonatomic) IBOutlet CustomPopoverView *popoverView;

- (IBAction)refreshButtonAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;
- (IBAction)popoverViewButtonAction:(id)sender;

@end


@class MapViewController, CLLocation;

@protocol MapViewControllerDelegate <NSObject>

- (void)setStringLocation:(NSString *)stringLocation
            andCLLocation:(CLLocation *)location;

@end
