//
//  YCVKShareSettingsController.h
//  VkPost
//
//  Created by book on 31.08.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "VKObject.h"
#import <UIKit/UIKit.h>


static NSString *const kSettingsViewController = @"YCVKShareSettingsController";
static NSString *const kSettingsTwitter = @"twitter";
static NSString *const kSettingsFacebook = @"facebook";
static NSString *const kSettingsLivejurnal = @"livejournal";

///----------------------------
/// @name Privacy settings class
///----------------------------
@interface YCVKPostSettings : VKObject

@property(nonatomic, strong) NSNumber *friendsOnly;
@property(nonatomic, assign) BOOL exportTwitter;
@property(nonatomic, assign) BOOL exportFacebook;
@property(nonatomic, assign) BOOL exportLivejournal;
@property(nonatomic, assign) BOOL toTime;
@property(nonatomic, strong) NSDate *date;

@end

@interface YCVKShareSettingsController : UITableViewController

@property(nonatomic, strong) YCVKPostSettings *currentSettings;
@property(nonatomic, strong) NSArray *rows;
@property(nonatomic, weak) id delegate;

- (void)initWithPostSettings:(YCVKPostSettings *)settings;
- (IBAction)doneBarButtonAction:(id)sender;

@end

@class YCVKShareSettingsController;

@protocol YCVKShareSettingsControllerDelegate <NSObject>

- (void)setupPostSettings:(YCVKPostSettings *)postSettings;

@end
