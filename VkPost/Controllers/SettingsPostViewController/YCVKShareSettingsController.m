//
//  YCVKShareSettingsController.m
//  VkPost
//
//  Created by book on 31.08.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "TimePickerTableViewCell.h"
#import "TimeTableViewCell.h"
#import "VKBundle.h"
#import "VKUtil.h"
#import "YCVKShareSettingsController.h"


static NSString *const YCSETTINGS_FRIENDS_ONLY = @"Только для друзей";
static NSString *const YCSETTINGS_TWITTER = @"ExportTwitter";
static NSString *const YCSETTINGS_FACEBOOK = @"ExportFacebook";
static NSString *const YCSETTINGS_LIVEJOURNAL = @"ExportLivejournal";
static NSString *const YCSETTINGS_TOTIME = @"Отложенная запись";

@interface YCVKPostSettings ()

@end

@implementation YCVKPostSettings
@end

@interface YCVKShareSettingsController ()

@end

@implementation YCVKShareSettingsController

@synthesize delegate;

- (void)viewDidLoad {

  [super viewDidLoad];

}

#pragma mark - initPostSettings

- (void)initWithPostSettings:(YCVKPostSettings *)settings {

  self.currentSettings = settings;
  NSMutableArray *newRows = [NSMutableArray new];

  if (self.currentSettings.exportTwitter)
    [newRows addObject:YCSETTINGS_TWITTER];
    
  if (self.currentSettings.exportFacebook)
    [newRows addObject:YCSETTINGS_FACEBOOK];
    
  if (self.currentSettings.exportLivejournal)
    [newRows addObject:YCSETTINGS_LIVEJOURNAL];
    
  self.rows = newRows;
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {

  if (self.currentSettings.friendsOnly && section == 0) {
    return 1;
      
  } else if (section == 1) {

    return self.rows.count;
      
  } else if (section == 2) {
      
      if(self.currentSettings.toTime) {
          
    return 3;
      }
      else {
          return 1;
      }
  }
  return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

  return 3;
}

- (nullable NSString *)tableView:(UITableView *)tableView
         titleForHeaderInSection:(NSInteger)section {

  switch (section) {

  case 1:
    return self.rows.count > 0 ? @"ЭКСПОРТ ЗАПИСИ" : nil;
    break;


  default:
          return  nil;
    break;
  }

  return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {

  if (indexPath.section == 0) {

    UITableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
    UISwitch *switchView;

    if (cell == nil) {

      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                    reuseIdentifier:@"SwitchCell"];
      cell.selectionStyle = UITableViewCellSelectionStyleNone;
      switchView = [[UISwitch alloc] initWithFrame:CGRectZero];

      cell.accessoryView = switchView;

      [switchView addTarget:self
                     action:@selector(switchChanged:)
           forControlEvents:UIControlEventValueChanged];
        
    } else {

      switchView = (UISwitch *)cell.accessoryView;
    }

    switchView.on = self.currentSettings.friendsOnly.boolValue;
      
    cell.textLabel.text = VKLocalizedString(YCSETTINGS_FRIENDS_ONLY);
    switchView.tag = 500;

    return cell;
  }
  else if (indexPath.section == 1) {

    UITableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
    UISwitch *switchView;
      
    if (cell == nil) {

      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                    reuseIdentifier:@"SwitchCell"];
      cell.selectionStyle = UITableViewCellSelectionStyleNone;
      switchView = [[UISwitch alloc] initWithFrame:CGRectZero];

      cell.accessoryView = switchView;

      [switchView addTarget:self
                     action:@selector(switchChanged:)
           forControlEvents:UIControlEventValueChanged];
    }
    else {

      switchView = (UISwitch *)cell.accessoryView;
    }
    NSString *currentRow = self.rows[(NSUInteger)indexPath.row];

    if ([currentRow isEqual:YCSETTINGS_TWITTER]) {

      switchView.on = self.currentSettings.exportTwitter;
    }
    else if ([currentRow isEqual:YCSETTINGS_FACEBOOK]) {

      switchView.on = self.currentSettings.exportFacebook;
    }
    else if ([currentRow isEqual:YCSETTINGS_LIVEJOURNAL]) {

      switchView.on = self.currentSettings.exportLivejournal;
    }
    switchView.enabled = !self.currentSettings.friendsOnly.boolValue;

    cell.textLabel.text = VKLocalizedString(currentRow);
    switchView.tag = 100 + indexPath.row;

    return cell;
  }
  else if (indexPath.section == 2) {

    if (indexPath.row == 0) {

      UITableViewCell *cell =
          [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
      UISwitch *switchView;

      if (cell == nil) {

        cell =
            [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                   reuseIdentifier:@"SwitchCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        switchView = [[UISwitch alloc] initWithFrame:CGRectZero];

        cell.accessoryView = switchView;

        [switchView addTarget:self
                       action:@selector(switchChanged:)
             forControlEvents:UIControlEventValueChanged];
        switchView.on = self.currentSettings.toTime;
      } else {

        switchView = (UISwitch *)cell.accessoryView;
      }

      cell.textLabel.text = VKLocalizedString(YCSETTINGS_TOTIME);
      switchView.tag = 600;

      return cell;
    } else if (indexPath.row == 1) {

      TimeTableViewCell *cell =
          [tableView dequeueReusableCellWithIdentifier:@"timeCell"];
      if (self.currentSettings.toTime) {
        [cell setLabelDate:self.currentSettings.date];
      } else {
        cell.timeLabel.text = @"";
      }

      cell.selectionStyle = UITableViewCellSelectionStyleNone;
      return cell;
    }

    else if (indexPath.row == 2) {

      TimePickerTableViewCell *cell =
          [tableView dequeueReusableCellWithIdentifier:@"toTimeCell"];
     
        [cell.datePicker setDate:self.currentSettings.date];
        [cell setMinimumDateForDatePicker];
      
      cell.datePicker.userInteractionEnabled = self.currentSettings.toTime;

      [cell.datePicker addTarget:self
                          action:@selector(dateIsChanged:)
                forControlEvents:UIControlEventValueChanged];

      cell.selectionStyle = UITableViewCellSelectionStyleNone;
      return cell;
    }
  }

  return nil;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {

  switch (indexPath.section) {

  case 2:
    if (indexPath.row == 2) {
      return 200;
    }
    break;

  default:
            return 44;
    break;
  }
  return 44;
}

#pragma mark - Actions

- (IBAction)doneBarButtonAction:(id)sender {

  [self.delegate setupPostSettings:self.currentSettings];

  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dateIsChanged:(UIDatePicker *)sender {
    
    if(self.currentSettings.toTime) {
        
  self.currentSettings.date = sender.date;
  [self.tableView beginUpdates];
  [self.tableView
      reloadRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:1 inSection:2] ]
            withRowAnimation:UITableViewRowAnimationNone];
  [self.tableView endUpdates];
        
    }
}

- (void)switchChanged:(UISwitch *)sender {

  if (sender.tag == 500) { //if push button friends only

    self.currentSettings.friendsOnly = @(sender.on);
    [self.tableView performSelector:@selector(reloadData)
                         withObject:nil
                         afterDelay:0.3];
  }

  else if (sender.tag == 600) {//if switch to time
        [self.tableView reloadData];
      NSDate *nowDate = [NSDate date];
      

      NSTimeInterval seconds = ceil([nowDate timeIntervalSinceReferenceDate]/300.0)*300.0;
      NSDate *rounded = [NSDate dateWithTimeIntervalSinceReferenceDate:seconds];
      
     
      
      
    self.currentSettings.date = [NSDate dateWithTimeInterval:14400 sinceDate:rounded];
    self.currentSettings.toTime = sender.on;
    

      [self.tableView reloadData];
  }

  else { //if push buttons shared to social networking

    NSString *currentRow = self.rows[(NSUInteger)(sender.tag - 100)];

    if ([currentRow isEqual:YCSETTINGS_TWITTER]) {

      self.currentSettings.exportTwitter = sender.on;
    } else if ([currentRow isEqual:YCSETTINGS_FACEBOOK]) {

      self.currentSettings.exportFacebook = sender.on;
    } else if ([currentRow isEqual:YCSETTINGS_LIVEJOURNAL]) {

      self.currentSettings.exportLivejournal = sender.on;
    }
  }
}

@end
