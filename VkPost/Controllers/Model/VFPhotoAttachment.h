//
//  VFPhoto.h
//  VK-Feed
//
//  Created by Dmitriy Kluev on 08.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VFAttachmentSize.h"


@interface VFPhotoAttachment : VFAttachmentSize

@property(nonatomic, strong) NSNumber *idx;
@property(nonatomic, strong) NSNumber *album_id;
@property(nonatomic, strong) NSNumber *owner_id;
@property(nonatomic, strong) NSNumber *user_id;
@property(nonatomic, strong) NSString *photo_75;
@property(nonatomic, strong) NSString *photo_130;
@property(nonatomic, strong) NSString *photo_604;
@property(nonatomic, strong) NSString *photo_807;
@property(nonatomic, strong) NSString *photo_1280;
@property(nonatomic, strong) NSString *photo_2560;
@property(nonatomic, strong) NSString *text;
@property(nonatomic, strong) NSNumber *date;
@property(strong, nonatomic) NSString *access_key;

#warning uncomment
//@property(nonatomic, strong) VFLikesCounters *likes ;
@property(nonatomic, assign) bool can_comment ;
@property(nonatomic, assign) bool can_repost ;
@property(nonatomic, strong) NSNumber *comments_count;

@property(nonatomic, assign) BOOL selectedStatus;

-(id)initWithDictionary:(NSDictionary *)dictionary ;


@end
