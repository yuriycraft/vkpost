//
//  YCAlbumModel.m
//  example
//
//  Created by book on 19.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "YCAlbumModel.h"

@implementation YCAlbumModel

-(id)initWithDictionary:(NSDictionary*) request {
    
    self = [super init];
    if(self) {
        
        self.album_id = request[@"id"];
        self.owner_id = request[@"owner_id"];
        self.countPhotos = [request[@"size"]integerValue];
        self.thumb_id = request[@"thumb_id"];

        self.title = [self.album_id isEqual:@"-1"] ? @"Все фотографии" : request[@"title"] ;
  
        
    }
    return self;
    
}

@end
