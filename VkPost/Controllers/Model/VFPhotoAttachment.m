//
//  VFPhoto.m
//  VK-Feed
//
//  Created by Dmitriy Kluev on 08.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "VFPhotoAttachment.h"

@implementation VFPhotoAttachment

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if(self)
    {
        self.idx = [dictionary objectForKey:@"id"];
        self.album_id = [dictionary objectForKey:@"album_id"];
        self.owner_id = [dictionary objectForKey:@"owner_id"];
        self.photo_75 = [dictionary objectForKey:@"photo_75"];
        self.photo_130 = [dictionary objectForKey:@"photo_130"];
        self.photo_604 = [dictionary objectForKey:@"photo_604"];
        self.photo_807 = [dictionary objectForKey:@"photo_807"];
        self.photo_1280 = [dictionary objectForKey:@"photo_1280"];
        self.photo_2560 = [dictionary objectForKey:@"photo_2560"];
        self.text = [dictionary objectForKey:@"text"];
        self.date = [dictionary objectForKey:@"date"];
        self.access_key = [dictionary objectForKey:@"access_key"];
        
        self.selectedStatus = NO;
        
        if ([dictionary objectForKey:@"user_id"]) {
            self.user_id =  [dictionary objectForKey:@"user_id"];
        }
        if ([dictionary objectForKey:@"likes"]) {
#warning uncomment
//            self.likes = [[VFLikesCounters alloc]initWithDictionary:[dictionary objectForKey:@"likes"]];
        }
        if ([dictionary objectForKey:@"comments"]) {
            self.comments_count = dictionary[@"comments"][@"count"];
        }
        if ([dictionary objectForKey:@"can_comment"]) {
            self.can_comment = [dictionary[@"can_comment"] boolValue];
        }
        if ([dictionary objectForKey:@"can_repost"]) {
            self.can_repost = [dictionary[@"can_repost"] boolValue];
        }
        
        if ([dictionary objectForKey:@"width"]) {
            self.width = [dictionary objectForKey:@"width"];
            self.height = [dictionary objectForKey:@"height"];
        }else{
            self.width = @100 ;
            self.height = @100 ;
        }
    
        self.frame = [[VFRect alloc]init] ;
    }
    
    return self;
}

@end
