//
//  VFAttachmentSize.h
//  VK-Feed
//
//  Created by Dmitriy Kluev on 26.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGGeometry.h>
#warning unComment
//#import "VFCommentsCounters.h"
//#import "VFLikesCounters.h"
//#import "VFRepostsCounters.h"

@interface VFRect : NSObject
@property(nonatomic) NSInteger width;
@property(nonatomic) NSInteger height;
@property(nonatomic) NSInteger x;
@property(nonatomic) NSInteger y;
@end


@interface VFAttachmentSize : NSObject

@property (nonatomic, strong) VFRect *frame ;

@property (nonatomic, assign) CGRect portretFrame ;
@property (nonatomic, assign) CGRect landscapeFrame ;

@property(nonatomic, strong) NSNumber *width;
@property(nonatomic, strong) NSNumber *height;

@end
