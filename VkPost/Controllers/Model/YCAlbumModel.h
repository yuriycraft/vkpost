//
//  YCAlbumModel.h
//  example
//
//  Created by book on 19.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YCAlbumModel : NSObject

@property(strong,nonatomic)NSNumber *album_id;
@property(strong,nonatomic)NSString *owner_id;
@property(assign,nonatomic)NSInteger countPhotos;
@property(strong,nonatomic)NSString *thumb_id;
@property(strong,nonatomic)NSString *title;
@property(strong,nonatomic)NSMutableArray *photosArray;
@property(assign,nonatomic)NSInteger offset;
@property(assign, nonatomic)BOOL isAllPhotosLoaded;

-(id)initWithDictionary:(NSDictionary*) request;
    

@end
