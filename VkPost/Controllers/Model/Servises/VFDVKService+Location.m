//
//  VFDVKService+Location.m
//  VkPost
//
//  Created by book on 06.02.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "VFDVKService+Location.h"
#import <CoreLocation/CoreLocation.h>

@implementation VFDVKService (Location)

#pragma mark - MapViewController

- (void)getAddressFromLocation:(CLLocation *)location
               complationBlock:(addressCompletionBlock)completionBlock {
    
    // Example URL
    // NSString *urlString =
    // @"http://maps.googleapis.com/maps/api/geocode/json?latlng=23.033915,72.524267&sensor=true_or_false";
    
    NSString *urlString = [NSString
                           stringWithFormat:@"httpS://maps.googleapis.com/maps/api/geocode/"
                           @"json?latlng=%f,%f&sensor=true_or_false",
                           location.coordinate.latitude,
                           location.coordinate.longitude];
    
    NSError *error;
    NSString *locationString =
    [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString]
                             encoding:NSUTF8StringEncoding
                                error:&error];
    NSArray *jsonObject = [NSJSONSerialization
                           JSONObjectWithData:[locationString dataUsingEncoding:NSUTF8StringEncoding]
                           options:0
                           error:NULL];
    
    NSString *strFormatedAddress = [[[jsonObject valueForKey:@"results"]
                                     objectAtIndex:0] valueForKey:@"formatted_address"];
    completionBlock(strFormatedAddress);
}



- (void)searchLocationWithString:(NSString *)searchString
                 currentLocation:(CLLocation*)location
                                :(void (^)(NSArray *itemsArray,NSError *error)) completionHandler  {
    
    float longitude = location.coordinate.longitude;
    float latitude = location.coordinate.latitude;
    
    NSDictionary *params = @{
                             @"q" : searchString,
                             @"latitude" : @(latitude),
                             @"longitude" : @(longitude),
                             @"radius" : searchString.length ? @4 : @""
                             };
    
    
    VKRequest *getLocationSearch =
    [VKRequest requestWithMethod:@"places.search"
                      parameters:params];
    
    [getLocationSearch executeWithResultBlock:^(VKResponse *response) {
        
        
        NSArray *array = response.json[@"items"];
        completionHandler(array,nil);
        
    }
                                   errorBlock:^(NSError *error) {
                                       
                                       if (error.code != VK_API_ERROR) {
                                           
                                           [error.vkError.request repeat];
                                       } else {
                                           
                                           completionHandler(nil,error);
                                           NSLog(@"VK error: %@", error);
                                       }
                                   }];
}


@end
