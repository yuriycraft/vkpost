//
//  VFDVKService.h
//  VKMessenger
//
//  Created by Dok on 08.08.16.
//  Copyright © 2016 mrook. All rights reserved.
//

#import "VKSdk.h"
#import <DOSingleton/DOSingleton.h>

// Notifications
#define PHOTO_LOADING_DID_END_NOTIFICATION @"PHOTO_LOADING_DID_END_NOTIFICATION"
#define PHOTO_PROGRESS_NOTIFICATION @"PHOTO_PROGRESS_NOTIFICATION"
#define PHOTO_UPLOAD_ERROR_NOTIFICATION @"PHOTO_UPLOAD_ERROR_NOTIFICATION"

@class VFDCurrentUser, VFDViewController, AFURLSessionManager ;

#define VKSERVICE [VFDVKService sharedInstance]

@protocol VFDVKServiceDelegate <NSObject>

//- (void)updateViewController;

@end

@interface VFDVKService : DOSingleton <VKSdkDelegate, VKSdkUIDelegate>

@property (readonly, nonatomic) NSArray *users;
@property (strong, nonatomic) VFDCurrentUser *currentUser;
@property (readonly, nonatomic) BOOL existVkApp;
@property (nonatomic, weak) UIViewController *currentVC;

@property(strong, nonatomic) VKResponse *accountResponse;
@property(weak, nonatomic) id<VFDVKServiceDelegate> delegateAccount;




///- (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication;

//Initializations
//- (void) initSDK;

#warning uncomment all
//Login
//- (void) checkLoginWithCompletion:(VFDSuccessBlock)completion;
//- (void) loginWithApp:(BOOL)withApp completion:(VFDSuccessBlock)completion;
//- (void) loginWithUser:(VFDCurrentUser *)user completion:(VFDSuccessBlock)completion;

//Logout
//- (void) changeUser;
//- (void) logout;

//Current user info
//- (void) getCurrentUserInfoWithCompletion:(VFDCurrentUserBlock)completion;
//- (void) getCurrentUsersFriendsFrom:(VFDViewController*)viewController offset:(NSNumber*)offset completion:(VFDFriendsArrayBlock)completion;
//- (void) searchFriendsFrom:(VFDViewController *)viewController withQuery:(NSString*)query offset:(NSNumber*)offset completion:(VFDFriendsArrayBlock)completion;
//
////Friends info
//- (void) getFriendsFrom:(VFDViewController*)viewController forUser:(VFDUser*)user offset:(NSNumber*)offset withCompletion:(VFDFriendsArrayBlock)completion;

-(void)errorHandler:(NSError *) error;
@end
