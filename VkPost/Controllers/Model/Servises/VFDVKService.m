//
//  VFDVKService.m
//  VKMessenger
//
//  Created by Dok on 08.08.16.
//  Copyright © 2016 mrook. All rights reserved.
//

#import "VFDVKService.h"
#import "VFDVKService+Photo.h"
#import <AFNetworking/AFNetworking.h>

#warning uncomment all

//#import <VKAuthorizeController.h>
//#import "VFDVKService+MJExtension.h"
//#import "VFDVKService+AbstractRequest.h"
//#import "VFDCurrentUser.h"
//#import "VFDViewController.h"
//#import <VK-ios-sdk/VKSdk.h>
//#import "Preferences.h"
//#import "VFDVKService+Users.h"
//
//#define vkScope @[@"wall",@"users",@"photos",@"video",@"friends",@"messages",@"docs",@"status",@"notifications",@"stats",@"notes",@"notify",@"offline",@"groups"]//@[@"friends", @"email", @"photos", @"messages"]
//#define vkMaxFriends @25
//
@implementation VFDVKService {
//    //Private ivars
//#warning uncomment
// //   VFDSuccessBlock _authCompletionBlock;
}
//
//#pragma mark - Initialization
//
//- (void)initSDK {
//    [[VKSdk initializeWithAppId:VFDConstantsVKAppId apiVersion:@"5.53"] registerDelegate:self];
//    [[VKSdk instance] setUiDelegate:self];
//    if(self.currentUser.accessToken)
//    {
//        [VKSdk setAccessToken:[self.currentUser getVKAccessToken]];
//    } else {
//        [VKSdk forceLogout];
//    }
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        [self usersUpdateWithCompletion:nil];
//    });
//}
//
//#pragma mark - Session work
//
//- (NSArray *)users
//{
//    // TODO: create `fold` extension
//    
//    NSMutableArray *array = [NSMutableArray array];
//    RLMResults *results = [VFDCurrentUser allObjectsInRealm:[RLMRealm defaultRealm]];
//    for (VFDCurrentUser *session in results) {
//        [array addObject:session];
//    }
//    
//    return array;
//}
//
//- (void)setCurrentUser:(VFDCurrentUser *)currentUser
//{
//    NSNumber *curId = [currentUser currentId];
//    [REALMSERVICE saveTransactionWithBlock:^(RLMRealm *realm) {
//        RLMResults *res = [VFDCurrentUser allObjectsInRealm:realm];
//        for (VFDCurrentUser *user in res) {
//            user.current = NO;
//        }
//        
//        if (curId) {
//            VFDCurrentUser *user = [[VFDCurrentUser objectsWhere:@"currentId = %d", [curId intValue]] firstObject];
//            user.current = YES;
//        }
//        
//    }];
//}
//
//- (void) removeUser:(VFDCurrentUser*)user {
//    [REALMSERVICE saveTransactionWithBlock:^(RLMRealm *realm) {
//        [realm deleteObject:user];
//    }];
//}
//
//- (VFDCurrentUser*) currentUser {
//    return ((VFDCurrentUser*)[[VFDCurrentUser objectsWhere:@"current = YES"] firstObject]);
//}
//
//- (BOOL)existVkApp
//{
//    return [VKSdk vkAppMayExists];
//}
//
//#pragma mark - Public
//
//- (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
//{
//    [VKSdk processOpenURL:url fromApplication:sourceApplication];
//    return YES;
//}
//
//#pragma mark - Login in
//
//- (void)checkLoginWithCompletion:(VFDSuccessBlock)completion {
//    
//    if ([VKSdk isLoggedIn]) {
//        completion(YES, nil);
//    } else {
//        [VKSdk wakeUpSession:vkScope completeBlock:^(VKAuthorizationState state, NSError *error) {
//            
//            completion((state == VKAuthorizationAuthorized), error);
//            
//        }];
//    }
//}
//
//- (void)loginWithApp:(BOOL)withApp completion:(VFDSuccessBlock)completion {
//    
//    _authCompletionBlock = completion;
//    
//    if (withApp) {
//        [VKSdk authorize:vkScope];
//    } else {
//        [VKAuthorizeController presentForAuthorizeWithAppId:VFDConstantsVKAppId
//                                             andPermissions:vkScope
//                                               revokeAccess:YES
//                                                displayType:VK_DISPLAY_IOS];
//    }
//}
//
//- (void)loginWithUser:(VFDCurrentUser *)user completion:(VFDSuccessBlock)completion {
//    
//    _authCompletionBlock = completion;
//    
//    [VKSdk setAccessToken:[user getVKAccessToken]];
//    [self checkLoginWithCompletion:^(BOOL success, NSError * _Nullable error) {
//        if (success) {
//            [self setCurrentUser:user];
//        }
//        [self callAuthCompletionWithSuccess:success error:error];
//    }];
//}
//
//- (void)logout {
//    [VKSdk forceLogout];
//    
//    [self removeUser:self.currentUser];
//    self.currentUser = nil;
//}
//
//- (void) changeUser {
//    [VKSdk forceLogout];
//    
//    self.currentUser = nil;
//}
//
//#pragma mark - auth delegates
//
//- (void) callAuthCompletionWithSuccess:(BOOL)success error:(NSError*)error {
//    if (_authCompletionBlock) {
//        _authCompletionBlock(success, error);
//        _authCompletionBlock = nil;
//    }
//}
//
//#pragma mark - Current User info
//
//- (void)getCurrentUserInfoWithCompletion:(VFDCurrentUserBlock)completion {
//    NSArray *fields = [self fieldsForObjectClass:[VFDUser class]];
//    VKRequest *userRequest = [[VKApi users] get:@{VK_API_FIELDS:[fields componentsJoinedByString:@","]}];
//    [userRequest executeWithResultBlock:^(VKResponse *response) {
//        
//        NSLog(@"VK Response: %@", response.json);
//        
//        VKUsersArray *usersArr = response.parsedModel;
//        VKUser *user = [usersArr.items firstObject];
//        //Hack for previous user model
//        DEFAULTS.user = response.json[0];
//        [REALMSERVICE updateCurrentUserWith:user withCompletion:completion];
//        [LONGPOLLMANAGER connectToLongPoll];
//        
//    } errorBlock:^(NSError *error) {
//        completion(nil, error);
//    }];
//}
//
//- (void)getCurrentUsersFriendsFrom:(VFDViewController *)viewController offset:(NSNumber *)offset completion:(VFDFriendsArrayBlock)completion {
//    
//    [self getFriendsFrom:viewController forUser:self.currentUser.user offset:offset withCompletion:completion];
//    
//}
//
//#pragma mark - Friends info
//
//- (void)getFriendsFrom:(VFDViewController *)viewController forUser:(VFDUser *)user offset:(NSNumber *)offset withCompletion:(VFDFriendsArrayBlock)completion {
//
//    NSArray *fields = [self fieldsForObjectClass:[VFDUser class]];
//    VKRequest *friendsRequest = [[VKApi friends] get:@{VK_API_FIELDS:fields,
//                                                       VK_API_USER_ID:user.id,
//                                                       VK_API_COUNT:vkMaxFriends,
//                                                       VK_API_OFFSET:offset,
//                                                       VK_API_ORDER: @"hints"}];
//    
//    [friendsRequest executeWithResultBlock:^(VKResponse *response) {
//        
//        [self getObjectAndCountFromResponse:response objectClass:[VFDUser class] completion:^(id object, NSNumber *count) {
//            [viewController removeDependentRequest:friendsRequest];
//        
//            
//             completion(object, count, nil);
//        }];
//        
//        
//    } errorBlock:^(NSError *error) {
//        [viewController removeDependentRequest:friendsRequest];
//        completion(nil, 0, error);
//    }];
//    
//    [viewController addDependentRequest:friendsRequest];
//}
//
//- (void)searchFriendsFrom:(VFDViewController *)viewController withQuery:(NSString *)query offset:(NSNumber *)offset completion:(VFDFriendsArrayBlock)completion {
//    NSArray *fields = [self fieldsForObjectClass:[VFDUser class]];
//    VKRequest *searchRequest = [VKApi requestWithMethod:@"friends.search" andParameters:@{VK_API_FIELDS:fields,
//                                                                                          VK_API_Q: query,
//                                                                                          VK_API_COUNT:vkMaxFriends,
//                                                                                          VK_API_OFFSET:offset}];
//    [searchRequest executeWithResultBlock:^(VKResponse *response) {
//        [self getObjectAndCountFromResponse:response objectClass:[VFDUser class] completion:^(id object, NSNumber *count) {
//            [viewController removeDependentRequest:searchRequest];
//            completion(object, count, nil);
//        }];
//    } errorBlock:^(NSError *error) {
//        [viewController removeDependentRequest:searchRequest];
//        completion(nil, 0, error);
//    }];
//    
//    [viewController addDependentRequest:searchRequest];
//}
//
//#pragma mark -
//#pragma mark - Delegates
//
//#pragma mark - VKSdkDelegate
//
//-(void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
//    if (result.state==VKAuthorizationAuthorized) {
//       // [self callAuthCompletionWithSuccess:YES error:nil];
//    } else if (result.error) {
//        [self callAuthCompletionWithSuccess:NO error:result.error];
//    }
//}
//
//- (void)vkSdkAuthorizationStateUpdatedWithResult:(VKAuthorizationResult *)result {
//    if (result.state==VKAuthorizationAuthorized) {
//       // [self callAuthCompletionWithSuccess:YES error:nil];
//    } else if (result.state==VKAuthorizationError) {
//        [self callAuthCompletionWithSuccess:NO error:result.error];
//    }
//}
//
//- (void)vkSdkUserAuthorizationFailed {
//    [self callAuthCompletionWithSuccess:NO error:nil];
//}
//
//- (void)vkSdkAccessTokenUpdated:(VKAccessToken *)newToken oldToken:(VKAccessToken *)oldToken {
//    if (!newToken || ![VKSdk isLoggedIn] || [newToken.accessToken isEqualToString:oldToken.accessToken]) {
//        return;
//    }
//    
//    [self getCurrentUserInfoWithCompletion:^(VFDCurrentUser * _Nullable currentUser, NSError * _Nullable error) {
//        if (!error) {
//            self.currentUser = currentUser;
//            [REALMSERVICE saveTransactionWithBlock:^(RLMRealm *realm) {
//                [self.currentUser setVKAccessToken:newToken];
//                [realm addOrUpdateObject:self.currentUser];
//            }];
//            [self callAuthCompletionWithSuccess:YES error:nil];
//        }
//         [self callAuthCompletionWithSuccess:NO error:error];
//    }];
//}
//
//#pragma mark - VKSdkUiDelegate
//
//- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
//    //TODO:VK present
//   // [ROUTER presentModalViewController:controller];
//    [_currentVC presentViewController:controller animated:YES completion:nil];
//}
//
//- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
//{
//    //TODO:VK Present
//    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
//    [_currentVC presentViewController:vc animated:YES completion:nil];
//   // [vc presentIn:ROUTER.navController];
//}
//

-(void)errorHandler:(NSError *) error {
    
    if (error.code != VK_API_ERROR) {
        
        [error.vkError.request repeat];
    }
    else {
        
        NSLog(@"VK error: %@", error);
    }
}
@end
