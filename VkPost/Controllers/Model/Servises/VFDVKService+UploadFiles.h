//
//  VFDVKService+UploadFiles.h
//  VkPost
//
//  Created by book on 03.02.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "VFDVKService.h"
#import <AFNetworking/AFNetworking.h>


@class VKRequest ,YCVKUploadingAttachment, VKResponse;

@interface VFDVKService (UploadFiles)

@property(nonatomic, strong) AFURLSessionManager *_manager;


//Upload files to vk server
-(VKRequest*)uploadRequestPhotoOrPollWithAttach:(YCVKUploadingAttachment*)attach;

-(void)uploadVideoOrDocsWithUrl:(NSString*)url  andFileUploading:(id)uploadObj andAttachment:(YCVKUploadingAttachment*)attach;

-(void) getServerAdressForVideoOrDocumentUploadWithAttach:(YCVKUploadingAttachment*)attach;


@end
