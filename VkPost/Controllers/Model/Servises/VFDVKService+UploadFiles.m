//
//  VFDVKService+UploadFiles.m
//  VkPost
//
//  Created by book on 03.02.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "VFDVKService+UploadFiles.h"
#import "YCVKUploadingAttachment.h"
#import "VKHTTPOperation.h"
#import "VKHTTPClient.h"
#import "YCVKPollModel.h"
#import "YCVKVideoUploadingModel.h"
#import <AFNetworking/AFNetworking.h>
#import "VFDDocAttachment.h"
#import "VFDVideoAttachment.h"
#import <objc/runtime.h>

static char managerKey;

static NSString *const kBoundary = @"Boundary(======VK_SDK======)";

@implementation VFDVKService (UploadFiles)

#pragma mark - setter & gette for ivar _manager

- (AFURLSessionManager*)_manager
{
    return objc_getAssociatedObject(self, &managerKey);
}

- (void)set_manager:(AFURLSessionManager *)_manager
{
    objc_setAssociatedObject(self, &managerKey, _manager, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Create UploadRequest for Docs or Video
- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method

                                                   path:(NSString *)path
                                           uploadObject:(id)uploadObj
                                               mimeType:(NSString*)mimeType
                                             uploadData:(NSData*)uploadData
                                              extension:(NSString*)extension{
    NSParameterAssert(method);
    NSParameterAssert(![method isEqualToString:@"GET"] && ![method isEqualToString:@"HEAD"]);
    
    NSMutableURLRequest *request = [[VKHTTPClient getClient]requestWithMethod:method path:path parameters:nil secure:YES];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", kBoundary];
    
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *postbody = [NSMutableData data];
    
    
    NSString *fileName = [NSString stringWithFormat:@"file"];
    
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", kBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.%@\"\r\n", fileName, fileName, extension] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postbody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimeType] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postbody appendData:uploadData];
    
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", kBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:postbody];
    
    
    return request;
}


#warning AFNetworking
#pragma mark - Execute uploadRequest for Docs or Videos

-(void)uploadVideoOrDocsWithUrl:(NSString*)url  andFileUploading:(id)uploadObj andAttachment:(YCVKUploadingAttachment*)attach {
    
    NSMutableURLRequest *request = [self multipartFormRequestWithMethod:@"POST" path:url uploadObject:uploadObj mimeType:[attach mimeType] uploadData:attach.uploadData extension:attach.ext];
    
    if(!self._manager){
        
        NSURLSessionConfiguration * config = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        self._manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:config];
        self._manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
    }
    
    
    NSURLSessionUploadTask *uploadTask;
    
    
    
    uploadTask = [self._manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                          attach.status = kUploading;
                          attach.progressUploading = uploadProgress.fractionCompleted;
                          [[NSNotificationCenter defaultCenter] postNotificationName:ATTACHMENT_PROGRESS_NOTIFICATION object:attach];
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      if (error) {
                          
                          attach.status = kErrorUploading;
                          attach.uploadingRequest = nil;
                          
                          
                          [[NSNotificationCenter defaultCenter]postNotificationName:ATTACHMENT_UPLOAD_ERROR_NOTIFICATION object:attach];
                          
                      } else {
                          
                       //                            NSLog(@"%@",responseObject);
                          if(responseObject[@"file"] && [uploadObj isKindOfClass:[VFDDocAttachment class]]) {
                              
                              [self saveDocumentUploadingFileOnVkWithFileString:responseObject[@"file"] andAttach:attach];
                          }
                          attach.uploadingRequest = nil;
                          attach.status = kDoneUploading;
                          
                          [[NSNotificationCenter defaultCenter] postNotificationName:ATTACHMENT_LOADING_DID_END_NOTIFICATION
                                                                              object:attach];
                      }
                  }];
    
    
    [uploadTask resume];
    
}

//Получение адреса сервера для загрузки на него файла или видео

-(void) getServerAdressForVideoOrDocumentUploadWithAttach:(YCVKUploadingAttachment*)attach {
    
    NSString * vkMethodString =  [self methodNameStringForForGettingServerUploadingVideoOrDocsWithAttach:attach];
    
    NSDictionary * paramsDict =  [self createParametersForGettingServerUploadingVideoOrDocsWithAttach:attach];
    
    VKRequest * getAdressServer =  [VKRequest requestWithMethod:vkMethodString
                                                     parameters:paramsDict
                                    ];
    
    [getAdressServer executeWithResultBlock:^(VKResponse *response) {
        
        [attach initUploadingInfoWithDictionary:response.json];
        
        
        if(attach.upload_url) {
            
            id uploadingObject = attach.video ? :attach.document;
            
            dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                
                [self uploadVideoOrDocsWithUrl:attach.upload_url andFileUploading:uploadingObject andAttachment:attach];
                
            });
        }
    } errorBlock:^(NSError *error) {
        
        
        attach.status = kErrorUploading;
        [[NSNotificationCenter defaultCenter]postNotificationName:ATTACHMENT_UPLOAD_ERROR_NOTIFICATION object:attach];
        //   NSLog(@"%@",error);
        
    }];
}

//Получение названия метода в зависимости от типа (Видео или документ)
-(NSString*)methodNameStringForForGettingServerUploadingVideoOrDocsWithAttach:(YCVKUploadingAttachment*)attach {
    
    NSString * vkMethodString;
    
    if(attach.type == kVideoType) {
        
        vkMethodString = @"video.save";
        
    } else  if(attach.type == kDocumentType) {
        
        vkMethodString = @"docs.getWallUploadServer";
    }
    return vkMethodString;
}

//Create parameter for request getting uploading url
-(NSDictionary*)createParametersForGettingServerUploadingVideoOrDocsWithAttach:(YCVKUploadingAttachment*)attach {
    
    NSDictionary* params;
    
    if(attach.type == kVideoType) {
        
        params=   @{
                    
                    @"name" : attach.video.name ? attach.video.name:@"",
                    
                    @"is_private" : @(attach.video.is_private),
                    
                    @"wallpost" : @(attach.video.wallpost),
                    
                    @"privacy_view" : @"",
                    
                    @"no_comments" : @(attach.video.no_comments),
                    
                    @"repeat" : @(attach.video.repeat)
                    
                    };
    }
    return params;
}

#pragma mark - Save Doc
-(void)saveDocumentUploadingFileOnVkWithFileString:(NSString*)fileString andAttach:(YCVKUploadingAttachment*)attach {
    
    NSDictionary * params = attach.document.title ? @{ @"file":fileString,@"title": attach.document.title } : @{@"file":fileString};
    
    VKRequest *saveRequest = [VKRequest requestWithMethod:@"docs.save" parameters:params];
    
    
    [saveRequest executeWithResultBlock:^(VKResponse *response) {
        
        NSLog(@"%@",response);
        
        NSDictionary *res = [[response json] firstObject];
        
        if([res objectForKey:@"id"]){
            
            attach.attachmentString = [NSString stringWithFormat:@"doc%@_%@",[res objectForKey:@"owner_id"],[res objectForKey:@"id"]];
        }
        
    } errorBlock:^(NSError *error) {
        
        NSLog(@"%@",error);
        
        if (error.code != VK_API_ERROR) {
            
            [error.vkError.request repeat];
        } else {
            
            attach.status = kErrorUploading;
            
            [[NSNotificationCenter defaultCenter]postNotificationName:ATTACHMENT_UPLOAD_ERROR_NOTIFICATION object:attach];
        }
    }];
    
    
}

#pragma mark - uploadRequestWithAttach для фото и опроса

-(VKRequest*)uploadRequestPhotoOrPollWithAttach:(YCVKUploadingAttachment*)attach
{
    
    VKRequest* uploadRequest = [self createUploadRequestForPhotoAndPollTypesWithAttach:attach];
    
    
    [uploadRequest setCompleteBlock:^(VKResponse *res) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            if(attach.type == kPhotoType){
                
                VKPhoto *photo = [res.parsedModel firstObject];
                attach.attachmentString = photo.attachmentString;
                
            } else if (attach.type == kPollType) {
                
                
                YCVKPollModel * poll = [[YCVKPollModel alloc]initWithDictionary:res.json];
                
                
                attach.attachmentString =  [NSString stringWithFormat:@"poll%@_%@", poll.owner_id, poll.idx];
                
            }
            
            
            attach.uploadingRequest = nil;
            attach.status = kDoneUploading;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:ATTACHMENT_LOADING_DID_END_NOTIFICATION
                                                                object:attach];
        });
    }];
    
    
    [uploadRequest setProgressBlock:^(VKProgressType progressType, long long bytesLoaded, long long bytesTotal) {
        
        if (bytesTotal < 0) {
            return;
        }
        
        if (bytesTotal > 0) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                attach.status = kUploading;
                attach.progressUploading = bytesLoaded / (float)bytesTotal;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:ATTACHMENT_PROGRESS_NOTIFICATION object:attach];
            });
        }
        
    }];
    
    
    
    [uploadRequest setErrorBlock:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"%@",error);
            attach.status = kErrorUploading;
            
            attach.uploadingRequest = nil;
            
            [[NSNotificationCenter defaultCenter]postNotificationName:ATTACHMENT_UPLOAD_ERROR_NOTIFICATION object:attach];
        });
    }];
    
    [uploadRequest start];
    
    return uploadRequest;
}


-(VKRequest*)createUploadRequestForPhotoAndPollTypesWithAttach:(YCVKUploadingAttachment*)attach {
    
    VKRequest *uploadRequest;

    if(attach.type == kPhotoType) {
        
        uploadRequest = [VKApi uploadWallPhotoRequest:attach.uploadingImage.sourceImage
                                           parameters:attach.uploadingImage.parameters
                                               userId:0
                                              groupId:0];
        
        
    } else if (attach.type == kPollType) {
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:attach.poll.answersArray options:NSJSONWritingPrettyPrinted error:nil];
        
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        uploadRequest =
        [VKRequest requestWithMethod:@"polls.create"
                          parameters:@{
                                       @"question" : attach.poll.question,
                                       @"is_anonymous" : @(attach.poll.is_anonymous),
                                       @"add_answers" : jsonString
                                       
                                       }
         ];
        
    }
    
    return uploadRequest;
}

@end
