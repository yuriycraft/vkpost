//
//  VFDVKService+Photo.h
//  VkPost
//
//  Created by book on 03.02.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "VFDVKService.h"

@interface VFDVKService (Photo)

@property(nonatomic,retain) dispatch_queue_t _imageProcessingQueue;

-(void)photosGetByIds:(NSString *)photoIds
           completion:(void (^)(NSMutableArray *items, NSError *error))completionHandler ;

-(void)photosGetAllWithOwnerId:(NSString *)owner_id
                        offset:(NSInteger)offset
                         count:(NSInteger)count
                    completion:(void (^)(NSNumber *count, NSMutableArray *items, NSError *error))completionHandler ;

-(void)photosGetWithAlbumId:(NSNumber *)album_id
                   owner_id:(NSString *)owner_id
                      count:(NSInteger)count
                     offset:(NSInteger)offset
                        rev:(BOOL)rev
                 completion:(void (^)(NSNumber *count, NSMutableArray *items, NSError *error))completionHandler;

-(void)photosCopyWithOwnerId:(NSString *)owner_id
                    photo_id:(NSNumber *)photo_id
                  access_key:(NSString *)access_key
                  completion:(void (^)(BOOL success))completionHandler;

-(void)photoDeleteWithOwnerId:(NSString *)owner_id
                     photo_id:(NSNumber *)photo_id
                   completion:(void (^)(BOOL success))completionHandler;

-(NSMutableArray*)parseAlbums:(NSArray *)items;


-(void)getAlbumsAndPhotosWithOwnerId:(NSString *)ownerId
                              offset:(NSInteger)offset
                         countAlbums:(NSInteger)countAlbums
                         countPhotos:(NSInteger)countPhotos
                          needSystem:(BOOL)needSystem
                          completion:(void (^)(
                                               NSMutableArray *complitionAlbumsArray,
                                               BOOL isOverCountAlbum,
                                               NSError *error))
completionHandler;


-(void)getPhotoWithUrl:(NSURL *)url
            completion:(void (^)(UIImage *image,NSError *error)) completionHandler;


@end
