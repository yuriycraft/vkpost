//
//  VFDVKService+Photo.m
//  VkPost
//
//  Created by book on 03.02.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "VFDVKService+Photo.h"
#import "VFPhotoAttachment.h"
#import "YCAlbumModel.h"
#import "VKHTTPOperation.h"
#import "VKHTTPClient.h"
#import <objc/runtime.h>

static char imageProcessingQueueKey;

@implementation VFDVKService (Photo)


- (dispatch_queue_t)_imageProcessingQueue
{
    return objc_getAssociatedObject(self, &imageProcessingQueueKey);
}

- (void)set_imageProcessingQueue:( dispatch_queue_t)_imageProcessingQueue
{
    objc_setAssociatedObject(self, &imageProcessingQueueKey, _imageProcessingQueue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}




#pragma mark - photos.getById



-(void)photosGetByIds:(NSString *)photoIds
           completion:(void (^)(NSMutableArray *items, NSError *error))completionHandler {
    
    VKRequest *photosGetById = [VKRequest requestWithMethod:@"photos.getById" parameters:@{@"photos": photoIds, @"extended":@"1"}];
    photosGetById.attempts = 3;
    
    [photosGetById executeWithResultBlock:^(VKResponse * response) {
        
        NSMutableArray *items = [response json] ;
        completionHandler([self parsePhotos:items], nil);
        
    } errorBlock:^(NSError * error) {
        completionHandler(nil, error);
        [self errorHandler:error];
    }];
}


#pragma mark - photos.get

-(void)photosGetWithAlbumId:(NSString *)album_id
                   owner_id:(NSString *)owner_id
                      count:(NSInteger)count
                     offset:(NSInteger)offset
                        rev:(BOOL)rev
                 completion:(void (^)(NSNumber *count, NSMutableArray *items, NSError *error))completionHandler{
    
    
    [self photosGetWithAlbumId:album_id
                      owner_id:owner_id
                         count:count
                        offset:offset
                      extended:YES
                           rev:rev
                     photo_ids:@""
                     feed_type:@""
                          feed:@""
                   photo_sizes:NO
                    completion:^(NSNumber *count, NSMutableArray *items, NSError *error) {
                        
                        completionHandler(count, items, nil);
                        
                    }];
    
}


-(void)photosGetWithAlbumId:(NSString *)album_id
                   owner_id:(NSString *)owner_id
                      count:(NSInteger)count
                     offset:(NSInteger)offset
                   extended:(BOOL)extended
                        rev:(BOOL)rev
                  photo_ids:(NSString *)photo_ids
                  feed_type:(NSString *)feed_type
                       feed:(NSString *)feed
                photo_sizes:(BOOL)photo_sizes
                 completion:(void (^)(NSNumber *count, NSMutableArray *items, NSError *error))completionHandler{
    
    NSString *token = [[VKSdk accessToken] accessToken];
    
    NSDictionary *param = @{@"owner_id" : owner_id,
                            @"album_id" : album_id,
                            @"photo_ids" : photo_ids,
                            @"feed_type" : feed_type,
                            @"feed" : feed,
                            @"photo_sizes": @(photo_sizes),
                            @"count" : @(count),
                            @"offset": @(offset),
                            @"extended":@(extended),
                            @"rev":@(rev),
                            @"access_token":token
                            };
    
    
    VKRequest *photosGet = [VKRequest requestWithMethod:@"photos.get" parameters:param];
    photosGet.attempts = 3;
    [photosGet setPreferredLang:@"ru"];
    [photosGet executeWithResultBlock:^(VKResponse * response) {
        
        NSDictionary *res = [response json];
        NSMutableArray *items = res[@"items"];
        NSNumber *itemsCount = res[@"count"];
        
        completionHandler(itemsCount, [self parsePhotos:items], nil);
        
    } errorBlock:^(NSError * error) {
        
        completionHandler(nil, nil, error);
        [self errorHandler:error];
    }];
    
}


#pragma mark - photos.getAll

-(void)photosGetAllWithOwnerId:(NSString *)owner_id
                        offset:(NSInteger)offset
                         count:(NSInteger)count
                    completion:(void (^)(NSNumber *count, NSMutableArray *result, NSError *error))completionHandler{
    
    
    
    [self photosGetAllWithOwnerId:owner_id
                         extended:YES
                           offset:offset
                            count:count
                      photo_sizes:NO
                no_service_albums:NO
                      need_hidden:YES
                      skip_hidden:NO
                              rev:YES
                       completion:^(NSNumber *count, NSMutableArray *result, NSError *error) {
                           
                           completionHandler(count, result, nil);
                           
                       }];
    
}

-(void)photosGetAllWithOwnerId:(NSString *)owner_id
                      extended:(BOOL)extended
                        offset:(NSInteger)offset
                         count:(NSInteger)count
                   photo_sizes:(BOOL)photo_sizes
             no_service_albums:(BOOL)no_service_albums
                   need_hidden:(BOOL)need_hidden
                   skip_hidden:(BOOL)skip_hidden
                           rev:(BOOL)rev
                    completion:(void (^)(NSNumber *count, NSMutableArray *result, NSError *error))completionHandler{
    NSString *token = [[VKSdk accessToken] accessToken];
    
    NSDictionary *param = @{@"owner_id" : owner_id,
                            @"count" : @(count),
                            @"offset": @(offset),
                            @"extended":@(extended),
                            @"photo_sizes":@(photo_sizes),
                            @"no_service_albums":@(no_service_albums),
                            @"need_hidden":@(need_hidden),
                            @"skip_hidden":@(skip_hidden),
                            @"rev0":@(rev),
                            @"access_token":token
                            
                            };
    
    VKRequest *photosGetAll = [VKRequest requestWithMethod:@"photos.getAll" parameters:param ];
    [photosGetAll setPreferredLang:@"ru"];
    photosGetAll.attempts = 3;
    
    [photosGetAll executeWithResultBlock:^(VKResponse * response) {
        
        NSMutableDictionary *res = [response json];
        NSMutableArray *items = res[@"items"];
        NSNumber *itemsCount = res[@"count"];
        completionHandler(itemsCount, [self parsePhotos:items], nil);
        
    } errorBlock:^(NSError * error) {
        completionHandler(nil, nil, error);
        [self errorHandler:error];
    }];
    
}


-(void)photosCopyWithOwnerId:(NSString *)owner_id
                    photo_id:(NSNumber *)photo_id
                  access_key:(NSString *)access_key
                  completion:(void (^)(BOOL success))completionHandler{
    
    
    NSDictionary *param = @{@"owner_id": owner_id, @"photo_id":photo_id};
    
    if (access_key.length)
        param = @{@"owner_id": owner_id, @"photo_id":photo_id, @"access_key":access_key};
    
    VKRequest *photosCopy = [VKRequest requestWithMethod:@"photos.copy" parameters:param];
    photosCopy.attempts = 3;
    
    [photosCopy executeWithResultBlock:^(VKResponse * response) {
        completionHandler(YES);
        
    } errorBlock:^(NSError * error) {
        completionHandler(NO);
        [self errorHandler:error];
    }];
}

-(void)photoDeleteWithOwnerId:(NSString *)owner_id
                     photo_id:(NSNumber *)photo_id
                   completion:(void (^)(BOOL success))completionHandler{
    
    NSDictionary *param = @{@"owner_id": owner_id, @"photo_id":photo_id};
    
    VKRequest *photosCopy = [VKRequest requestWithMethod:@"photos.delete" parameters:param ];
    photosCopy.attempts = 3;
    
    [photosCopy executeWithResultBlock:^(VKResponse * response) {
        completionHandler(YES);
    } errorBlock:^(NSError * error) {
        completionHandler(NO);
        [self errorHandler:error];
    }];
    
}

#pragma mark - parse attachments from array

-(NSMutableArray *)parsePhotos:(NSArray *)items {
    
    NSMutableArray *photosArray = [NSMutableArray new];
    
    for (NSDictionary *item in items) {
        VFPhotoAttachment *photo = [[VFPhotoAttachment alloc] initWithDictionary:item] ;
        [photosArray addObject:photo];
    }
    
    return photosArray ;
}

#pragma mark - parse photo albums

-(NSMutableArray*)parseAlbums:(NSArray *)items {
    
    NSMutableArray *albumsArray = [NSMutableArray new];
    
    for (NSDictionary *item in items) {
        
        YCAlbumModel *album = [[YCAlbumModel alloc] initWithDictionary:item] ;
        
        NSArray * arrayPhotos = item[@"photos"];
        
        album.photosArray = [self parsePhotos:arrayPhotos];
        
        album.offset +=  arrayPhotos.count;
        
        [albumsArray addObject:album];
    }
    
    return albumsArray;
    
}

//просмотр фотоальбомов и выборщик фото из вк VKPhotoPicker
#pragma mark - VKPhotoPicker
#pragma mark - photos.getAlbums

-(void)getAlbumsAndPhotosWithOwnerId:(NSString *)ownerId
                              offset:(NSInteger)offset
                         countAlbums:(NSInteger)countAlbums
                         countPhotos:(NSInteger)countPhotos
                          needSystem:(BOOL)needSystem
                          completion:(void (^)(
                                               NSMutableArray *complitionAlbumsArray,
                                               BOOL isOverCountAlbum,
                                               NSError *error))
completionHandler {
    
    NSString *token = [[VKSdk accessToken] accessToken];
    VKRequest *getAlbums =
    [VKRequest requestWithMethod:@"execute.photos_getAlbums"
                      parameters:@{
                                   @"access_token" : token,
                                   VK_API_OWNER_ID : ownerId,
                                   @"offset" : @(offset),
                                   @"count" : @(countAlbums),
                                   @"need_system" : @(needSystem),
                                   @"countPhotos" : @(countPhotos),
                                   @"extended":@(YES),
                                   @"rev":@(YES)
                                   }
     ];
    
    
    getAlbums.attempts = 3;
    //  [getAlbums setPreferredLang:@"ru"];
    [getAlbums executeWithResultBlock:^(VKResponse *response) {
        
        NSArray *arrayAlbums = response.json[@"items"];
        
        //парсим альбомы и  фото
        NSMutableArray * parsedAlbumsArray = [self parseAlbums:arrayAlbums];
        
        BOOL isOverCountAlbum;
        
        !arrayAlbums.count ? (isOverCountAlbum = YES) : (isOverCountAlbum = NO);
        
        completionHandler(parsedAlbumsArray,isOverCountAlbum,nil);
        
    }
                           errorBlock:^(NSError *error) {
                               
                               completionHandler(nil,NO,error);
                               [self errorHandler:error];
                               
                           }];
}


#pragma mark -getPhotoWithUrl

-(void)getPhotoWithUrl:(NSURL *)url
            completion:(void (^)(UIImage *image,NSError *error)) completionHandler {
    
    VKHTTPOperation *imageLoad = [[VKHTTPOperation alloc]
                                  initWithURLRequest:[NSURLRequest
                                                      requestWithURL:url]];
    
    [imageLoad setCompletionBlockWithSuccess:^(VKHTTPOperation *operation, id responseObject) {
        
        UIImage *resultImage = [UIImage imageWithData:operation.responseData];
        
        completionHandler(resultImage,nil);
        
    } failure:^(VKHTTPOperation *operation, NSError *error) {
        
        completionHandler(nil,error);
        NSLog(@"%@",error);
    }];
    
    imageLoad.successCallbackQueue = self._imageProcessingQueue;
    [[VKHTTPClient getClient] enqueueOperation:imageLoad];
    
}





@end
