//
//  VFDVKService+Wall.h
//  VkPost
//
//  Created by book on 06.02.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "VFDVKService.h"


@interface VFDVKService (Wall)

-(void)sendPostOnWallWithParametrs:(NSMutableDictionary*)parametrs
                        completion:(void (^)(VKResponse *response,NSError *error)) completionHandler;


- (void)getPostUserPermissionCompletion:(void (^)(VKUser *user,NSError *error)) completionHandler ;
@end
