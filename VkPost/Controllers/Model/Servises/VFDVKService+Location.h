//
//  VFDVKService+Location.h
//  VkPost
//
//  Created by book on 06.02.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "VFDVKService.h"

@class  CLLocation;

@interface VFDVKService (Location)


typedef void (^addressCompletionBlock)(NSString *);

- (void)getAddressFromLocation:(CLLocation *)location
               complationBlock:(addressCompletionBlock)completionBlock;

- (void)searchLocationWithString:(NSString *)searchString
                 currentLocation:(CLLocation*)location
                                :(void (^)(NSArray *itemsArray,NSError *error)) completionHandler;

@end
