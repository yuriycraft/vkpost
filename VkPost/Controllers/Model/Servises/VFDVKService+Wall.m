//
//  VFDVKService+Wall.m
//  VkPost
//
//  Created by book on 06.02.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "VFDVKService+Wall.h"

@class VKResponse;

@implementation VFDVKService (Wall)

//постинг на стену
#pragma mark - sendPostOnWall

-(void)sendPostOnWallWithParametrs:(NSMutableDictionary*)parametrs
                        completion:(void (^)(VKResponse *response,NSError *error)) completionHandler
{
    
    VKRequest *post = [[VKApi wall] post:parametrs];
    
    [post executeWithResultBlock:^(VKResponse *response) {
        
        completionHandler(response,nil);
        
    }
                      errorBlock:^(NSError *error) {
                          completionHandler(nil,error);
                          
                      }];
}

#warning ??? User Category
//Запрос разрешений пользователя
#pragma mark - getPostUserPermission

- (void)getPostUserPermissionCompletion:(void (^)(VKUser *user,NSError *error)) completionHandler {
    
    [[[VKApi users] get:@{
                          VK_API_FIELDS : @"first_name_acc,can_post,sex,exports"
                          }] executeWithResultBlock:^(VKResponse *response) {
        
        VKUser *user = [response.parsedModel firstObject];
        
        completionHandler(user,nil);
    }
     errorBlock:^(NSError *error) {
         
         completionHandler(nil,error);
     }];
    
}

@end
