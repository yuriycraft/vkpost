//
//  YCPhotoViewController.m
//  example
//
//  Created by book on 18.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "UIBarButtonItem+Badge.h"
#import "UIScrollView+InfiniteScroll.h"
#import "VFPhotoAttachment.h"
#import "VKSdk.h"
#import "YCAlbumModel.h"
#import "YCPhotoCollectionViewCell.h"
#import "YCPhotoTableViewCell.h"
#import "YCPhotoViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#warning uncomment
//#import "commonUtillites.h"

//#import "PhotoPageViewController.h"
#import "AlbumOpenedCV.h"
#import "VFDVKService+Photo.h"

static NSString *kPhotoCellId = @"photoCell";
static NSString *kAlbumOpenedCVId = @"openedAlbum";


#define COUNTALBUMS 6
#define COUNTPHOTOS 10
#define INDEX_FOR_REQUEST_PHOTOS_TO_ALBUM 5
#define INDEX_FOR_REQUEST_ALBUMS 5
#define MENU_IMAGE_NAME @"menu_ios1.png"


@interface YCPhotoViewController () <
UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource,
UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, YCPhotoCollectionViewCellDelegate, YCPhotoPickerViewControllerDelegate,backAlbumOpenedCVDelegate>

@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic) NSMutableArray *albumsArray;
//@property(assign, nonatomic) NSInteger indexPosition;

@property(weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property(assign, nonatomic) BOOL isOverCountAlbums;
@property(assign, nonatomic) BOOL isAlbumsLoading;



@end

@implementation YCPhotoViewController {
    
    NSInteger _pageIndexAlbums;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if (self.isPicker) {
        
        self.seletedImageArray = [NSMutableArray array];
        self.toolbarHeightConstraint.constant = HEIGHT_TOOLBAR_CONSTANT;
        self.toolBar.hidden = NO;
        
    }
    
    _isOverCountAlbums = NO;
    
    _pageIndexAlbums = 0;
    
    if (!self.ownerId) {
    
    self.ownerId = @"-10290";
       }
    
    self.albumsArray = [NSMutableArray array];
    
    self.tableView.tableHeaderView = [[UIView alloc]
                                      initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width,
                                                               0.01f)];
    // change indicator view style to white
    self.tableView.infiniteScrollIndicatorStyle =
    UIActivityIndicatorViewStyleGray;
    
 
    
    [self getAlbumsAndPhotos];
    
    [self.tableView addInfiniteScrollWithHandler:^(UITableView *tableView) {
    }];
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Actions
#warning unComment
//-(void)addMenuButton{
//
//    if (CASHED_VARIABLES.appMode == vfeed && self.showMenuButton) {
//        UIImage *image = [[UIImage imageNamed:MENU_IMAGE_NAME]
//        imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        UIBarButtonItem *menu = [[UIBarButtonItem alloc] initWithImage:image
//        style:UIBarButtonItemStylePlain target:self
//        action:@selector(revealToggle:)];
//        self.navigationItem.leftBarButtonItem = menu;
//        self.navigationController.interactivePopGestureRecognizer.delegate =
//        self;
//
//        menu.target = self.revealViewController;
//        [self.view
//        removeGestureRecognizer:self.revealViewController.panGestureRecognizer];
//        [self.view
//        addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//    }
//}

-(void)makePickerWithCell:(YCPhotoCollectionViewCell*)cell
               albumModel:(YCAlbumModel*)albumModel
                indexPath:(NSIndexPath*)indexPath
{
    if(self.isPicker) {
        
        VFPhotoAttachment *photoModel =
        albumModel.photosArray[indexPath.item];
        
        cell.cellDelegate = self;
        cell.selectButton.hidden = NO;
        cell.selectButton.enabled = YES;
        cell.selectedStatus = photoModel.selectedStatus;
        
        // Select button block
        [cell buttonSelectBlock:^(BOOL seletedStatus) {
            
            photoModel.selectedStatus = seletedStatus;
            
            if (photoModel.selectedStatus) {
                
                [self.seletedImageArray addObject:photoModel];
                
            } else {
                
                [self.seletedImageArray removeObject:photoModel];
                
            }
  
            
            [self checkSelectedCount];
            
            
        }];
    }
}


- (void)addPhotosOnScrollingWithArray:(NSMutableArray *)array
                           albumModel:(YCAlbumModel*)albumModel
{
    
    [albumModel.photosArray addObjectsFromArray:array];
    
    albumModel.offset += array.count;
    
    [self.tableView reloadData];
}

#pragma mark - Get data from server

- (void)getAlbumsAndPhotos {
    
    self.isAlbumsLoading = YES;
    
    NSInteger preloadOffset = _pageIndexAlbums * COUNTALBUMS;
    
    __weak __typeof(self) weakSelf = self;
    
    [VKSERVICE getAlbumsAndPhotosWithOwnerId:self.ownerId offset:preloadOffset countAlbums:COUNTALBUMS countPhotos:COUNTPHOTOS needSystem:YES completion:^(NSMutableArray *complitionAlbumsArray, BOOL isOverCountAlbum, NSError *error) {
        
        if (complitionAlbumsArray) {
            
            _pageIndexAlbums++;
            
            [weakSelf.albumsArray
             addObjectsFromArray:complitionAlbumsArray];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [weakSelf.tableView reloadData];
                
                // finish infinite scroll animation
                [weakSelf.tableView
                 finishInfiniteScrollWithCompletion:nil];
            });
        }
        
        
        if (error) {
            
            [weakSelf.tableView finishInfiniteScroll];
            
        }
        
        if (isOverCountAlbum) {
            
            weakSelf.isOverCountAlbums = isOverCountAlbum;
            
            [weakSelf.tableView finishInfiniteScroll];
            
        }
        weakSelf.isAlbumsLoading = NO;
    }];
    
}

// получение фотографий при скроллинге
- (void)getMorePhotosWithAlbum:(YCAlbumModel *)albumModel
{
    
    albumModel.isAllPhotosLoaded = YES;
    
    //Запрос данных о фотографиях в альбоме
    if (![albumModel.album_id isEqual:@"-1"]) {
        
        [VKSERVICE
         photosGetWithAlbumId:albumModel.album_id
         owner_id:self.ownerId
         count:COUNTPHOTOS
         offset:albumModel.offset
         rev:YES
         completion:^(NSNumber *count, NSMutableArray *items,
                      NSError *error) {
             
             if (items) {
                 
                 [self addPhotosOnScrollingWithArray:items albumModel:albumModel];
             }
             
             albumModel.isAllPhotosLoaded = NO;
             
         }];
    }
    
    //Запрос данных о фотографиях в альбоме ВСЕ ФОТО
    else {
        
        [VKSERVICE
         photosGetAllWithOwnerId:self.ownerId
         offset:albumModel.offset
         count:COUNTPHOTOS
         completion:^(NSNumber *count, NSMutableArray *items,
                      NSError *error) {
             
             if (items) {
                 
                 [self addPhotosOnScrollingWithArray:items albumModel:albumModel];
             }
             albumModel.isAllPhotosLoaded = NO;
         }];
    }
}

#pragma mark -UITableViewDataSource


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    YCAlbumModel *albumModel = [self.albumsArray objectAtIndex:indexPath.row];
    
    YCPhotoTableViewCell *cell = (YCPhotoTableViewCell *)[tableView
                                                          dequeueReusableCellWithIdentifier:kPhotoCellId];
    [cell initWithModel:albumModel collectionViewDataSourceDelegate:self indexPath:indexPath];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.albumsArray.count;
}

#pragma mark - UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 165;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
    YCAlbumModel *albumModel = [self.albumsArray objectAtIndex:indexPath.section];
   
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (albumModel.photosArray.count > 0) {
        
        AlbumOpenedCV *contentController =
        [[UIStoryboard storyboardWithName:kPhotoStoryboardName bundle:nil]
         instantiateViewControllerWithIdentifier:kAlbumOpenedCVId];
   
        YCAlbumModel * album = self.albumsArray[indexPath.row];

        contentController.albumModel = album;
        
        contentController.modelIndexPathRow = indexPath.row;
    
        contentController.isPicker = YES;
        
        contentController.backFinishPickingDelegate = self;
        
        contentController.seletedImageArray = self.seletedImageArray;
        
        contentController.imagePickerDelegate = self.imagePickerDelegate;
        
//        NSInteger maxSelectCount = self.maxNumberOfSelections - self.seletedImageArray.count;
        
        contentController.maxNumberOfSelections = self.maxNumberOfSelections;
        
        [self.navigationController pushViewController:contentController animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row > ([tableView numberOfRowsInSection:0] - INDEX_FOR_REQUEST_ALBUMS)
        && !_isAlbumsLoading && !_isOverCountAlbums) {
        
        [self getAlbumsAndPhotos];
        
    } else if (_isOverCountAlbums) {
        
        [self.tableView removeInfiniteScroll];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(YCPhotoCollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    
    YCAlbumModel *albumModel = [self.albumsArray objectAtIndex:collectionView.tag];
    
    return [albumModel.photosArray count];
}

- (YCPhotoCollectionViewCell *)collectionView:
(YCPhotoCollectionView *)collectionView
                       cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    YCPhotoCollectionViewCell *cell = [collectionView
                                       dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier
                                       forIndexPath:indexPath];
    
    YCAlbumModel *albumModel = [self.albumsArray objectAtIndex:collectionView.tag];
    
    [self makePickerWithCell:cell albumModel:albumModel indexPath:indexPath];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView
       willDisplayCell:(YCPhotoCollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    YCAlbumModel *albumModel = [self.albumsArray objectAtIndex:collectionView.tag];
    
    VFPhotoAttachment *photoModel =
    albumModel.photosArray[indexPath.item];
    
    [cell.image sd_setImageWithURL:[NSURL URLWithString:photoModel.photo_130]
                         completed:^(UIImage *image, NSError *error,
                                     SDImageCacheType cacheType, NSURL *imageURL){
                             
                         }];
    
    if (indexPath.row > ([collectionView numberOfItemsInSection:0] - INDEX_FOR_REQUEST_PHOTOS_TO_ALBUM)
        && !albumModel.isAllPhotosLoaded) {
        
        if (albumModel.countPhotos > [albumModel.photosArray count]) {
            
            [self getMorePhotosWithAlbum:albumModel];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
#warning unComment
    //    [NAVIGATION presentPhotoFromController:self
    //                                    photos:self.collectionPhotosArray[collectionView.tag]
    //                           firstPhotoIndex:indexPath.row
    //                                 canDelete:NO
    //                           photoViewerType:AlbumPhotoViewerType];
    
    


}

#pragma mark UICollectionViewFlowLayoutDelegate

- (CGSize)collectionView:(YCPhotoCollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    YCAlbumModel *albumModel = [self.albumsArray objectAtIndex:collectionView.tag];
    VFPhotoAttachment *model =
    albumModel.photosArray[indexPath.item];
    
    CGSize sizeImage = CGSizeMake(95, 95);
    CGFloat ratio = [model.height floatValue] / [model.width floatValue];
    
    if (ratio < 1.f) {
        
        sizeImage = CGSizeMake(130.f, 95.f);
        return sizeImage;
    } else if (ratio > 1.f) {
        
        sizeImage = CGSizeMake(75, 95.f);
        
        return sizeImage;
    } else if (ratio == 1.f) {
        
        sizeImage = CGSizeMake(95, 95.f);
    }
    
    return sizeImage;
}

//#pragma mark -MWPhotoBrowserDelegate
//
//    - (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
//    {
//
//        return [(NSArray *)self.collectionPhotosArray[self.indexPosition]
//        count];
//    }
//
//

#pragma mark -  YCPhotoCollectionViewCellDelegate;

- (void)setSelectedItemWithCell:(YCPhotoCollectionViewCell *)cell {
    
            [cell changeReverseSelectedStatus: cell.selectButton.selected ? NO : YES];
    
        if (self.seletedImageArray.count > self.maxNumberOfSelections) { // если колиество уже выбранных превышает макс
            
            [cell changeReverseSelectedStatus:NO];
            
            [self.doneToolBarButton shakeBageAnimation];
            
            return;
        }

}

#pragma mark - backAlbumOpenedCVDelegate

-(void)backButtonFinishPickingMediaWithIndexAlbumModel:(NSInteger)index {
    
   // self.seletedImageArray = imageArray;
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
   
    [self checkSelectedCount];

    
}

#pragma mark - Actions
-(void)checkSelectedCount {
    
    self.doneToolBarButton.enabled = self.seletedImageArray.count > 0 ? YES : NO;
    
    self.doneToolBarButton.badgeValue = [NSString
                                         stringWithFormat:@"%lu", (unsigned long)self.seletedImageArray.count];
}
- (IBAction)doneToolBarButtonAction:(id)sender {
        
        [self.imagePickerDelegate ycPhotoPicker:self
            didFinishPickingMediaWithImageArray:self.seletedImageArray];
        [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)cancelToolBarButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];

}
@end
