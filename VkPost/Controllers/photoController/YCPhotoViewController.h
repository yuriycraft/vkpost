//
//  YCPhotoViewController.h
//  example
//
//  Created by book on 18.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCPhotoPickerViewController.h"

static NSString *const kPhotoStoryboardName = @"photoStoryboard";
static NSString *const kYCPhotoViewControllerIdentifier = @"photoViewController";


@interface YCPhotoViewController : YCPhotoPickerViewController

@property(strong, nonatomic) NSString* ownerId;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneToolBarButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toolbarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

- (IBAction)doneToolBarButtonAction:(id)sender;
- (IBAction)cancelToolBarButtonAction:(id)sender;

@end
