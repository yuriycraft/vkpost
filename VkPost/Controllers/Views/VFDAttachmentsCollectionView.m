//
//  VFDAttachmentsCollectionView.m
//  VkPost
//
//  Created by book on 12.01.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import "VFDAttachmentsCollectionView.h"
#import "YCVKUploadingAttachment.h"
#import "VFDVKService+Photo.h"
#import "VFDVKService+UploadFiles.h"
#import "VFPhotoAttachment.h"
#import "VFDDocAttachment.h"
#import "YCVKVideoUploadingModel.h"
#import "VFDVideoAttachment.h"



@implementation VFDAttachmentsCollectionView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.attachmentsArray = [NSMutableArray array];
    
    self.maxAttachmentSelectedItems = kMaxSelectedAttachment;
    
    self.isPoll = NO;
    self.isGeo = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(insertNewObject:)
                                                 name:kCompleteCreateAttachDone
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadItemWithAttach:)
                                                 name:PHOTO_LOADING_DID_END_NOTIFICATION
                                               object:nil];
}

-(void)reloadItemWithAttach:(NSNotification*)notification {
    
    YCVKUploadingAttachment* attach = [notification object];
    NSInteger index =  [self.attachmentsArray indexOfObject:attach];
    NSIndexPath *indexPath =
    [NSIndexPath indexPathForRow:index inSection:0];
    
    if (indexPath.item != NSNotFound) {
        
        [self reloadItemsAtIndexPaths:@[indexPath]];
    }
}

//if error uploading tap reUpload image
-(void)tapGestureReUploadAction:
(UITapGestureRecognizer *)tapGesture {
    
    UIView *view = (UIView *)tapGesture.view;
    
    NSIndexPath *indexPath =
    [NSIndexPath indexPathForRow:(NSUInteger)view.tag inSection:0];
    
    if (indexPath.item != NSNotFound) {
        
        YCVKUploadingAttachment *attach = (YCVKUploadingAttachment*)[self.attachmentsArray objectAtIndex:indexPath.item];
        
        attach.status = kUploading;
        attach.progressUploading = 0.f;
        
        if(!attach.uploadingRequest) {
            
            if(attach.type == kPhotoType || attach.type == kPollType){
                
                attach.uploadingRequest = [VKSERVICE uploadRequestPhotoOrPollWithAttach:attach];
                
            }
            else if(attach.upload_url && (attach.type == kVideoType || attach.type == kDocumentType)) {
                    
                    [VKSERVICE getServerAdressForVideoOrDocumentUploadWithAttach:attach];
                }
            
        }
        [self reloadItemsAtIndexPaths:@[indexPath]];
    }
}


// remove attach on remove (x) button action
- (void)removeAttachCollectionViewAtIndexPath:
(UITapGestureRecognizer *)tapGesture {
    
    UIView *view = (UIView *)tapGesture.view;
    if(self.attachmentsArray.count >= view.tag ) {
        
        NSIndexPath *indexPath =
        [NSIndexPath indexPathForRow:(NSUInteger)view.tag inSection:0];
        
        if (indexPath.item != NSNotFound) {

            //Cancel uploading request
            YCVKUploadingAttachment * att = (YCVKUploadingAttachment*) self.attachmentsArray[indexPath.row];
            
            if(att.uploadingRequest) {
                
                [att.uploadingRequest cancel];
            }
            if(att.geo) {
                
                self.isGeo = NO;
            }
            if(att.poll) {
                
                self.isPoll = NO;
            }
            
                [self performBatchUpdates:^{
                    
                    //remove attach
                    [self deleteItemsAtIndexPaths:@[indexPath]];
                      [self.attachmentsArray removeObject:att];
               
                } completion:^(BOOL finished) {
                    
                    [self reloadItemsAtIndexPaths:self.indexPathsForVisibleItems];
            
                            [[NSNotificationCenter defaultCenter] postNotificationName:ATTACHMENT_REMOVE_NOTIFICATION object:self];
                    [self layoutIfNeeded];
                }];
        }
        
    }
    
    [self changeCollectionViewHeight];
}


- (void)changeCollectionViewHeight {
    
    if (self.attachmentsArray.count > 0 &&
        self.contentSize.height == 0) {
        
        [self.attachmentsCollectionViewDelegate changeCollectionViewHeight:85.f];
        
    } else {
        
        [self.attachmentsCollectionViewDelegate changeCollectionViewHeight:self.collectionViewLayout.collectionViewContentSize.height];
    }
    
}

- (void)addAttachments:(id) anotherObj withPreviewImage:(nullable UIImage*)previewImage andExtensionString:(nullable NSString*)extension andUploadData:(nullable NSData*)uploadData  {
    
    YCVKUploadingAttachment *attachment = [YCVKUploadingAttachment new];
    [attachment initWithAnotherAttachments:anotherObj withPreviewImage:previewImage andExtensionString:extension andUploadingData:uploadData];
    
    
}

-(void)insertNewObject:(NSNotification *)notification {
    
    YCVKUploadingAttachment* attach = [notification object];

        [self.attachmentsArray addObject:attach];

        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
        for (NSUInteger i = [self numberOfItemsInSection:0]; i < self.attachmentsArray.count ; i++)
        {
            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        }
    
        [self performBatchUpdates:^{
            
            [self insertItemsAtIndexPaths:arrayWithIndexPaths];
            
        } completion:^(BOOL finished) {
            
            [self changeCollectionViewHeight];
            
        }];
}


@end
