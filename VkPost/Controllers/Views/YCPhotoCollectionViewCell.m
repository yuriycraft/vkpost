//
//  YCPhotoCollectionViewCell.m
//  example
//
//  Created by book on 19.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "YCPhotoCollectionViewCell.h"

@implementation YCPhotoCollectionViewCell
@synthesize selectedStatus,selectBlock;


#pragma mark -  Action

- (void)setSelectedStatus:(BOOL)selectStatus {
    
    [_selectButton setSelected:selectStatus ? YES : NO];
    
    self.image.alpha = selectStatus ? 0.9f : 1.f;
    
    [self animateButtonOnSelect:_selectButton];
}


- (IBAction)selectButtonAction:(id)sender {
    
    [self.cellDelegate setSelectedItemWithCell:self];
}


@end
