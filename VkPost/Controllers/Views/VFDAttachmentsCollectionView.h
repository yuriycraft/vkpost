//
//  VFDAttachmentsCollectionView.h
//  VkPost
//
//  Created by book on 12.01.17.
//  Copyright © 2017 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ATTACHMENT_REMOVE_NOTIFICATION @"ATTACHMENT_REMOVE_NOTIFICATION"

@protocol VFDAttachmentsCollectionViewDelegate <NSObject>


-(void)changeCollectionViewHeight:(float) heightCollectionView;

@end

typedef enum ChangeCount : NSUInteger {
    
    kIncrement  = 1,
    kDecrement  = 0,
    
} ChangeCount;


static const NSInteger kMaxSelectedAttachment = 10;
static NSString *const kVKPhotoAttachmentCell = @"VKPhotoAttachmentCell";

@class VKUploadImage;

@interface VFDAttachmentsCollectionView : UICollectionView

@property(nonatomic, strong) NSMutableArray *attachmentsArray;

@property(assign, nonatomic) NSInteger countAttachments;

@property(assign, nonatomic) BOOL isPoll;

@property(assign, nonatomic) BOOL isGeo;

@property(assign, nonatomic) NSInteger maxAttachmentSelectedItems;


@property(nonatomic, weak, readwrite) id <VFDAttachmentsCollectionViewDelegate> attachmentsCollectionViewDelegate;

- (void)addAttachments:(id) anotherObj withPreviewImage:(UIImage*)previewImage andExtensionString:(NSString*)extension andUploadData:(NSData*)uploadData ;

- (void)changeCollectionViewHeight;

// Метод который вызывается при жесте нажатия на UIImageView (X) "Удаление". Удаляет Attachment из массива и CollectionView
- (void)removeAttachCollectionViewAtIndexPath:
(UITapGestureRecognizer *)tapGesture;

// Метод который вызывается при жесте нажатия когда возникла "Ошибка загрузки". Начинает загрузку на сервер заново;
-(void)tapGestureReUploadAction:
(UITapGestureRecognizer *)tapGesture;

@end
