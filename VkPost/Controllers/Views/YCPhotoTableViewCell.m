//
//  YCPhotoTableViewCell.m
//  example
//
//  Created by book on 18.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "YCPhotoTableViewCell.h"
#import "YCPhotoCollectionViewCell.h"
#import "YCAlbumModel.h"

@implementation YCPhotoCollectionView

@end


@implementation YCPhotoTableViewCell

-(void)initWithModel:(YCAlbumModel*)model
collectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate
           indexPath:(NSIndexPath*)indexPath {
    
        self.albumNameLabel.text = model.title;
        
        self.photoCountLabel.text =
        [NSString stringWithFormat:@"%@", [self changeStringWithCount:model.countPhotos]];
    
        self.myCollectionView.dataSource = dataSourceDelegate;
        
        self.myCollectionView.delegate = dataSourceDelegate;
        
        [self.myCollectionView setTag:indexPath.row];
        
        [self.myCollectionView reloadData];
        
}


- (NSString *)changeStringWithCount:(NSInteger)count {
    
    if ((count % 100 >= 11) && (count % 100 <= 19)) {
        
        return [NSString stringWithFormat:@"%ld фотографий", (long)count];
        
    } else {
        
        if (count % 10 == 1) {
            
            return [NSString stringWithFormat:@"%ld фотография", (long)count];
            
        } else if (count % 10 == 2 || count % 10 == 3 || count % 10 == 4) {
            
            return [NSString stringWithFormat:@"%ld фотографии", (long)count];
            
        } else {
            
            return [NSString stringWithFormat:@"%ld фотографий", (long)count];
        }
    }
}

@end
