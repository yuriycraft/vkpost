//
//  YCPhotoCollectionViewCell.h
//  example
//
//  Created by book on 19.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCSelectedCollectionViewCell.h"


@interface YCPhotoCollectionViewCell : YCSelectedCollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@end
