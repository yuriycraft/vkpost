//
//  YCPhotoTableViewCell.h
//  example
//
//  Created by book on 18.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCPhotoCollectionView : UICollectionView

@end

static NSString *CollectionViewCellIdentifier = @"CollectionViewCellIdentifier";

@class YCAlbumModel;

@interface YCPhotoTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet YCPhotoCollectionView *myCollectionView;

@property (weak, nonatomic) IBOutlet UILabel *photoCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *albumNameLabel;

-(void)initWithModel:(YCAlbumModel*)model
collectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath*)indexPath ;


@end
