//
//  AssetsCollectionViewCell.m
//  VkPost
//
//  Created by book on 25.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "AssetsCollectionViewCell.h"

@interface AssetsCollectionViewCell ()

@end

@implementation AssetsCollectionViewCell

- (void)configureWithModel:(AssetsModel *)model {

  self.assetsImageView.image = model.image;

  self.selectedStatus = model.selectedStatus;
}



- (void)setSelectedStatus:(BOOL)selectStatus {
    
    [_selectButton setSelected:selectStatus ? YES : NO];
    
    self.assetsImageView.alpha = selectStatus ? 0.9f : 1.f;
    
    [self animateButtonOnSelect:_selectButton];
    
    
}

- (IBAction)selectButtonAction:(id)sender {
    
    [self.cellDelegate setSelectedItemWithCell:self];
}



@end
