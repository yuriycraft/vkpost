//
//  NavigationBarTitleView.h
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^navigationBarTitleViewSelectedBlock)(void);


@interface NavigationBarTitleView : UIView

@property (nonatomic, strong, readwrite) UIButton *navigationBarTitleButton;

@property (nonatomic, assign, readwrite) UIColor *indicatorColor;

@property (nonatomic, strong, readwrite) UIColor  *titleColor;

@property (nonatomic, strong, readwrite) UIImageView *navigationBarTitleImageView;

- (instancetype)initWithFrame:(CGRect)frame
                selectedBlock:(navigationBarTitleViewSelectedBlock)selectedBlock;


- (void)navigationBarTitleButtonAction:(id)sender;

@end
