//
//  NavigationBarTitleView.m
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.

#import "NavigationBarTitleView.h"

#define PULL_BUTTON_PICKER_IMAGE  @"pullButton_black_picker"

@interface NavigationBarTitleView ()

@property(nonatomic, strong, readwrite)
    navigationBarTitleViewSelectedBlock inSelectedBlock;

@end

@implementation NavigationBarTitleView

- (void)setTitleColor:(UIColor *)titleColor {
    
  _titleColor = titleColor;

  [_navigationBarTitleButton setTitleColor:_titleColor
                                  forState:UIControlStateNormal];
}

- (void)setIndicatorColor:(UIColor *)indicatorColor {
    
  _indicatorColor = indicatorColor;

  _navigationBarTitleImageView.tintColor = indicatorColor;
}

#pragma - init.

- (instancetype)initWithFrame:(CGRect)frame
                selectedBlock:
                    (navigationBarTitleViewSelectedBlock)selectedBlock {

  self = [super initWithFrame:frame];

  if (self) {

    [self initNavigationBarTitleButton];

    [self initNavigationBarTitleImageView];

    _inSelectedBlock = selectedBlock;
  }
  return self;
}

- (void)initNavigationBarTitleButton {

  if (!_navigationBarTitleButton) {

    _navigationBarTitleButton = [UIButton buttonWithType:UIButtonTypeCustom];

    if (_titleColor) {

        [_navigationBarTitleButton setTitleColor:[UIColor colorWithRed:73 / 255.f
                                                                 green:92 / 255.f
                                                                  blue:241 / 255.f
                                                                 alpha:1.f]
                                      forState:UIControlStateNormal];
    } else {

        [_navigationBarTitleButton setTitleColor:[UIColor colorWithRed:73 / 255.f
                                                                 green:92 / 255.f
                                                                  blue:241 / 255.f
                                                                 alpha:1.f]
                                      forState:UIControlStateNormal];
    }

    _navigationBarTitleButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.f];

    [_navigationBarTitleButton
               addTarget:self
                  action:@selector(navigationBarTitleButtonAction:)
        forControlEvents:UIControlEventTouchUpInside];

    [_navigationBarTitleButton setTranslatesAutoresizingMaskIntoConstraints:NO];

    [self addSubview:_navigationBarTitleButton];

    [self addConstraint:[NSLayoutConstraint
                            constraintWithItem:self.navigationBarTitleButton
                                     attribute:NSLayoutAttributeCenterX
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self
                                     attribute:NSLayoutAttributeCenterX
                                    multiplier:1.0
                                      constant:0.0]];
    [self addConstraint:[NSLayoutConstraint
                            constraintWithItem:self.navigationBarTitleButton
                                     attribute:NSLayoutAttributeCenterY
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self
                                     attribute:NSLayoutAttributeCenterY
                                    multiplier:1.0
                                      constant:0.0]];
  }
}

- (void)initNavigationBarTitleImageView {

  if (!_navigationBarTitleImageView) {

    _navigationBarTitleImageView =
        [[UIImageView alloc] initWithFrame:CGRectZero];

    [_navigationBarTitleImageView
        setImage:
            [[UIImage imageNamed:PULL_BUTTON_PICKER_IMAGE]
                imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];

    if (_indicatorColor) {
      _navigationBarTitleImageView.tintColor = _indicatorColor;
    }
    else {
        
      _navigationBarTitleImageView.tintColor = [UIColor colorWithRed:73 / 255.f
                                                               green:92 / 255.f
                                                                blue:241 / 255.f
                                                               alpha:1.f];
    }

    [_navigationBarTitleImageView
        setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_navigationBarTitleImageView setUserInteractionEnabled:YES];

    [self addSubview:_navigationBarTitleImageView];

    NSLayoutConstraint *imageViewLeft =
        [NSLayoutConstraint constraintWithItem:self.navigationBarTitleImageView
                                     attribute:NSLayoutAttributeLeft
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self.navigationBarTitleButton
                                     attribute:NSLayoutAttributeRight
                                    multiplier:1
                                      constant:3];

    NSLayoutConstraint *imageViewHeight = [NSLayoutConstraint
        constraintWithItem:self.navigationBarTitleImageView
                 attribute:NSLayoutAttributeHeight
                 relatedBy:NSLayoutRelationEqual
                    toItem:self.navigationBarTitleImageView.superview
                 attribute:NSLayoutAttributeHeight
                multiplier:0.28
                  constant:0];
    NSLayoutConstraint *imageViewWidth =
        [NSLayoutConstraint constraintWithItem:self.navigationBarTitleImageView
                                     attribute:NSLayoutAttributeWidth
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self.navigationBarTitleImageView
                                     attribute:NSLayoutAttributeHeight
                                    multiplier:1
                                      constant:0];

    NSLayoutConstraint *imageViewCenterY = [NSLayoutConstraint
        constraintWithItem:self.navigationBarTitleImageView
                 attribute:NSLayoutAttributeCenterY
                 relatedBy:NSLayoutRelationEqual
                    toItem:self.navigationBarTitleImageView.superview
                 attribute:NSLayoutAttributeCenterY
                multiplier:1
                  constant:0];

    [self.navigationBarTitleImageView.superview addConstraints:@[
      imageViewCenterY,
      imageViewLeft,
      imageViewHeight,
      imageViewWidth
    ]];
  }
}

- (void)navigationBarTitleButtonAction:(id)sender {

  if (!_navigationBarTitleButton.selected) {

    _inSelectedBlock ? _inSelectedBlock() : nil;
  }
}

@end
