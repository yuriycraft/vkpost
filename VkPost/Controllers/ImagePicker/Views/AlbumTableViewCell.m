//
//  AlbumTableViewCell.m
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "AlbumTableViewCell.h"

@implementation AlbumTableViewCell

#pragma mark - cell data loader.

- (void)configureCellDataWithAssetCollection:(PHAssetCollection *)collection {

  _titleLabel.text = collection.localizedTitle;

  PHFetchResult *fetchResult =
      [PHAsset fetchAssetsInAssetCollection:collection options:nil];

  _countLabel.text =
      [NSString stringWithFormat:@"%lu", (unsigned long)fetchResult.count];

  PHCachingImageManager *imageManager = [[PHCachingImageManager alloc] init];

  [imageManager requestImageForAsset:fetchResult.lastObject
                          targetSize:_photoImageView.frame.size
                         contentMode:PHImageContentModeAspectFill
                             options:nil
                       resultHandler:^(UIImage *result, NSDictionary *info) {
                           
                         if (result) {
                             
                           [_photoImageView setImage:result];
                         }
                       }];
}

#pragma mark - Action
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
  [super setSelected:selected animated:animated];
}

@end
