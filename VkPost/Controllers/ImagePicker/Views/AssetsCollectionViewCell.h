//
//  AssetsCollectionViewCell.h
//  VkPost
//
//  Created by book on 25.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "AssetsModel.h"
#import <UIKit/UIKit.h>
#import "YCSelectedCollectionViewCell.h"


@interface AssetsCollectionViewCell : YCSelectedCollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *assetsImageView;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (assign, nonatomic) BOOL isMaxItem;

- (IBAction)selectButtonAction:(id)sender;
- (void)configureWithModel:(AssetsModel *)model;

@end


