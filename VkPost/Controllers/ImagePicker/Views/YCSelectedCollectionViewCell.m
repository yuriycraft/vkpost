//
//  YCSelectedCollectionViewCell.m
//  VkPost
//
//  Created by book on 08.12.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "YCSelectedCollectionViewCell.h"

@implementation YCSelectedCollectionViewCell

-(void)changeReverseSelectedStatus:(BOOL)reverseSelect { //обратный статус
    
    
    self.selectedStatus = reverseSelect ;
    
    if(self.selectBlock) {
        
        self.selectBlock(reverseSelect);
    }
}

-(void)animateButtonOnSelect:(UIButton*) button {
    
    [UIView animateWithDuration:0.1
                     animations:^{
                         
                         button.transform = CGAffineTransformMakeScale(0.9, 0.9);
                         button.transform = CGAffineTransformMakeScale(1, 1);
                     }
                     completion:^(BOOL finished) {

                                             }];
    
    
}

- (void)buttonSelectBlock:(void (^)(BOOL seletedStatus))selectedBlock {
    
    self.selectBlock = selectedBlock ;
}
@end
