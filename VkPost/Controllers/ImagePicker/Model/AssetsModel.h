//
//  AssetsModel.h
//  VkPost
//
//  Created by book on 25.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>
#import <UIKit/UIKit.h>

@interface AssetsModel : NSObject

@property(nonatomic, strong, readwrite) UIImage *image;
@property(nonatomic, assign, readwrite) BOOL selectedStatus;
@property(nonatomic, assign, readwrite) NSInteger index;
@property(nonatomic, strong, readwrite) PHAsset *imageAsset;


@end
