//
//  ImagePickerViewController.m
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "AlbumsViewController.h"
#import "AssetsCollectionViewCell.h"
#import "AssetsModel.h"
#import "ImagePickerViewController.h"
#import "UIBarButtonItem+Badge.h"
#import <AVFoundation/AVFoundation.h>


#define NOT_AUTHORIZATION_IMAGE @"authorization"

static NSString *const kCameraCellId = @"CameraCollectionViewCell";
static NSString *const kPhotoCellId = @"AssetsCollectionViewCell";
static NSString *const kNotAuthorizationImageName = @"notAuthorization_picker.png";


static NSString *const kAlbumsViewControllerIdentifier = @"AlbumsViewController";


@interface ImagePickerViewController () <
UICollectionViewDelegate, UICollectionViewDataSource,
AlbumsViewControllerDelegate, YCPhotoCollectionViewCellDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property(nonatomic, strong, readwrite) NSMutableArray *assetsGroupArray;

@property(nonatomic, strong, readwrite) NSMutableDictionary *assetsDic;

@property(nonatomic, strong, readwrite) NSMutableDictionary *cellDic;

//@property(nonatomic, strong, readwrite) NSMutableArray *seletedImageArray;

@property(nonatomic, assign, readwrite) NSInteger currentAssetsIndex;

@property(nonatomic, strong, readwrite) PHImageManager *imageManager;

@property(nonatomic, strong, readwrite) PHFetchResult *currentFetchResult;

@property(nonatomic, strong) UIViewController *cameraController;

@property(nonatomic, assign)BOOL isCameraAvailable;


@property(nonatomic, strong, readwrite)
UIImageView *authorizationStatusDeniedView;

@property(nonatomic, strong, readwrite)
NavigationBarTitleView *navigationBarTitleView;

@end

@implementation ImagePickerViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _assetsDic = [[NSMutableDictionary alloc] init];
    
    _assetsGroupArray = [[NSMutableArray alloc] init];
    
    self.seletedImageArray = [[NSMutableArray alloc] init];
    
    _imageManager = [[PHCachingImageManager alloc] init];
    
    [self initAuthorizationStatusDeniedView];
    
    [self judgeIfHavePhotoAblumAuthority];
    
    [self initNavigationBarTitleView];
    
    [self.collectionView setScrollsToTop:YES];
}

#pragma mark - setter and getter

- (CGFloat)collectionCellWidthCompareToScreen {
    
    if (!_collectionCellWidthCompareToScreen) {
        
        return PICKER_COUNT_CELLS_IN_LINE; //количество фото вмещающихся в строку
        
    } else {
        
        return _collectionCellWidthCompareToScreen;
    }
}

#pragma mark - Init

- (void)initAuthorizationStatusDeniedView {
    
    if (!_authorizationStatusDeniedView) {
        
        _authorizationStatusDeniedView = [[UIImageView alloc]
                                          initWithFrame:CGRectMake(0, 0, self.view.frame.size.width / 2,
                                                                   self.view.frame.size.width / 2)];
        
        _authorizationStatusDeniedView.center = self.view.center;
        
        _authorizationStatusDeniedView.image =
        [UIImage imageNamed:NOT_AUTHORIZATION_IMAGE];
        
        _authorizationStatusDeniedView.hidden = YES;
        
        [self.view addSubview:_authorizationStatusDeniedView];
    }
}

- (void)initNavigationBarTitleView {
    
    if (!_navigationBarTitleView) {
        
        _navigationBarTitleView = [[NavigationBarTitleView alloc]
                                   initWithFrame:CGRectMake(0, 0, 120.f,
                                                            self.navigationController.navigationBar.bounds
                                                            .size.height)
                                   selectedBlock:^{
                                       
                                       AlbumsViewController *contentController = [
                                                                                  [UIStoryboard storyboardWithName:kImagePickerStoryboardName bundle:nil]
                                                                                  instantiateViewControllerWithIdentifier:kAlbumsViewControllerIdentifier];
                                       contentController.assetsGroupArray = _assetsGroupArray;
                                       contentController.delegate = self;
                                       contentController.lastSelectedIndex = self.currentAssetsIndex;
                                       UINavigationController *nav = [[UINavigationController alloc]
                                                                      initWithRootViewController:contentController];
                                       
                                       [self presentViewController:nav animated:YES completion:nil];
                                       
                                   }];
        
        self.navigationItem.titleView = _navigationBarTitleView;
        
        if (_assetsGroupArray.count) {
            
            [_navigationBarTitleView.navigationBarTitleButton
             setTitle:[_assetsGroupArray[0] localizedTitle]
             forState:UIControlStateNormal];
        } else {
            
            [_navigationBarTitleView.navigationBarTitleButton
             setTitle:@"Все фото"
             forState:UIControlStateNormal];
        }
    }
}

#pragma mark - authorization judgement.

- (void)judgeIfHavePhotoAblumAuthority { // получаем статус авторизации
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusRestricted ||
        status == PHAuthorizationStatusDenied ) {  // если нет доступа
        
        [self showErrorAccessPhoto];
        
        self.doneButton.enabled = NO;
        self.authorizationStatusDeniedView.hidden =NO;
        
    } else if (status == PHAuthorizationStatusNotDetermined) { // если доступ разшен получаем авторизацию
        
        self.doneButton.enabled = NO;
        
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                
                [self initAlbumAssetsLibrary];
                self.authorizationStatusDeniedView.hidden = YES;
                self.doneButton.enabled = NO;
            }
        }];
        
        
    }  else    if (status == PHAuthorizationStatusAuthorized) { // если получена авторизация
        
        [self initAlbumAssetsLibrary];
        
        self.doneButton.enabled = NO;
        self.authorizationStatusDeniedView.hidden = YES;
    }
    
}

-(void)showErrorAccessPhoto {
    
    UIAlertAction *cancelAction =
    [UIAlertAction actionWithTitle:@"Отмена"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *_Nonnull action) {
                               
                               _authorizationStatusDeniedView.hidden = NO;
                           }];
    
    UIAlertAction *setUpAction = [UIAlertAction
                                  actionWithTitle:@"Настройки"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *_Nonnull action) {
                                      
                                      NSURL *url =
                                      [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                      
                                      if ([[UIApplication sharedApplication] canOpenURL:url]) {
                                          
                                          NSURL *url = [NSURL
                                                        URLWithString:UIApplicationOpenSettingsURLString];
                                          
                                          [[UIApplication sharedApplication] openURL:url];
                                      }
                                  }];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:
                                          @"Нет доступа к фото"
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:setUpAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}


#pragma mark - Photo Library.

- (void)initAlbumAssetsLibrary {
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        
        
        PHFetchResult *smartAlbumsResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
        
        PHFetchResult *userAlbumsResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
        
        
        NSArray * fetchResultArray = @[smartAlbumsResult, userAlbumsResult];
        
        // Filter albums
        NSArray *assetCollectionSubtypes = @[
                                             @(PHAssetCollectionSubtypeSmartAlbumUserLibrary),
                                             @(PHAssetCollectionSubtypeAlbumMyPhotoStream),
                                             @(PHAssetCollectionSubtypeSmartAlbumSelfPortraits),
                                             @(PHAssetCollectionSubtypeSmartAlbumPanoramas),
                                             @(PHAssetCollectionSubtypeSmartAlbumScreenshots),
                                             @(PHAssetCollectionSubtypeSmartAlbumRecentlyAdded)
                                             
                                             
                                             ];
        NSMutableDictionary *smartAlbums = [NSMutableDictionary dictionaryWithCapacity:assetCollectionSubtypes.count];
        
        NSMutableArray *userAlbums = [NSMutableArray array];
        
        for (PHFetchResult *fetchResult in fetchResultArray) {
            
            [fetchResult enumerateObjectsUsingBlock:^(PHAssetCollection *assetCollection, NSUInteger index, BOOL *stop) {
                
                PHAssetCollectionSubtype subtype = assetCollection.assetCollectionSubtype;
                
                if (subtype == PHAssetCollectionSubtypeAlbumRegular) {
                    
                    if(assetCollection.estimatedAssetCount > 0){
                        
                        [userAlbums addObject:assetCollection];
                    }
                } else if ([assetCollectionSubtypes containsObject:@(subtype)]) {
                    
                    PHFetchResult *fetchResultSmartALbums =
                    [PHAsset fetchAssetsInAssetCollection:assetCollection
                                                  options:nil];
                    if(fetchResultSmartALbums.count > 0) {
                        
                        if (!smartAlbums[@(subtype)]) {
                            
                            smartAlbums[@(subtype)] = [NSMutableArray array];
                        }
                        
                        [smartAlbums[@(subtype)] addObject:assetCollection];
                    }
                }
            }];
        }
        
        NSMutableArray *assetCollections = [NSMutableArray array];
        
        // Fetch smart albums
        for (NSNumber *assetCollectionSubtype in assetCollectionSubtypes) {
            
            NSArray *collections = smartAlbums[assetCollectionSubtype];
            
            if (collections.count > 0) {
                
                [assetCollections addObjectsFromArray:collections];
            }
        }
        
        // Fetch user albums
        [userAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *assetCollection, NSUInteger index, BOOL *stop) {
            
            
            [assetCollections addObject:assetCollection];
            
        }];
        
        self.assetsGroupArray = assetCollections;
        
        [self initAssetArray];
        
    });
}

- (void)initAssetArray {
    
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    
    options.sortDescriptors = @[
                                [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]
                                ];
    options.predicate = [NSPredicate predicateWithFormat:@"mediaType = %i",PHAssetMediaTypeImage];
    
    _currentFetchResult = [PHAsset
                           fetchAssetsInAssetCollection:_assetsGroupArray[_currentAssetsIndex]
                           options:options];
    
    [self checkStatusSourceCameraTypeAvailable];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        //Run UI Updates
        [self.collectionView reloadData];
        
    });
    
}

#pragma mark - Action

- (IBAction)cancelButtonAction:(id)sender {
    
    [self turnBack:nil];
}

- (IBAction)doneButtonAction:(id)sender {
    
    [self.imagePickerDelegate ycPhotoPicker:self
        didFinishPickingMediaWithImageArray:self.seletedImageArray];
    
    [self turnBack:nil];
}

- (void)turnBack:(id)sender {
    
    _imageManager = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

//set navigation title
-(void)setNavigationTitle {
    
    if (_assetsGroupArray.count) {
        
        [_navigationBarTitleView.navigationBarTitleButton
         setTitle:NSLocalizedString(
                                    [_assetsGroupArray[_currentAssetsIndex] localizedTitle],
                                    nil)
         forState:UIControlStateNormal];
    }
    
}
//проверяем статус доступности камеры
-(void)checkStatusSourceCameraTypeAvailable {
    
    if (_currentAssetsIndex == 0 &&
        [UIImagePickerController
         isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] &&
        _assetsGroupArray.count) {
        
        self.isCameraAvailable = YES;
        return;
    }
    
    self.isCameraAvailable = NO;
}

-(AssetsModel*)getAssetInDictionaryWithIndexPath:(NSInteger)indexPathRow { //получение модели для ячейки
    
    AssetsModel *assetsModel;
    
    NSString* assetsKeyAtIndexPathRow = [NSString
                                         stringWithFormat:@"%@",
                                         [(PHAsset *)_currentFetchResult[indexPathRow]
                                          localIdentifier]];
    if (![_assetsDic
          objectForKey:assetsKeyAtIndexPathRow
          ]) {
        
        assetsModel = [[AssetsModel alloc] init];
        
        @autoreleasepool {
            
            [_imageManager
             requestImageForAsset:_currentFetchResult[indexPathRow]
             targetSize:CGSizeMake(200, 200)
             contentMode:PHImageContentModeAspectFill
             options:nil
             resultHandler:^(UIImage *result, NSDictionary *info) {
                 
                 if (result) {
                     
                     assetsModel.image = result;
                     
                     assetsModel.selectedStatus = NO;
                     
                     [_assetsDic
                      setObject:assetsModel
                      forKey:assetsKeyAtIndexPathRow];
                     
                 }
                 
             }];
        }
        
        
    } else {
        
        assetsModel = (AssetsModel *)[_assetsDic
                                      objectForKey:assetsKeyAtIndexPathRow];
    }
    return assetsModel;
    
}


#pragma mark - UICollectionViewDelegate.

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    
    [self setNavigationTitle];
    
    if (self.isCameraAvailable &&
        _assetsGroupArray.count) {
        
        NSInteger count = _currentFetchResult.count;
        
        return count + 1;
    }
    return _currentFetchResult.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.isCameraAvailable && indexPath.row == 0) { //если доступна камера показываем ячейку камеры
        
        UICollectionViewCell *cell = [collectionView
                                      dequeueReusableCellWithReuseIdentifier:kCameraCellId
                                      forIndexPath:indexPath];
        
        return cell;
    }
    
    //если камера доступна то вычитаем из indexPath.row единицу
    NSInteger indexPathRow = (_currentAssetsIndex == 0 && self.isCameraAvailable)
    ? indexPath.row - 1
    : indexPath.row;
    
    AssetsCollectionViewCell *cell = [collectionView
                                      dequeueReusableCellWithReuseIdentifier:kPhotoCellId
                                      forIndexPath:indexPath];
    
    AssetsModel *assetsModel = [self getAssetInDictionaryWithIndexPath:indexPathRow];
    
    [cell configureWithModel: assetsModel];
    
    cell.cellDelegate = self;
    
    __weak typeof(self.seletedImageArray) seletedImageArrayWeak = self.seletedImageArray;
    
    
    //Select button block
    [cell buttonSelectBlock:^(BOOL seletedStatus) {
        
        assetsModel.selectedStatus = seletedStatus;
        
        assetsModel.imageAsset = _currentFetchResult[indexPathRow];
        
        if (assetsModel.selectedStatus) {
            
            [seletedImageArrayWeak addObject:assetsModel];
            
            self.doneButton.enabled = YES;
            
        } else {
            
            [seletedImageArrayWeak removeObject:assetsModel];
            
            if (!seletedImageArrayWeak.count) {
                
                self.doneButton.enabled = NO;
            }
        }
        
        self.doneButton.badgeValue = [NSString
                                      stringWithFormat:@"%lu", (unsigned long)seletedImageArrayWeak.count];
        
        [_assetsDic
         setObject:assetsModel
         forKey:[NSString
                 stringWithFormat:@"%@",
                 [(PHAsset *)
                  _currentFetchResult[indexPathRow]
                  localIdentifier]]];
        
    }];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isCameraAvailable && indexPath.row == 0) {
        
        if (self.cameraController != nil) {
            
            id delegate;
            if ([self.cameraController valueForKey:@"delegate"]) {
                
                delegate = [self.cameraController valueForKey:@"delegate"];
            }
            
            NSData *tempArchiveViewController =
            [NSKeyedArchiver archivedDataWithRootObject:self.cameraController];
            
            UIViewController *cameraVC =
            [NSKeyedUnarchiver unarchiveObjectWithData:tempArchiveViewController];
            
            if (delegate && delegate != nil) {
                
                [cameraVC setValue:delegate forKey:@"delegate"];
            }
            
            [self presentViewController:cameraVC animated:YES completion:NULL];
        } else {
            
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((([UIScreen mainScreen].bounds.size.width - 10) /
                       self.collectionCellWidthCompareToScreen),
                      (([UIScreen mainScreen].bounds.size.width - 10) /
                       self.collectionCellWidthCompareToScreen));
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(2, 2, 2, 2);
}

#pragma mark -  AssetsCollectionViewCellDelegate;

- (void)setSelectedItemWithCell:(AssetsCollectionViewCell *)cell {
    
        [cell changeReverseSelectedStatus: cell.selectButton.selected ? NO : YES];
    
    if (self.seletedImageArray.count > self.maxNumberOfSelections) { // если колиество уже выбранных превышает макс
        
        [cell changeReverseSelectedStatus:NO];
        
        [self.doneButton shakeBageAnimation];
        
        return;
    }
    

    
}

#pragma mark - AlbumsViewControllerDelegate

- (void)albumsViewControllerSetIndexAlbum:(NSInteger)index {
    
    if (_currentAssetsIndex != index) {
        
        _currentAssetsIndex = index;
        
        [self initAssetArray];
        
        [_navigationBarTitleView navigationBarTitleButtonAction:nil];
        
        if (!self.seletedImageArray.count) {
            
            self.doneButton.enabled = NO;
        }
        
        [self.collectionView reloadData];
        
    } else {
        
        [_navigationBarTitleView navigationBarTitleButtonAction:nil];
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
    //этот метод вызывается для фотокамеры
    
    __block PHAssetChangeRequest *_mChangeRequest = nil;
    
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        
        _mChangeRequest = [PHAssetChangeRequest
                           creationRequestForAssetFromImage:
                           [info valueForKey:UIImagePickerControllerOriginalImage]];
        
    }
                                      completionHandler:^(BOOL success, NSError *error) {
                                          
                                          if (success) {
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  [self initAssetArray];
                                                  //выбираем первую фотографию
                                                  NSIndexPath *indexPath =
                                                  [NSIndexPath indexPathForItem:1 inSection:0];
                                                  
                                                  AssetsCollectionViewCell *cell = (AssetsCollectionViewCell *)[self
                                                                                                                collectionView:self.collectionView
                                                                                                                cellForItemAtIndexPath:indexPath];
                                                  
                                                  double delayInSeconds = 0.5f;
                                                  
                                                  dispatch_time_t popTime =
                                                  dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                                  
                                                  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                                                      
                                                      if (self.seletedImageArray.count < self.maxNumberOfSelections) {
                                                          
                                                          [cell selectButtonAction:cell];
                                                          
                                                          [self.collectionView reloadData];
                                                          
                                                      } else {
                                                          
                                                          [self.doneButton shakeBageAnimation];
                                                      }
                                                  });
                                              });
                                              
                                          }
                                      }];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
