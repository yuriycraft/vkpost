//
//  ImagePickerViewController.h
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import "NavigationBarTitleView.h"
#import "YCPhotoPickerViewController.h"
#import "AssetsModel.h"

static NSString *const kImagePickerStoryboardName = @"ImagePicker";
static NSString *const kImagePickerViewControllerIdentifier = @"ImagePickerViewController";


@interface ImagePickerViewController : YCPhotoPickerViewController

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (nonatomic, assign, readwrite) CGFloat collectionCellWidthCompareToScreen;

@property (nonatomic, strong, readwrite) UIColor *navigationBarColor;


- (IBAction)cancelButtonAction:(id)sender;

- (IBAction)doneButtonAction:(id)sender;

@end

