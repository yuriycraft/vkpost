//
//  AlbumsViewController.m
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "AlbumsViewController.h"
#import "AlbumTableViewCell.h"

static NSString *const kAlbumTableViewCellIdentifier = @"AlbumTableViewCell";

@interface AlbumsViewController ()

@end

@implementation AlbumsViewController

@synthesize delegate;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.tableView setSeparatorColor:[UIColor clearColor]];

}

#pragma mark - UITableViewDelegate.

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _assetsGroupArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AlbumTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAlbumTableViewCellIdentifier];

    [cell configureCellDataWithAssetCollection:_assetsGroupArray[indexPath.row]];
    
    [[cell checkMarkImageView] setHidden:YES];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(AlbumTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == _lastSelectedIndex) {
        
        [[cell checkMarkImageView] setHidden:NO];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.lastSelectedIndex = indexPath.row;
    
    [self.delegate albumsViewControllerSetIndexAlbum:indexPath.row];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)cancelButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
