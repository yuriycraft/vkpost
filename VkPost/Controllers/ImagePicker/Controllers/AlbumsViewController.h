//
//  AlbumsViewController.h
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AlbumsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
/**
 *  The assets group array.
 */
@property (nonatomic, strong, readwrite) NSMutableArray *assetsGroupArray;

@property(nonatomic, assign) NSInteger lastSelectedIndex;

@property(nonatomic, weak) id delegate;

- (IBAction)cancelButtonAction:(id)sender;

@end


//AlbumsViewControllerDelegate

@class AlbumsViewController;

@protocol AlbumsViewControllerDelegate <NSObject>

- (void)albumsViewControllerSetIndexAlbum:(NSInteger)index;

@end
