//
//  logInVC.m
//  VK Feed
//
//  Created by Dmitriy on 17.08.15.
//  Copyright (c) 2015 Dima. All rights reserved.
//

#import "AppDelegate.h"
#import "logInVC.h"

@interface logInVC ()

@end

@implementation logInVC


- (void)viewDidLoad {
    
  [super viewDidLoad];

  scope = @[
    VK_PER_FRIENDS,
    VK_PER_WALL,
    VK_PER_AUDIO,
    VK_PER_PHOTOS,
    VK_PER_NOHTTPS,
    VK_PER_EMAIL,
    VK_PER_MESSAGES,
    VK_PER_VIDEO,
    VK_PER_DOCS,
    
    
  ];

  [[VKSdk initializeWithAppId:@"5316500"] registerDelegate:self];
  [[VKSdk instance] setUiDelegate:self];
    
  }

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self wakeUp];

}

#pragma mark - Actions

-(void)wakeUp {
    
    [VKSdk wakeUpSession:scope
           completeBlock:^(VKAuthorizationState state, NSError *error) {
               
               if (state == VKAuthorizationAuthorized) {
                   
                   [self startWorking];
               }
               else if (error) {
                   

                   UIAlertController *alert = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[error description]
                                               preferredStyle:UIAlertControllerStyleAlert];
                   UIAlertAction *okButton =
                   [UIAlertAction actionWithTitle:@"OK"
                                            style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction *action){
                                             [self authorize];
                                          }];
                   [alert addAction:okButton];
                   [self presentViewController:alert animated:YES completion:nil];

                 
               }
               else {
                   
                 [self authorize];
               }
           }];

}

- (void)authorize {

  [VKSdk authorize:scope];
  
}

- (void)startWorking {

  [self performSegueWithIdentifier:@"main" sender:self];
}

#pragma mark - VKSdkDelegate

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {

  VKCaptchaViewController *vc =
      [VKCaptchaViewController captchaControllerWithError:captchaError];
  [vc presentIn:self.navigationController.topViewController];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {

  [self authorize];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:
    (VKAuthorizationResult *)result {

  if (result.token) {

    [self startWorking];
  }
  else if (result.error) {
 

    UIAlertController *alert = [UIAlertController
        alertControllerWithTitle:nil
                         message:[NSString
                                     stringWithFormat:@"Access denied\n%@",
                                                      result.error]
                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton =
        [UIAlertAction actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action){
                                   [self authorize];
                               }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];

  }
}

- (void)vkSdkUserAuthorizationFailed {

  UIAlertController *alert =
      [UIAlertController alertControllerWithTitle:nil
                                          message:@"Access denied"
                                   preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *okButton =
      [UIAlertAction actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction *action){
 [self authorize];
                             }];
  [alert addAction:okButton];
  [self presentViewController:alert animated:YES completion:nil];

    
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {


    [[self getTopMostViewController] presentViewController:controller animated:YES completion:nil];

}

#pragma mark - получение текущего (видимого) контроллера

- (UIViewController*) getTopMostViewController
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(window in windows) {
            if (window.windowLevel == UIWindowLevelNormal) {
                break;
            }
        }
    }
    
    for (UIView *subView in [window subviews])
    {
        UIResponder *responder = [subView nextResponder];
        
        //added this block of code for iOS 8 which puts a UITransitionView in between the UIWindow and the UILayoutContainerView
        if ([responder isEqual:window])
        {
            //this is a UITransitionView
            if ([[subView subviews] count])
            {
                UIView *subSubView = [subView subviews][0]; //this should be the UILayoutContainerView
                responder = [subSubView nextResponder];
            }
        }
        
        if([responder isKindOfClass:[UIViewController class]]) {
            return [self topViewController: (UIViewController *) responder];
        }
    }
    
    return nil;
}

- (UIViewController *) topViewController: (UIViewController *) controller
{
    BOOL isPresenting = NO;
    do {
        // this path is called only on iOS 6+, so -presentedViewController is fine here.
        UIViewController *presented = [controller presentedViewController];
        isPresenting = presented != nil;
        if(presented != nil) {
            controller = presented;
        }
        
    } while (isPresenting);
    
    return controller;
}


@end
