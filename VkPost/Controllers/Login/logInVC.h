//
//  logInVC.h
//  VK Feed
//
//  Created by Dmitriy on 17.08.15.
//  Copyright (c) 2015 Dima. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <VKSdk/VKSdk.h>

#import "VKSdk.h"

@interface logInVC : UIViewController<VKSdkDelegate,VKSdkUIDelegate>{
    
    NSArray *scope;
    UIViewController *visibleVC ;
    
}

@property (weak, nonatomic) IBOutlet UIButton *vkPassLogin;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;




@end
