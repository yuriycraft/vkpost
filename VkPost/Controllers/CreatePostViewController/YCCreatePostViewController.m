

//
//  YCCreatePostViewController.m
//  VkPost
//
//  Created by book on 18.08.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//


#import "DetailMapViewController.h"
#import "MapViewController.h"
#import "VKBundle.h"
#import "VKSdk.h"
#import "YCCreatePostViewController.h"
#import "YCPlaceholderTextView.h"
#import "YCVKPhotoAttachmentCell.h"
#import "YCVKShareSettingsController.h"
#import "YCVKUploadingAttachment.h"
#import "YCImagePickerViewController.h"
#import "YCAssetsModel.h"
#import "YCPhotoViewController.h"
#import "VFPhotoAttachment.h"
#import "VFDVKService+UploadFiles.h"
#import "VFDVKService+Wall.h"
#import "UIBarButtonItem+Badge.h"
#import "VFDAttachmentsCollectionView.h"
#import "YCVKPollModel.h"
#import "YCVKVideoUploadingModel.h"
#import "VFDDocAttachment.h"
#import "VFDGeoAttachment.h"
#import "VFDVideoAttachment.h"
#import "YCCeatePollViewController.h"



static NSString *const kToolBarImageNameLocation = @"7_newpost_panel_icon_location.png";
static NSString *const kToolBarImageNamePhoto = @"7_newpost_panel_icon_photo.png";
static NSString *const kToolBarImageNameSettings = @"7_newpost_panel_icon_settings.png";

static NSString *const kToolBarImageNameAnotherAttachment = @"7_newpost_panel_icon_attach.png";
static NSString *const kToolBarImageNameMusic = @"7_newpost_panel_icon_music.png";

static NSString *const kMainStoryboardName = @"Main";

static NSString *const kErrorUloadingPhotoMessage =@"Вы не можете прикрепить более 10 вложений";
static NSString *const kFontNameTextView = @"System";


@interface YCCreatePostViewController () <
UITextViewDelegate, UINavigationControllerDelegate,
UICollectionViewDelegate,
UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,
MapViewControllerDelegate,
UIGestureRecognizerDelegate, YCVKShareSettingsControllerDelegate, YCPhotoPickerViewControllerDelegate,VFDAttachmentsCollectionViewDelegate,YCCeatePollViewControllerDelegate>

@property(nonatomic, strong) UIToolbar *toolBarPost;
@property(assign, nonatomic) float height;

/// Links attachment for new post
//@property(nonatomic, strong) VKShareLink *shareLink;
@property(nonatomic, strong) YCVKPostSettings *postSettings;

@property(strong, nonatomic) CLLocation *location;
@property(strong, nonatomic) NSNumber *place_Id;
@property(strong, nonatomic) UIBarButtonItem *locationButton;
@property(strong, nonatomic) UIBarButtonItem *settingsButton;
@end

@implementation YCCreatePostViewController{
    
    CGFloat _keyboardSize;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self configTextView];
    [self configToolBar];
    [self getUserPostPermission];
    [self configObserver];
    
    self.collectionView.attachmentsCollectionViewDelegate = self;
    
    
}

- (void)dealloc {
    
    self.locationButton = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:YES];
    [self checkAttachment];
    [self.textView becomeFirstResponder];
}

#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification *)notification {
    
    CGSize keyboardSize =
    [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey]
     CGRectValue]
    .size;
    
    UIEdgeInsets contentInsets;
    
    if (UIInterfaceOrientationIsPortrait(
                                         [[UIApplication sharedApplication] statusBarOrientation])) {
        
         _keyboardSize =(self.view.frame.size.height - keyboardSize.height);
      
        if(self.heightSecondView.constant < _keyboardSize) {
        
            [self.heightSecondView setConstant:_keyboardSize];
        }
      
        contentInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top,
                                         0.f, (keyboardSize.height - 50), 0.f);
        
        
    } else {
        
        contentInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top,
                                         0.f, (keyboardSize.width), 0.f);
          _keyboardSize =(self.view.frame.size.width - keyboardSize.width);
    }
    
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    UIEdgeInsets contentInsets =
    UIEdgeInsetsMake(self.scrollView.contentInset.top, 0.f, 0.f, 0.f);
    
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma - mark UITextViewDelegate methods

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView
shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    
    [self checkAttachment];
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    [self.heightTextView setConstant:newFrame.size.height];
    [self.collectionView layoutIfNeeded];
    
    [self changeContentViewHeight];
    
   
}

#pragma mark - Actions

-(void)changeContentViewHeight {
    
    CGFloat heightContent = (50 + self.collectionViewHeight.constant +
                             self.heightTextView.constant);
    
    if(self.heightSecondView.constant <= heightContent){
        
        [self.heightSecondView setConstant:heightContent];
        
    } else {
        
        [self.heightSecondView setConstant:_keyboardSize];
    }
    [self.scrollView
     setContentSize:CGSizeMake( self.textView.frame.size.width, self.heightSecondView.constant)];
}

// Calculate frame from NSString text size
- (CGSize)text:(NSString *)text constrainedToSize:(CGSize)size {
    
    UIFont *font = [UIFont fontWithName:kFontNameTextView size:14];
    
    CGRect frame =
    [text boundingRectWithSize:size
                       options:(NSStringDrawingUsesLineFragmentOrigin |
                                NSStringDrawingUsesFontLeading)
                    attributes:@{
                                 
                                 NSFontAttributeName : font
                                 }
                       context:nil];
    return frame.size;
}


- (IBAction)openPhotoController:(id)sender {
    
    self.collectionView.maxAttachmentSelectedItems = kMaxSelectedAttachment - self.collectionView.attachmentsArray.count;
    
    if (self.collectionView.maxAttachmentSelectedItems <= 0  ) {
        
        [self showAlertWithMessage:kErrorUloadingPhotoMessage];
        
    } else {
        
        UIAlertController *actionSheet = [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet
         addAction:[UIAlertAction actionWithTitle:@"Отмена"
                                            style:UIAlertActionStyleCancel
                                          handler:^(UIAlertAction *action){
                                              
                                              // Cancel button tappped.
                                              
                                          }]];
        
        [actionSheet
         addAction:[UIAlertAction
                    actionWithTitle:@"Фото"
                    style:UIAlertActionStyleDefault
                    handler:^(UIAlertAction *action) {
                        
                        [self choosePhotoFromExistingImagesWithType:kPhotoPickerType];
                        
                    }]];
        
        [actionSheet
         addAction:[UIAlertAction actionWithTitle:@"Фото VK"
                                            style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction *action) {
                                              
                                              [self  choosePhotoFromVKImages];
                                              
                                          }]];
        
        // if iPad
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            actionSheet.popoverPresentationController.barButtonItem = self.toolBarPhotoButton;
            actionSheet.popoverPresentationController.sourceView = self.view;
            
        }
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
        
    }
}

- (IBAction)cancelButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(NSMutableDictionary*)createSendParameters {
    
    NSMutableArray *attachStrings =
    [NSMutableArray arrayWithCapacity:self.collectionView.attachmentsArray.count];
    
    for (int i = 0; i < self.collectionView.attachmentsArray.count; i++) {
        
        if ([self.collectionView.attachmentsArray[i]
             isKindOfClass:[YCVKUploadingAttachment class]]) {
            
            YCVKUploadingAttachment *attach = self.collectionView.attachmentsArray[i];
            
            if(attach.status == kDoneUploading && attach.attachmentString) {
                
                [attachStrings addObject:attach.attachmentString];
            }
            
        }  else {
            
            [self performSelector:@selector(sendPostOnWall:)
                       withObject:self
                       afterDelay:1.0f];
            return nil;
        }
    }
    
    NSMutableDictionary *paramsPost = [NSMutableDictionary
                                       dictionaryWithObjectsAndKeys:self.textView.text ?: @"", VK_API_MESSAGE,
                                       [attachStrings
                                        componentsJoinedByString:@","],
                                       VK_API_ATTACHMENTS, nil];
    
    if (self.place_Id) {
        
        [paramsPost setObject:self.place_Id forKey:@"place_id"];
    }
    else if (self.location) {
        
        [paramsPost
         setObject:[NSNumber numberWithDouble:self.location.coordinate.latitude]
         forKey:@"lat"];
        [paramsPost
         setObject:[NSNumber numberWithDouble:self.location.coordinate.longitude]
         forKey:@"long"];
    }
    if (self.postSettings.toTime) {
        
        int timestamp = [self.postSettings.date timeIntervalSince1970];
        [paramsPost setObject:[NSNumber numberWithInt:timestamp]
                       forKey:@"publish_date"];
    }
    
    
    NSMutableArray *exports = [NSMutableArray new];
    
    if (self.postSettings.friendsOnly.boolValue)
        [paramsPost setObject:@1 forKey:VK_API_FRIENDS_ONLY];
    
    if (self.postSettings.exportTwitter)
        [exports addObject:kSettingsTwitter];
    
    if (self.postSettings.exportFacebook)
        [exports addObject:kSettingsFacebook];
    
    if (self.postSettings.exportLivejournal)
        [exports addObject:kSettingsLivejurnal];
    
    if (exports.count)
        [paramsPost setObject:[exports componentsJoinedByString:@","] forKey:VK_API_SERVICES];
    
    return paramsPost;
}

//Send post on your wall in vk
- (IBAction)sendPostOnWall:(id)sender {
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc]
                                         initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithCustomView:activity];
    
    [activity startAnimating];
    
    
    NSMutableDictionary * params = [self createSendParameters];
    
    self.textView.editable = NO;
    [self.textView endEditing:YES];
    
    
    [VKSERVICE sendPostOnWallWithParametrs:params completion:^(VKResponse *response, NSError *error) {
        
        if(response) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            
            self.navigationItem.rightBarButtonItem =
            [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                             style:UIBarButtonItemStyleDone
                                            target:self
                                            action:@selector(sendPostOnWall:)];
            
            self.textView.editable = YES;
            [self.textView becomeFirstResponder];
            [self showAlertWithMessage:@"Ошибка постинга"];
        }
    }];
    
}

- (IBAction)openSettingsController:(id)sender {
    
    YCVKShareSettingsController *vc =
    [[UIStoryboard storyboardWithName:kMainStoryboardName bundle:nil]
     instantiateViewControllerWithIdentifier:kSettingsViewController];
    [vc initWithPostSettings:self.postSettings];
    vc.delegate = self;
    UINavigationController *navController =
    [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self presentViewController:navController animated:YES completion:nil];
    
}

- (IBAction)openAnotherAttachment:(id)sender {
    
    self.collectionView.maxAttachmentSelectedItems = kMaxSelectedAttachment - self.collectionView.attachmentsArray.count;
    
    if (self.collectionView.maxAttachmentSelectedItems <= 0  ) {
        
        [self showAlertWithMessage:kErrorUloadingPhotoMessage];
        
    } else {
        
        UIAlertController *actionSheet = [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet
         addAction:[UIAlertAction actionWithTitle:@"Отмена"
                                            style:UIAlertActionStyleCancel
                                          handler:^(UIAlertAction *action){
                                              
                                              // Cancel button tappped.
                                              
                                          }]];
        
        [actionSheet
         addAction:[UIAlertAction
                    actionWithTitle:@"Документы"
                    style:UIAlertActionStyleDefault
                    handler:^(UIAlertAction *action) {
                        
                        [self choosePhotoFromExistingImagesWithType:kDocumentPickerType];
                        
                    }]];
        
        
        [actionSheet
         addAction:[UIAlertAction
                    actionWithTitle:@"Видео"
                    style:UIAlertActionStyleDefault
                    handler:^(UIAlertAction *action) {
                        
                        [self choosePhotoFromExistingImagesWithType:kVideoPickerType];
                        
                    }]];
        
        if(!self.collectionView.isPoll){
            [actionSheet
             addAction:[UIAlertAction
                        actionWithTitle:@"Опрос"
                        style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction *action) {
                            
                            [self createPoll];
                            
                        }]];
        }
        // if iPad
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            actionSheet.popoverPresentationController.barButtonItem = self.toolBarPhotoButton;
            actionSheet.popoverPresentationController.sourceView = self.view;
            
        }
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
        
    }
}

-(void)showAlertWithMessage:(NSString*)message {
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton =
    [UIAlertAction actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action) {
                               
                           }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)openLocationController:(id)sender {
    
    if (!self.location && !self.place_Id) {
        
        [self showLocationController];
        
    } else {
        
        UIAlertController *actionSheet = [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet
         addAction:[UIAlertAction actionWithTitle:@"Отмена"
                                            style:UIAlertActionStyleCancel
                                          handler:^(UIAlertAction *action){
                                              
                                              // Cancel button tappped.
                                              
                                          }]];
        
        [actionSheet
         addAction:[UIAlertAction
                    actionWithTitle:@"Удалить"
                    style:UIAlertActionStyleDestructive
                    handler:^(UIAlertAction *action) {
                        
                        self.location = nil;
                        self.locationLabel.hidden = YES;
                        self.locationLabel.text = @"";
                        self.place_Id = nil;
                        
                        [self.locationButton
                         setTintColor:[UIColor lightGrayColor]];
                        [self.toolBarLocationButton
                         setTintColor:[UIColor lightGrayColor]];
                        [self checkAttachment];
                        
                    }]];
        
        [actionSheet
         addAction:[UIAlertAction actionWithTitle:@"Изменить"
                                            style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction *action) {
                                              
                                              [self showLocationController];
                                              
                                          }]];
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
}

- (void)showLocationController {
    
    MapViewController *contentController =
    [[UIStoryboard storyboardWithName:kMainStoryboardName bundle:nil]
     instantiateViewControllerWithIdentifier:kMapViewController];
    contentController.delegate = self;
    UINavigationController *navController = [[UINavigationController alloc]
                                             initWithRootViewController:contentController];
    navController.navigationBar.barTintColor = [UIColor colorWithRed:94 / 255.f
                                                               green:133 / 255.f
                                                                blue:169 / 255.f
                                                               alpha:1.f];
    navController.navigationBar.tintColor = [UIColor whiteColor];
    [navController.navigationBar setTitleTextAttributes:@{
                                                          NSForegroundColorAttributeName : [UIColor whiteColor]
                                                          }];
    [self presentViewController:navController animated:YES completion:nil];
}


- (void)checkAttachment {
    
    
    if (self.textView.text.length > 0 ||
        self.place_Id || self.location ) {
        
        self.rightDoneButton.enabled = YES;
        
        
        if (self.location || self.place_Id) {
            //set selected bar button for location
            
            UIColor *color = [UIColor colorWithRed:94 / 255.f
                                             green:133 / 255.f
                                              blue:169 / 255.f
                                             alpha:1.f];
            [self.locationButton setTintColor:color];
            [self.toolBarLocationButton setTintColor:color];
            [self.toolBar sizeToFit];
        }
    }
    else {
        
        self.rightDoneButton.enabled = NO;
        
    }
    
    if (self.collectionView.attachmentsArray.count > 0) {
        
        //   check upload Attach
        for(YCVKUploadingAttachment* attachment in self.collectionView.attachmentsArray) {//перебираем вложения
            
            if(attachment.status != kDoneUploading) {//если ошибка или не догруз
                
                self.rightDoneButton.enabled = NO;
                return;
            }
            else {
                self.rightDoneButton.enabled = YES;
                
            }
        }
    }
    
    
    // set color toolbar button if select on settings
    if(self.postSettings) {
        
        if([self.postSettings.friendsOnly isEqual:@1] ||
           self.postSettings.toTime||
           self.postSettings.exportTwitter ||
           self.postSettings.exportFacebook ||
           self.postSettings.exportLivejournal) {

            UIColor *color = [UIColor colorWithRed:94 / 255.f
                                             green:133 / 255.f
                                              blue:169 / 255.f
                                             alpha:1.f];
            [self.settingsButton setTintColor:color];
            [self.toolBarSettingsButton setTintColor:color];

        }
        else {
         
            
            [self.settingsButton setTintColor:[UIColor lightGrayColor]];
            [self.toolBarSettingsButton setTintColor:[UIColor lightGrayColor]];
        }
        
    }

}

#pragma mark - prepare

- (void)getUserPostPermission {
    
    self.postSettings = [YCVKPostSettings new];
    
    [VKSERVICE getPostUserPermissionCompletion:^(VKUser *user, NSError *error) {
        
        if(user){
            
            self.postSettings.friendsOnly = @0;
            
            if (user.exports.twitter) {
                
                self.postSettings.exportTwitter = YES;
            }
            if (user.exports.facebook) {
                
                self.postSettings.exportFacebook = YES;
            }
            if (user.exports.livejournal) {
                
                self.postSettings.exportLivejournal = YES;
            }
            self.postSettings.toTime = NO;
        }
    }];
    
}

#pragma mark - Configs

-(void)configObserver {
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(checkAttachment)
     name:ATTACHMENT_LOADING_DID_END_NOTIFICATION
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(checkAttachment)
     name:ATTACHMENT_UPLOAD_ERROR_NOTIFICATION
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(checkAttachment)
     name:ATTACHMENT_REMOVE_NOTIFICATION
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(changeContentViewHeight)
     name:ATTACHMENT_REMOVE_NOTIFICATION
     object:nil];
    
    
} 

- (void)configTextView {
    
    _textView.font = [UIFont systemFontOfSize:18.0f];
    _textView.delegate = self;
    _textView.textAlignment = NSTextAlignmentLeft;
    _textView.returnKeyType = UIReturnKeyDone;
    _textView.placeholder = VKLocalizedString(@"Что у Вас нового?");
    
    [self.textView becomeFirstResponder];
}

- (void)configToolBar {
    
    _toolBarPost = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    _toolBarPost.barStyle = UIBarStyleDefault;
    _toolBarPost.tintColor = [UIColor lightGrayColor];
    
    self.locationButton = [[UIBarButtonItem alloc]
                           initWithImage:[UIImage imageNamed:kToolBarImageNameLocation]
                           style:UIBarButtonItemStyleDone
                           target:self
                           action:@selector(openLocationController:)];
    
    self.settingsButton =   [[UIBarButtonItem alloc]
                             initWithImage:[UIImage imageNamed:kToolBarImageNameSettings]
                             style:UIBarButtonItemStyleDone
                             target:self
                             action:@selector(openSettingsController:)];
    
    _toolBarPost.items = @[
                           [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                            target:nil
                            action:nil],
                           
                           self.locationButton,
                           
                           [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                            target:nil
                            action:nil],
                           
                           [[UIBarButtonItem alloc]
                            initWithImage:[UIImage imageNamed:kToolBarImageNamePhoto]
                            style:UIBarButtonItemStyleDone
                            target:self
                            action:@selector(openPhotoController:)],
                           [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                            target:nil
                            action:nil],
                           [[UIBarButtonItem alloc]
                            initWithImage:[UIImage imageNamed:kToolBarImageNameAnotherAttachment]
                            style:UIBarButtonItemStyleDone
                            target:self
                            action:@selector(openAnotherAttachment:)],
                           [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                            target:nil
                            action:nil],
                           self.settingsButton,
                           [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                            target:nil
                            action:nil]
                           ];
    [_toolBarPost sizeToFit];
    
    _textView.inputAccessoryView = _toolBarPost;
}

#pragma mark - VKPhoto Picker

- (void)choosePhotoFromVKImages {
    
    YCPhotoViewController *photoVkController =  [[UIStoryboard storyboardWithName:kPhotoStoryboardName bundle:nil] instantiateViewControllerWithIdentifier:kYCPhotoViewControllerIdentifier];
    
    photoVkController.imagePickerDelegate = self ;
    
    photoVkController.ownerId = [VKSdk accessToken].userId;
    
    photoVkController.maxNumberOfSelections = self.collectionView.maxAttachmentSelectedItems;
    
    photoVkController.isPicker = YES;
    
    UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:photoVkController];
    
    nc.navigationBar.tintColor = [UIColor whiteColor];
    
    [nc.navigationBar setTitleTextAttributes:@{
                                               NSForegroundColorAttributeName : [UIColor whiteColor]
                                               }];
    
    nc.navigationBar.barTintColor = [UIColor colorWithRed:94 / 255.f
                                                    green:133 / 255.f
                                                     blue:169 / 255.f
                                                    alpha:1.f];
    [self presentViewController:nc animated:YES completion:NULL];
}

-(void) createPoll {
    
    YCCeatePollViewController * pollController =  [[YCCeatePollViewController alloc] initWithNibName:@"YCCeatePollViewController" bundle:nil];
    
    pollController.delegate = self;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:pollController];
    
    [self presentViewController:nc animated:YES completion:nil];
    
}

#pragma mark - Photo Picker
- (void)choosePhotoFromExistingImagesWithType:(NSUInteger)ycPickerControllerType {
    
    YCImagePickerViewController *contentController = [[UIStoryboard storyboardWithName:kImagePickerStoryboardName bundle:nil] instantiateViewControllerWithIdentifier:kImagePickerViewControllerIdentifier];
    
    contentController.imagePickerDelegate = self;
    
    contentController.type = ycPickerControllerType;
    
    contentController.maxNumberOfSelections = self.collectionView.maxAttachmentSelectedItems;
    
    UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:contentController];
    
    [self presentViewController:nc animated:YES completion:nil];
}

#pragma mark - YCPhotoPickerViewControllerDelegate
//Получение фото или видео
- (void)ycPhotoPicker:(UIViewController *)picker didFinishPickingMediaWithImageArray:(NSArray *)mediaArray {
    
    if([picker isKindOfClass:[YCImagePickerViewController class]]) {//если видео или фото из галереи
        
        PHImageManager *imageManager = [[PHImageManager alloc]init];
        
        
        for (YCAssetsModel *asset in mediaArray) {
            
            if(asset.mediaType == kPhotoPickerType ) {
                
                dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    @autoreleasepool {
                        
                        [imageManager requestImageForAsset:asset.imageAsset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeDefault options:PHImageRequestOptionsVersionCurrent resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                            
                            if(result){
                                dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    
                                    VKUploadImage *imageVk = [VKUploadImage
                                                              uploadImageWithImage:result
                                                              andParams:
                                                              [VKImageParameters
                                                               jpegImageWithQuality:1.0f]];
                                    dispatch_async( dispatch_get_main_queue(), ^{
                                        
                                        [self.collectionView addAttachments:imageVk withPreviewImage:nil andExtensionString:nil andUploadData:nil];
                                        
                                    });
                                    
                                });
                            }
                            
                            
                            
                        }];
                    }
                });
            }
            else  if(asset.mediaType == kDocumentPickerType) {
                
                @autoreleasepool {
                    
                    [imageManager requestImageDataForAsset:asset.imageAsset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
                        
                        NSURL * url = info[@"PHImageFileURLKey"];
                        
                        NSString *path = [url path];
                        
                        NSString *extension = [path pathExtension];
                        
                        VFDDocAttachment * doc = [VFDDocAttachment new];
                        
                        doc.title = url.lastPathComponent;;
                        
                        doc.ext = extension;
                        
                        dispatch_async( dispatch_get_main_queue(), ^{
                            
                            [self.collectionView addAttachments:doc withPreviewImage:asset.image andExtensionString:extension andUploadData:imageData];
                        });
                    }];
                    
                }
            }
            
            else if(asset.mediaType == kVideoPickerType) {
                
                YCVKVideoUploadingModel * video = [YCVKVideoUploadingModel new];
                video.duration = asset.duration;
                
                PHVideoRequestOptions *options = [PHVideoRequestOptions new];
                options.networkAccessAllowed = YES;
                [[PHImageManager defaultManager] requestAVAssetForVideo:asset.imageAsset options:options resultHandler:^(AVAsset *assetAV, AVAudioMix *audioMix, NSDictionary *info) {
                    
                    if(([assetAV isKindOfClass:[AVComposition class]] && ((AVComposition *)assetAV).tracks.count == 2)){
                        //slow motion videos. See Here: https://overflow.buffer.com/2016/02/29/slow-motion-video-ios/
                        
                        //Output URL of the slow motion file.
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *documentsDirectory = paths.firstObject;
                        NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"mergeSlowMoVideo-%d.mov",arc4random() % 1000]];
                        NSURL *url = [NSURL fileURLWithPath:myPathDocs];
                        
                        //Begin slow mo video export
                        AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:assetAV presetName:AVAssetExportPresetHighestQuality];
                        exporter.outputURL = url;
                        exporter.outputFileType = AVFileTypeQuickTimeMovie;
                        exporter.shouldOptimizeForNetworkUse = YES;
                        
                        [exporter exportAsynchronouslyWithCompletionHandler:^{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (exporter.status == AVAssetExportSessionStatusCompleted) {
                                    NSURL *URL = exporter.outputURL;
                                    //self.filePath=URL.absoluteString;

                                NSData *uploadData = [NSData dataWithContentsOfURL:URL];
                                    
                                    NSString *path = [URL path];
                                    
                                    asset.videoData = uploadData;
                                    //
                                                        NSString *extension = [path pathExtension];
                                    
                                                        video.name = URL.lastPathComponent;
                                    
                                    
                                    
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            [self.collectionView addAttachments:video withPreviewImage:asset.image andExtensionString:extension andUploadData:uploadData];
                                                        });

                                    //
                                    //// Upload
                                    //[self uploadSelectedVideo:video data:videoData];
                                }
                            });
                        }];
                        
                        
                    }          else if ([assetAV isKindOfClass: [AVURLAsset class]]) {
                        
                                      asset.url = [(AVURLAsset*)assetAV URL];
                        
                        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:assetAV presetName:AVAssetExportPresetMediumQuality];
                        
                                        exportSession.outputURL = asset.url;
                                        exportSession.outputFileType = AVFileTypeQuickTimeMovie;
                                        exportSession.shouldOptimizeForNetworkUse = YES;
                                        [exportSession exportAsynchronouslyWithCompletionHandler:^{
                        
                                            NSData * uploadData = [NSData dataWithContentsOfURL:asset.url];
                        
                                            NSString *path = [asset.url path];
                        
                                            NSString *extension = [path pathExtension];
                        
                                            video.name = asset.url.lastPathComponent;
                                            
                                            
                        
                                          //  video.duration = asset.duration;
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                
                                                [self.collectionView addAttachments:video withPreviewImage:asset.image andExtensionString:extension andUploadData:uploadData];
                                            });

                        
                                        }];
                    
                                
                       
                            }                 }];
                
                
//                AVURLAsset *urlAsset = [AVURLAsset URLAssetWithURL:asset.url options:nil];
//                
//                AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:urlAsset presetName:AVAssetExportPresetMediumQuality];
//                
//                exportSession.outputURL = asset.url;
//                exportSession.outputFileType = AVFileTypeQuickTimeMovie;
//                exportSession.shouldOptimizeForNetworkUse = YES;
//                [exportSession exportAsynchronouslyWithCompletionHandler:^{
//                    
//                    NSData * uploadData = [NSData dataWithContentsOfURL:asset.url];
//                    
//                    NSString *path = [asset.url path];
//                    
//                    NSString *extension = [path pathExtension];
//                    
//                    video.name = asset.url.lastPathComponent;
//                    
//                    video.duration = asset.duration;
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        
//                        [self.collectionView addAttachments:video withPreviewImage:asset.image andExtensionString:extension andUploadData:uploadData];
//                    });
//                }];
//            }
            
       }
        }
    } else { // если фото из вк
        
        for (VFPhotoAttachment *photo in mediaArray) {
            
            [self.collectionView addAttachments:photo withPreviewImage:nil andExtensionString:nil andUploadData:nil];
        }
    }
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    
    return self.collectionView.attachmentsArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView {
    
    return 1;
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize size = CGSizeMake(85.f, 85.f);
    return size;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    YCVKPhotoAttachmentCell *cell = (YCVKPhotoAttachmentCell *)[collectionView
                                                                dequeueReusableCellWithReuseIdentifier:kVKPhotoAttachmentCell
                                                                forIndexPath:indexPath];
    
    YCVKUploadingAttachment* attach =  self.collectionView.attachmentsArray[(NSUInteger)indexPath.item];
    
    cell.closeView.tag = indexPath.row;
    
    cell.maskView.tag = attach.uid;
    
    [cell configUloadingCellWithAttachment:attach andVFDAttachmentsCollectionView:self.collectionView andGestureDelegate:self andIndexPath:indexPath];
    
    return cell;
}


#pragma mark - MapViewControllerDelegate

- (void)setStringLocation:(NSString *)stringLocation
            andCLLocation:(CLLocation *)location {
    
    if (stringLocation) {
        
        NSArray *listItems = [stringLocation componentsSeparatedByString:@", "];
        
        if (listItems.count > 1) {
            
            self.locationLabel.hidden = NO;
            self.locationLabel.text =
            [NSString stringWithFormat:@"%@, %@", listItems[0], listItems[1]];
        }
    }
    if (location) {
        
        self.location = location;
    }
    [self.collectionView changeCollectionViewHeight];
}

#pragma mark - DetailMapViewControllerDelegate

- (void)setStringLocation:(NSString *)stringLocation
              andIdPlaces:(NSNumber *)place_id {
    
    if (place_id && stringLocation) {
        
        self.locationLabel.hidden = NO;
        self.locationLabel.text = stringLocation;
        self.place_Id = place_id;
        self.location = nil;
        [self.collectionView changeCollectionViewHeight];
    }
}

#pragma mark -  YCVKShareSettingsControllerDelegate

- (void)setupPostSettings:(YCVKPostSettings *)postSettings {
    
    self.postSettings = postSettings;
   
    if(self.postSettings.toTime && self.postSettings.date){

        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateStyle = NSDateFormatterShortStyle;
        formatter.timeStyle = NSDateFormatterShortStyle;
        
        formatter.doesRelativeDateFormatting = YES;
        NSString * dateString = [formatter stringFromDate:self.postSettings.date];
        
        self.timeToLabel.text = [NSString stringWithFormat:@"Опубликовать %@",dateString];
        self.timeToLabel.hidden = NO;
        
    }else {
        
        self.timeToLabel.text = @"";
        self.timeToLabel.hidden = NO;
    }
}

#pragma mark - VFDAttachmentsCollectionViewDelegate
-(void)changeCollectionViewHeight:(float) heightCollectionView {
    
    self.collectionViewHeight.constant = heightCollectionView;
    
    [self textViewDidChange:self.textView];
    [self.collectionView performBatchUpdates:^{
        [self.collectionView reloadData];
    } completion:^(BOOL finished) {}];
}

#pragma mark - YCCeatePollViewControllerDelegateYCVKPollModel
-(void)setupYCVKPollModel:(YCVKPollModel *)pollModel{
    
    [self.collectionView addAttachments:pollModel withPreviewImage:nil andExtensionString:nil andUploadData:nil];
    
    self.collectionView.isPoll = YES;
}
@end
