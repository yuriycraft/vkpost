//
//  YCCreatePostViewController.h
//  VkPost
//
//  Created by book on 18.08.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YCPlaceholderTextView,VFDAttachmentsCollectionView;

@interface YCCreatePostViewController : UIViewController

@property(weak, nonatomic) IBOutlet UIBarButtonItem *rightDoneButton;
@property(weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property(weak, nonatomic) IBOutlet YCPlaceholderTextView *textView;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextView;
@property(weak, nonatomic) IBOutlet VFDAttachmentsCollectionView *collectionView;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@property(weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *heightSecondView;
@property(weak, nonatomic) IBOutlet UILabel *locationLabel;
@property(weak, nonatomic) IBOutlet UIBarButtonItem *toolBarLocationButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toolBarSettingsButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toolBarPhotoButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toolBarAnotherAttachButton;
@property (weak, nonatomic) IBOutlet UILabel *timeToLabel;

- (IBAction)sendPostOnWall:(id)sender;
- (IBAction)openSettingsController:(id)sender;
- (IBAction)openLocationController:(id)sender;

- (IBAction)openPhotoController:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;
- (IBAction)openAnotherAttachment:(id)sender;

@end
