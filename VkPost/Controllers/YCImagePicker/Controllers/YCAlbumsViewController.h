//
//  YCAlbumsViewController.h
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCAlbumsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong, readwrite) NSMutableArray *assetsGroupArray;

@property(nonatomic, assign) NSInteger lastSelectedIndex;

@property(nonatomic, weak) id delegate;

- (IBAction)cancelButtonAction:(id)sender;

@end


//AlbumsViewControllerDelegate

@class YCAlbumsViewController;

@protocol YCAlbumsViewControllerDelegate <NSObject>

- (void)albumsViewControllerSetIndexAlbum:(NSInteger)index;

@end
