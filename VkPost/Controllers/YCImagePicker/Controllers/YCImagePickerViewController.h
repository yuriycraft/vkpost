//
//  YCImagePickerViewController.h
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import "YCNavigationBarTitleView.h"
#import "YCPhotoPickerViewController.h"
#import "YCAssetsModel.h"

static NSString *const kImagePickerStoryboardName = @"YCImagePicker";
static NSString *const kImagePickerViewControllerIdentifier = @"ImagePickerViewController";

typedef enum PickerType : NSUInteger {
    
    kAllPickerType      = 1,
    kPhotoPickerType    = 2,
    kVideoPickerType    = 3,
    kDocumentPickerType = 4
    
} PickerType;

@interface YCImagePickerViewController : YCPhotoPickerViewController

@property (assign , nonatomic) NSUInteger type;

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (nonatomic, assign, readwrite) CGFloat collectionCellWidthCompareToScreen;

@property (nonatomic, strong, readwrite) UIColor *navigationBarColor;


- (IBAction)cancelButtonAction:(id)sender;

- (IBAction)doneButtonAction:(id)sender;

@end

