//
//  YCPhotoPickerViewController.h
//  VkPost
//
//  Created by book on 08.12.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//
//
//Этот класс РОДИТЕЛЬ для всех пикеров используемых в проекте

#import <UIKit/UIKit.h>

#define HEIGHT_TOOLBAR_CONSTANT 44.0
#define PICKER_COUNT_CELLS_IN_LINE 3

@class YCPhotoPickerViewController;

@protocol YCPhotoPickerViewControllerDelegate <NSObject>

@optional

- (void)ycPhotoPicker:(YCPhotoPickerViewController *)picker didFinishPickingMediaWithImageArray:(NSArray *)mediaArray;

@end

@interface YCPhotoPickerViewController : UIViewController

@property(assign, nonatomic) BOOL isPicker;
@property(nonatomic, weak, readwrite) id <YCPhotoPickerViewControllerDelegate> imagePickerDelegate;
@property(nonatomic , assign) NSInteger maxNumberOfSelections;
@property(nonatomic, strong, readwrite) NSMutableArray *seletedImageArray;

@end
