//
//  YCAlbumsViewController.m
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "YCAlbumsViewController.h"
#import "YCAlbumTableViewCell.h"

static NSString *const kAlbumTableViewCellIdentifier = @"AlbumTableViewCell";

@interface YCAlbumsViewController ()

@end

@implementation YCAlbumsViewController

@synthesize delegate;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _assetsGroupArray.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(YCAlbumTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == _lastSelectedIndex) {
        
        [[cell checkMarkImageView] setHidden:NO];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    YCAlbumTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAlbumTableViewCellIdentifier];
    
    [cell configureCellDataWithAssetCollection:_assetsGroupArray[indexPath.row]];
    
    [[cell checkMarkImageView] setHidden:YES];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.lastSelectedIndex = indexPath.row;
    
    [self.delegate albumsViewControllerSetIndexAlbum:indexPath.row];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Actions

- (IBAction)cancelButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
