//
//  YCImagePickerViewController.m
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "YCAlbumsViewController.h"
#import "YCAssetsCollectionViewCell.h"
#import "YCAssetsModel.h"
#import "YCImagePickerViewController.h"
#import "UIBarButtonItem+Badge.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>

static NSString *const kCameraCellId = @"CameraCollectionViewCell";
static NSString *const kPhotoCellId = @"AssetsCollectionViewCell";
static NSString *const kNotAuthorizationImageName = @"notAuthorization_picker.png";
static NSString *const kAlbumsViewControllerIdentifier = @"AlbumsViewController";
static float itemMinSpasing = 2.0;

@interface YCImagePickerViewController () <
UICollectionViewDelegate, UICollectionViewDataSource,
YCAlbumsViewControllerDelegate, YCPhotoCollectionViewCellDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property(nonatomic, strong, readwrite) NSMutableArray *assetsGroupArray;

@property(nonatomic, strong, readwrite) NSMutableDictionary *assetsDic;

@property(nonatomic, strong, readwrite) NSMutableDictionary *cellDic;

@property(nonatomic, assign, readwrite) NSInteger currentAssetsIndex;

@property(nonatomic, strong, readwrite) PHImageManager *imageManager;

@property(nonatomic, strong, readwrite) PHFetchResult *currentFetchResult;

@property(nonatomic, strong) UIViewController *cameraController;

@property(nonatomic, assign)BOOL isCameraAvailable;


@property(nonatomic, strong, readwrite)
UIImageView *authorizationStatusDeniedView;

@property(nonatomic, strong, readwrite)
YCNavigationBarTitleView *navigationBarTitleView;

@end

@implementation YCImagePickerViewController{
    CGFloat countItemsInLine;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self getItemsCountOfLine];
    
    self.type = self.type == 0 ?  kDocumentPickerType : self.type ;
    
    _assetsDic = [[NSMutableDictionary alloc] init];
    
    _assetsGroupArray = [[NSMutableArray alloc] init];
    
    self.seletedImageArray = [[NSMutableArray alloc] init];
    
    _imageManager = [[PHCachingImageManager alloc] init];
    
    [self initAuthorizationStatusDeniedView];
    
    [self judgeIfHavePhotoAblumAuthority];
    
    [self initNavigationBarTitleView];
    
    [self.collectionView setScrollsToTop:YES];
}


-(void)getItemsCountOfLine {
    
    countItemsInLine = 3;
    
    if([[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeLeft || [[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeRight){
        countItemsInLine *= 2;
    }
    
    // if iPad
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        countItemsInLine *= 2;
    }
   

}
#pragma mark - setter and getter

- (CGFloat)collectionCellWidthCompareToScreen {
    
    
    if (!_collectionCellWidthCompareToScreen) {
        
       
        
        return countItemsInLine; //количество фото вмещающихся в строку
        
    } else {
        
        return _collectionCellWidthCompareToScreen;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

#pragma mark - Init

- (void)initAuthorizationStatusDeniedView {
    
    if (!_authorizationStatusDeniedView) {
        
        _authorizationStatusDeniedView = [[UIImageView alloc]
                                          initWithFrame:CGRectMake(0, 0, self.view.frame.size.width / 2,
                                                                   self.view.frame.size.width / 2)];
        
        _authorizationStatusDeniedView.center = self.view.center;
        
        _authorizationStatusDeniedView.image =
        [UIImage imageNamed:kNotAuthorizationImageName];
        
        _authorizationStatusDeniedView.hidden = YES;
        
        [self.view addSubview:_authorizationStatusDeniedView];
    }
}

- (void)initNavigationBarTitleView {
    
    if (!_navigationBarTitleView) {
        
        _navigationBarTitleView = [[YCNavigationBarTitleView alloc]
                                   initWithFrame:CGRectMake(0, 0, 120.f,
                                                            self.navigationController.navigationBar.bounds
                                                            .size.height)
                                   selectedBlock:^{
                                       
                                       YCAlbumsViewController *contentController = [
                                                                                    [UIStoryboard storyboardWithName:kImagePickerStoryboardName bundle:nil]
                                                                                    instantiateViewControllerWithIdentifier:kAlbumsViewControllerIdentifier];
                                       contentController.assetsGroupArray = _assetsGroupArray;
                                       contentController.delegate = self;
                                       contentController.lastSelectedIndex = self.currentAssetsIndex;
                                       UINavigationController *nav = [[UINavigationController alloc]
                                                                      initWithRootViewController:contentController];
                                       
                                       [self presentViewController:nav animated:YES completion:nil];
                                       
                                   }];
        
        self.navigationItem.titleView = _navigationBarTitleView;
        
        if (_assetsGroupArray.count) {
            
            [_navigationBarTitleView.navigationBarTitleButton
             setTitle:[_assetsGroupArray[0] localizedTitle]
             forState:UIControlStateNormal];
        } else {
            
            [_navigationBarTitleView.navigationBarTitleButton
             setTitle:@"Все фото"
             forState:UIControlStateNormal];
        }
    }
}

#pragma mark - authorization judgement.

- (void)judgeIfHavePhotoAblumAuthority { // получаем статус авторизации
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusRestricted ||
        status == PHAuthorizationStatusDenied ) {  // если нет доступа
        
        [self showErrorAccessPhoto];
        
        self.doneButton.enabled = NO;
        self.authorizationStatusDeniedView.hidden =NO;
        
    } else if (status == PHAuthorizationStatusNotDetermined) { // если доступ разшен получаем авторизацию
        
        self.doneButton.enabled = NO;
        
        //Запрос авторизации
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                
                [self initAlbumAssetsLibrary];
                self.authorizationStatusDeniedView.hidden = YES;
                self.doneButton.enabled = NO;
            }
        }];
        
        
    }  else    if (status == PHAuthorizationStatusAuthorized) { // если получена авторизация
        
        [self initAlbumAssetsLibrary];
        
        self.doneButton.enabled = NO;
        self.authorizationStatusDeniedView.hidden = YES;
    }
    
}

-(void)showErrorAccessPhoto {
    
    UIAlertAction *cancelAction =
    [UIAlertAction actionWithTitle:@"Отмена"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *_Nonnull action) {
                               
                               _authorizationStatusDeniedView.hidden = NO;
                           }];
    
    UIAlertAction *setUpAction = [UIAlertAction
                                  actionWithTitle:@"Настройки"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *_Nonnull action) {
                                      
                                      NSURL *url =
                                      [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                      
                                      if ([[UIApplication sharedApplication] canOpenURL:url]) {
                                          
                                          NSURL *url = [NSURL
                                                        URLWithString:UIApplicationOpenSettingsURLString];
                                          
                                          [[UIApplication sharedApplication] openURL:url];
                                      }
                                  }];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:
                                          @"Нет доступа к фото"
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:setUpAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}


#pragma mark - Photo Library.

- (void)initAlbumAssetsLibrary {
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        
        PHFetchResult *smartAlbumsResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
        
        PHFetchResult *userAlbumsResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
        
        
        NSArray * fetchResultArray = @[smartAlbumsResult, userAlbumsResult];
        
        
        
        // Filter albums
        NSArray *assetCollectionSubtypes = [self filterContentWithType];
        
        NSMutableDictionary *smartAlbums = [NSMutableDictionary dictionaryWithCapacity:assetCollectionSubtypes.count];
        
        NSMutableArray *userAlbums = [NSMutableArray array];
        
       
        for (PHFetchResult *fetchResult in fetchResultArray) {
            
            [fetchResult enumerateObjectsUsingBlock:^(PHAssetCollection *assetCollection, NSUInteger index, BOOL *stop) {
                
                PHAssetCollectionSubtype subtype = assetCollection.assetCollectionSubtype;
                
               
                if (subtype == PHAssetCollectionSubtypeAlbumRegular && assetCollection.estimatedAssetCount > 0) {
                    
                        
                        [userAlbums addObject:assetCollection];
                    
                } else if ([assetCollectionSubtypes containsObject:@(subtype)]) {
                    
                    PHFetchResult *fetchResultSmartALbums =
                    [PHAsset fetchAssetsInAssetCollection:assetCollection
                                                  options:nil];
                    if(fetchResultSmartALbums.count > 0) {
                        
                        if (!smartAlbums[@(subtype)]) {
                            
                            smartAlbums[@(subtype)] = [NSMutableArray array];
                        }
                        
                        [smartAlbums[@(subtype)] addObject:assetCollection];
                    }
                }
            }];
        }
        
        NSMutableArray *assetCollections = [NSMutableArray array];
        
        // Fetch smart albums
        for (NSNumber *assetCollectionSubtype in assetCollectionSubtypes) {
            
            NSArray *collections = smartAlbums[assetCollectionSubtype];
            
            if (collections.count > 0) {
                
                [assetCollections addObjectsFromArray:collections];
            }
        }
        
        // Fetch user albums
        [userAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *assetCollection, NSUInteger index, BOOL *stop) {
            
            
            [assetCollections addObject:assetCollection];
            
        }];
        
        self.assetsGroupArray = assetCollections;
        
        [self initAssetArray];
        
    });
}


//Create PHFetchOptions with picker type
- (PHFetchOptions *)fetchOptions
{
    PHFetchOptions *fetchOptions = [PHFetchOptions new];
    
    fetchOptions.sortDescriptors = @[
                                     [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]
                                     ];
    
    switch (self.type) {
            
        case kAllPickerType:
            fetchOptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d || mediaType = %d", PHAssetMediaTypeVideo, PHAssetMediaTypeImage];
            break;
            
        case kPhotoPickerType:
            fetchOptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d", PHAssetMediaTypeImage];
            break;
            
        case kVideoPickerType:
            fetchOptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d", PHAssetMediaTypeVideo];
            break;
            
        case kDocumentPickerType:
            fetchOptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d", PHAssetMediaTypeImage];
            break;
            
        default:
            break;
    }
    
    return fetchOptions;
}

- (void)initAssetArray {
    
    PHFetchOptions *options = [self fetchOptions];
    if(_assetsGroupArray.count){
    _currentFetchResult = [PHAsset
                           fetchAssetsInAssetCollection:_assetsGroupArray[_currentAssetsIndex]
                           options:options];
    
    [self checkStatusSourceCameraTypeAvailable];
    }
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        //Run UI Updates
        [self.collectionView reloadData];
        
    });
    
}

-(NSArray*)filterContentWithType {
    
    NSArray *assetCollectionSubtypes ;
    
    switch (self.type) {
            
        case kAllPickerType:
            assetCollectionSubtypes =
            @[
              @(PHAssetCollectionSubtypeSmartAlbumUserLibrary),
              @(PHAssetCollectionSubtypeAlbumMyPhotoStream),
              @(PHAssetCollectionSubtypeSmartAlbumSelfPortraits),
              @(PHAssetCollectionSubtypeSmartAlbumPanoramas),
              @(PHAssetCollectionSubtypeSmartAlbumScreenshots),
              @(PHAssetCollectionSubtypeSmartAlbumRecentlyAdded),
              @(PHAssetCollectionSubtypeSmartAlbumVideos)
              ];
            
            break;
            
        case kDocumentPickerType:
            assetCollectionSubtypes = @[
                                        @(PHAssetCollectionSubtypeSmartAlbumUserLibrary),
                                        @(PHAssetCollectionSubtypeAlbumMyPhotoStream),
                                        @(PHAssetCollectionSubtypeSmartAlbumSelfPortraits),
                                        @(PHAssetCollectionSubtypeSmartAlbumPanoramas),
                                        @(PHAssetCollectionSubtypeSmartAlbumScreenshots),
                                        @(PHAssetCollectionSubtypeSmartAlbumRecentlyAdded)
                                        ];
            
            break;
            
        case kPhotoPickerType:
            assetCollectionSubtypes = @[
                                        @(PHAssetCollectionSubtypeSmartAlbumUserLibrary),
                                        @(PHAssetCollectionSubtypeAlbumMyPhotoStream),
                                        @(PHAssetCollectionSubtypeSmartAlbumSelfPortraits),
                                        @(PHAssetCollectionSubtypeSmartAlbumPanoramas),
                                        @(PHAssetCollectionSubtypeSmartAlbumScreenshots),
                                        @(PHAssetCollectionSubtypeSmartAlbumRecentlyAdded)
                                        ];
            break;
            
        case kVideoPickerType:
            assetCollectionSubtypes = @[
                                        @(PHAssetCollectionSubtypeSmartAlbumVideos)
                                        ];
            break;
            
        default:
            break;
    }
    return assetCollectionSubtypes;
}

#pragma mark - Action

- (IBAction)cancelButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneButtonAction:(id)sender {
    
    [self.imagePickerDelegate ycPhotoPicker:self
        didFinishPickingMediaWithImageArray:self.seletedImageArray];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


//set navigation title
-(void)setNavigationTitle {
    
    if (_assetsGroupArray.count) {
        
        [_navigationBarTitleView.navigationBarTitleButton
         setTitle:NSLocalizedString(
                                    [_assetsGroupArray[_currentAssetsIndex] localizedTitle],
                                    nil)
         forState:UIControlStateNormal];
    }
    
}

//проверяем статус доступности камеры
-(void)checkStatusSourceCameraTypeAvailable {
    
    if (_currentAssetsIndex == 0 &&
        [UIImagePickerController
         isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] &&
        _assetsGroupArray.count) {
        
        self.isCameraAvailable = YES;
        return;
    }
    
    self.isCameraAvailable = NO;
}


#pragma mark  - Get YCAssetsModel
//получение модели
-(void)getAssetInDictionaryWithIndexPath:(NSInteger)indexPathRow withBlock:(void (^)(YCAssetsModel* blockModel))block { //получение модели для ячейки
    
    NSString* assetsKeyAtIndexPathRow = [NSString
                                         stringWithFormat:@"%@",
                                         [(PHAsset *)_currentFetchResult[indexPathRow]
                                          localIdentifier]];
    if (![_assetsDic
          objectForKey:assetsKeyAtIndexPathRow]) {
        
        
        PHAsset* asset = _currentFetchResult[indexPathRow];
        
        
        if (asset.mediaType == PHAssetMediaTypeVideo) { //VIDEO
            
            [self createVideoAssetModelObjectWith:asset andAssetKeyAtIndexPathRow:assetsKeyAtIndexPathRow completionHandler:^(YCAssetsModel *complitionAsset) {
                
                if(complitionAsset != nil) {
                
                    block(complitionAsset);
                
                } else {
                    
                    block(nil);
                }
                
            }];
            
        }
        
        else if(asset.mediaType == PHAssetMediaTypeImage ) {// IMAGE or DOCUMENT
            
            [self createPhotoAssetModelObjectWith:asset andAssetKeyAtIndexPathRow:assetsKeyAtIndexPathRow completionHandler:^(YCAssetsModel *complitionAsset) {
                
                if(complitionAsset != nil) {
                    
                    block(complitionAsset);
                    
                } else {
                    
                    block(nil);
                }

            }];
            
        }
        
    } else {
        
        YCAssetsModel* assetsModel = (YCAssetsModel *)[_assetsDic
                                                       objectForKey:assetsKeyAtIndexPathRow];
        block(assetsModel);
    }
}


-(void)createPhotoAssetModelObjectWith:(PHAsset*)asset andAssetKeyAtIndexPathRow:(NSString*)assetsKeyAtIndexPathRow completionHandler:(void(^)(YCAssetsModel* complitionAsset))completionHandler {
    
    YCAssetsModel* assetsModel = [[YCAssetsModel alloc] init];
    
    assetsModel.mediaType = self.type != kDocumentPickerType ? kPhotoPickerType : kDocumentPickerType;
    
    @autoreleasepool {
        
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc]init];
        
        options.deliveryMode =  PHImageRequestOptionsDeliveryModeHighQualityFormat;
        options.synchronous = YES;
        options.resizeMode = PHImageRequestOptionsResizeModeFast;
        
        [_imageManager
         requestImageForAsset:asset
         targetSize:CGSizeMake(130, 130)
         contentMode:PHImageContentModeAspectFill
         options:options
         resultHandler:^(UIImage *result, NSDictionary *info) {
             
             if (result) {
                 
                 assetsModel.image = result;
                 
                 assetsModel.selectedStatus = NO;
                 
                 [_assetsDic
                  setObject:assetsModel
                  forKey:assetsKeyAtIndexPathRow];
                 completionHandler(assetsModel);
             }
             
         }];
    }
}


-(void)createVideoAssetModelObjectWith:(PHAsset*)asset andAssetKeyAtIndexPathRow:(NSString*)assetsKeyAtIndexPathRow completionHandler:(void(^)(YCAssetsModel* complitionAsset))completionHandler {
    
    YCAssetsModel* assetsModel = [[YCAssetsModel alloc] init];
    
    assetsModel.mediaType = kVideoPickerType;
    assetsModel.duration = asset.duration;
    
    @autoreleasepool {
        
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc]init];
        
        options.deliveryMode =  PHImageRequestOptionsDeliveryModeHighQualityFormat;
        options.synchronous = YES;
        options.resizeMode = PHImageRequestOptionsResizeModeFast;
        
        [_imageManager
         requestImageForAsset:asset
         targetSize:CGSizeMake(130, 130)
         contentMode:PHImageContentModeAspectFill
         options:options
         resultHandler:^(UIImage *result, NSDictionary *info) {
             
             if (result) {
                 
                 assetsModel.image = result;
                 
                 assetsModel.selectedStatus = NO;
                 
                 assetsModel.imageAsset = asset;
                 
                 [_assetsDic
                  setObject:assetsModel
                  forKey:assetsKeyAtIndexPathRow];
                 completionHandler(assetsModel);
             }
             
         }];
    }

    
    PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
   // options.networkAccessAllowed = YES;
    options.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    
//  //  @autoreleasepool {
//    [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:options resultHandler:^(AVAsset *assetAV, AVAudioMix *audioMix, NSDictionary *info) {
//        
//        if ([assetAV isKindOfClass: [AVURLAsset class]]) {
//            
//                            NSURL *url = [(AVURLAsset*)assetAV URL];
//            
//                        assetsModel.url = url;
//                         assetsModel.selectedStatus = NO;
//            
//            Ъ

//
//        AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:assetAV];
//        
//        gen.appliesPreferredTrackTransform = YES;
//        CMTime time = CMTimeMakeWithSeconds(0.0, 600);
//        NSError *error = nil;
//        CMTime actualTime;
//        
//        CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
//        UIImage *thumb = [[UIImage alloc] initWithCGImage:image];
//        CGImageRelease(image);
//        
//        assetsModel.videoAsset = assetAV;
//        assetsModel.image = thumb;
//
//        
//        
//        if(([asset isKindOfClass:[AVComposition class]] && ((AVComposition *)asset).tracks.count == 2)){
//            //slow motion videos. See Here: https://overflow.buffer.com/2016/02/29/slow-motion-video-ios/
//            
//            //Output URL of the slow motion file.
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            NSString *documentsDirectory = paths.firstObject;
//           
//            NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"mergeSlowMoVideo-%d.mov",arc4random() % 1000]];
//           
//            NSURL *url = [NSURL fileURLWithPath:myPathDocs];
//            
//            //Begin slow mo video export
//            AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:assetAV presetName:AVAssetExportPresetHighestQuality];
//            exporter.outputURL = url;
//            exporter.outputFileType = AVFileTypeQuickTimeMovie;
//            exporter.shouldOptimizeForNetworkUse = YES;
//            
//            [exporter exportAsynchronouslyWithCompletionHandler:^{
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    if (exporter.status == AVAssetExportSessionStatusCompleted) {
//                        NSURL *URL = exporter.outputURL;
//                     
//                        assetsModel.url = URL;//avAsset.URL;//urlAsset.URL;
//                        assetsModel.selectedStatus = NO;
//                        
//                        
//                        [_assetsDic
//                         setObject:assetsModel
//                         forKey:assetsKeyAtIndexPathRow];
//                        
//                        completionHandler(assetsModel);
//                        
//                    }
//                });
//            }];
//        }
//        
//         else if ([assetAV isKindOfClass: [AVURLAsset class]]) {
//        
//                NSURL *url = [(AVURLAsset*)assetAV URL];
//
//            assetsModel.url = url;
//             assetsModel.selectedStatus = NO;
//        
//        
//        [_assetsDic
//         setObject:assetsModel
//         forKey:assetsKeyAtIndexPathRow];
//        
//        completionHandler(assetsModel);
//    } else {
//        
//        completionHandler(nil);
//    }
//     
//    }];
}

#pragma mark - UICollectionViewDelegate.

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    
    [self setNavigationTitle];
    
    if (self.isCameraAvailable &&
        _assetsGroupArray.count) {
        
        NSInteger count = _currentFetchResult.count;
        
        return count + 1;
    }
    return _currentFetchResult.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.isCameraAvailable && indexPath.row == 0) { //если доступна камера показываем ячейку камеры
        
        UICollectionViewCell *cell = [collectionView
                                      dequeueReusableCellWithReuseIdentifier:kCameraCellId
                                      forIndexPath:indexPath];
        
        return cell;
    }
    
    //если камера доступна то вычитаем из indexPath.row единицу
    NSInteger indexPathRow = (_currentAssetsIndex == 0 && self.isCameraAvailable)
    ? indexPath.row - 1
    : indexPath.row;
    
    YCAssetsCollectionViewCell *cell = [collectionView
                                        dequeueReusableCellWithReuseIdentifier:kPhotoCellId
                                        forIndexPath:indexPath];
    
    [self getAssetInDictionaryWithIndexPath:indexPathRow withBlock:^(YCAssetsModel * assetsModel) {
        
        if(assetsModel) {
            
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [cell configureWithModel: assetsModel];
            
            cell.cellDelegate = self;
            
            [self configSelectButtonBlockWith:cell indexPathRow:indexPathRow andAssetModel:assetsModel];
        });
            
        }
    }];
    
    return cell;
}

-(void)configSelectButtonBlockWith:(YCAssetsCollectionViewCell*)cell indexPathRow:(NSInteger)indexPathRow andAssetModel:(YCAssetsModel*)assetsModel{
    
    __weak typeof(self.seletedImageArray) seletedImageArrayWeak = self.seletedImageArray;
    
    
    //Select button block
    [cell buttonSelectBlock:^(BOOL seletedStatus) {
        
        assetsModel.selectedStatus = seletedStatus;
        
        assetsModel.imageAsset = _currentFetchResult[indexPathRow];
        
        if (assetsModel.selectedStatus) {
            
            [seletedImageArrayWeak addObject:assetsModel];
            
            self.doneButton.enabled = YES;
            
        } else {
            
            [seletedImageArrayWeak removeObject:assetsModel];
            
            if (!seletedImageArrayWeak.count) {
                
                self.doneButton.enabled = NO;
            }
        }
        
        self.doneButton.badgeValue = [NSString
                                      stringWithFormat:@"%lu", (unsigned long)seletedImageArrayWeak.count];
        
        [_assetsDic
         setObject:assetsModel
         forKey:[NSString
                 stringWithFormat:@"%@",
                 [(PHAsset *)
                  _currentFetchResult[indexPathRow]
                  localIdentifier]]];
        
    }];
    
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isCameraAvailable && indexPath.row == 0) {
        
        if (self.cameraController != nil) {
            
            id delegate;
            if ([self.cameraController valueForKey:@"delegate"]) {
                
                delegate = [self.cameraController valueForKey:@"delegate"];
            }
            
            NSData *tempArchiveViewController =
            [NSKeyedArchiver archivedDataWithRootObject:self.cameraController];
            
            UIViewController *cameraVC =
            [NSKeyedUnarchiver unarchiveObjectWithData:tempArchiveViewController];
            
            if (delegate && delegate != nil) {
                
                [cameraVC setValue:delegate forKey:@"delegate"];
            }
            
            [self presentViewController:cameraVC animated:YES completion:NULL];
            
        } else {
            
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            picker.mediaTypes = [self returnMediaTypesArray];
            [self presentViewController:picker animated:YES completion:NULL];
        }
    }
}



- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((([UIScreen mainScreen].bounds.size.width - (countItemsInLine* itemMinSpasing)) /
                       self.collectionCellWidthCompareToScreen),
                      (([UIScreen mainScreen].bounds.size.width-(countItemsInLine* itemMinSpasing)) /
                       self.collectionCellWidthCompareToScreen));

}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                               duration:(NSTimeInterval)duration {
    [self getItemsCountOfLine];
    [self.collectionView.collectionViewLayout invalidateLayout];
}

-(NSArray*)returnMediaTypesArray {
    
    NSArray * arrayMediaType;
    switch (self.type) {
            
        case kAllPickerType:
            arrayMediaType = @[(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage];
            break;
            
        case kDocumentPickerType:
            arrayMediaType = @[(NSString *)kUTTypeImage];
            break;
            
        case kPhotoPickerType:
            arrayMediaType = @[(NSString *)kUTTypeImage];
            break;
        case kVideoPickerType:
            arrayMediaType = @[(NSString *)kUTTypeMovie];
            break;
            
        default:
            break;
    }
    return arrayMediaType;
}

#pragma mark -  AssetsCollectionViewCellDelegate;

- (void)setSelectedItemWithCell:(YCAssetsCollectionViewCell *)cell {
    
    [cell changeReverseSelectedStatus: cell.selectButton.selected ? NO : YES];
    
    if (self.seletedImageArray.count > self.maxNumberOfSelections) { // если колиество уже выбранных превышает макс
        
        [cell changeReverseSelectedStatus:NO];
        
        [self.doneButton shakeBageAnimation];
        
        return;
    }
}

#pragma mark - AlbumsViewControllerDelegate

- (void)albumsViewControllerSetIndexAlbum:(NSInteger)index {
    
    if (_currentAssetsIndex != index) {
        
        _currentAssetsIndex = index;
        
        [self initAssetArray];
        
        [_navigationBarTitleView navigationBarTitleButtonAction:nil];
        
        if (!self.seletedImageArray.count) {
            
            self.doneButton.enabled = NO;
        }
        
        [self.collectionView reloadData];
        
    } else {
        
        [_navigationBarTitleView navigationBarTitleButtonAction:nil];
    }
}

#pragma mark - UIImagePickerControllerDelegate
//этот метод вызывается для фотокамеры
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
    
    __block PHAssetChangeRequest *_mChangeRequest = nil;
    __weak __typeof(self) weakSelf = self;
    //добавляем фото или видео в Photo Library
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        //Фото
        if([[info valueForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.image"]) {
            
            _mChangeRequest = [PHAssetChangeRequest
                               creationRequestForAssetFromImage:
                               [info valueForKey:UIImagePickerControllerOriginalImage]];
        }
        //Видео
        else if([[info valueForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"]) {
            
            _mChangeRequest = [PHAssetChangeRequest
                               creationRequestForAssetFromVideoAtFileURL:
                               [info valueForKey:UIImagePickerControllerMediaURL]];
            
        }
    }
                                      completionHandler:^(BOOL success, NSError *error) {
                                          
                                          if (success) {
                                              //выбираем добавленную фотографию
                                              [weakSelf selectFirstAssetFromCameraCapturing];
                                          }
                                      }];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)selectFirstAssetFromCameraCapturing {
    
    [self initAssetArray];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //выбираем первую фотографию
        NSIndexPath *indexPath =
        [NSIndexPath indexPathForItem:1 inSection:0];
        
        YCAssetsCollectionViewCell *cell = (YCAssetsCollectionViewCell *)[self
                                                                          collectionView:self.collectionView
                                                                          cellForItemAtIndexPath:indexPath];
        
        double delayInSeconds = 0.5f;
        
        dispatch_time_t popTime =
        dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            
            if (self.seletedImageArray.count < self.maxNumberOfSelections) {
                
                [cell selectButtonAction:cell];
                
                [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
                
            } else {
                
                [self.doneButton shakeBageAnimation];
            }
        });
    });
    
}
@end
