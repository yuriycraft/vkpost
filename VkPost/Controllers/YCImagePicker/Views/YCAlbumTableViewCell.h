//
//  YCAlbumTableViewCell.h
//  VkPost
//
//  Created by book on 24.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

@interface YCAlbumTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkMarkImageView;

//configure cell
- (void)configureCellDataWithAssetCollection:(PHAssetCollection *)collection;

@end
