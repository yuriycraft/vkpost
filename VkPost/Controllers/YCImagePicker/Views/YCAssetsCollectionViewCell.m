//
//  YCAssetsCollectionViewCell.m
//  VkPost
//
//  Created by book on 25.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "YCAssetsCollectionViewCell.h"
static NSUInteger videoType = 3;

@interface YCAssetsCollectionViewCell ()


@end

@implementation YCAssetsCollectionViewCell

- (void)configureWithModel:(YCAssetsModel *)model {

  self.assetsImageView.image = model.image;

  self.selectedStatus = model.selectedStatus;
    
    if(model.mediaType == videoType) {
        
    NSDate* d = [NSDate dateWithTimeIntervalSince1970:model.duration];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [dateFormatter setDateFormat:@"mm:ss"];
    
    self.timeLabel.text = [dateFormatter stringFromDate:d];
        self.timeLabel.hidden = NO;
        
    } else {
        
        self.timeLabel.hidden = YES;
  
    }
}



- (void)setSelectedStatus:(BOOL)selectStatus {
    
    [_selectButton setSelected:selectStatus ? YES : NO];
    
    self.assetsImageView.alpha = selectStatus ? 0.9f : 1.f;
    
    [self animateButtonOnSelect:_selectButton];
    
    
}

- (IBAction)selectButtonAction:(id)sender {
    
    [self.cellDelegate setSelectedItemWithCell:self];
}



@end
