//
//  YCAssetsCollectionViewCell.h
//  VkPost
//
//  Created by book on 25.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "YCAssetsModel.h"
#import <UIKit/UIKit.h>
#import "YCSelectedCollectionViewCell.h"


@interface YCAssetsCollectionViewCell : YCSelectedCollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *assetsImageView;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (assign, nonatomic) BOOL isMaxItem;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

- (IBAction)selectButtonAction:(id)sender;
- (void)configureWithModel:(YCAssetsModel *)model;

@end


