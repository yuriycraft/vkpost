//
//  YCSelectedCollectionViewCell.h
//  VkPost
//
//  Created by book on 08.12.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YCPhotoCollectionViewCellDelegate <NSObject>

-(void)setSelectedItemWithCell:(UICollectionViewCell*)cell;

@end


typedef void (^YCPhotoCollectionViewCellSelecteBlock)(BOOL selectedStatus);

@interface YCSelectedCollectionViewCell : UICollectionViewCell

@property(nonatomic, weak, readwrite) id <YCPhotoCollectionViewCellDelegate> cellDelegate;

@property (nonatomic, assign, readwrite) BOOL selectedStatus;

@property(assign,nonatomic) CGSize imageSize;

@property(nonatomic, copy, readwrite) YCPhotoCollectionViewCellSelecteBlock selectBlock;

- (void)buttonSelectBlock:(void (^)(BOOL selectedStatus))selectedBlock;

-(void)changeReverseSelectedStatus:(BOOL)selected;

-(void)animateButtonOnSelect:(UIButton*)button;
@end
