//
//  YCAssetsModel.m
//  VkPost
//
//  Created by book on 25.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "YCAssetsModel.h"

@implementation YCAssetsModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.selectedStatus = NO;
    }
    return self;
}

- (void) updateElapsedTimeDisplay: (NSTimeInterval )tim {
    
    // You could also have stored the start time using
    // CFAbsoluteTimeGetCurrent()
 //   NSTimeInterval elapsedTime = [startDate timeIntervalSinceNow];
    
    // Divide the interval by 3600 and keep the quotient and remainder
    div_t h = div(tim, 3600);
    int hours = h.quot;
    // Divide the remainder by 60; the quotient is minutes, the remainder
    // is seconds.
    div_t m = div(h.rem, 60);
    int minutes = m.quot;
    int seconds = m.rem;
    
    // If you want to get the individual digits of the units, use div again
    // with a divisor of 10.
    
    NSLog(@"%d:%d:%d", hours, minutes, seconds);
}
@end
