//
//  YCAssetsModel.h
//  VkPost
//
//  Created by book on 25.10.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>
#import <UIKit/UIKit.h>

@interface YCAssetsModel : NSObject

@property(nonatomic, assign, readwrite) NSUInteger mediaType;
@property(nonatomic, strong, readwrite) UIImage *image;
@property(nonatomic, assign, readwrite) BOOL selectedStatus;
@property(nonatomic, assign, readwrite) NSInteger index;
@property(nonatomic, strong, readwrite) PHAsset *imageAsset;
@property(nonatomic, strong, readwrite) AVAsset *videoAsset;
@property (nonatomic, strong ,readwrite) NSString *filename;
@property (nonatomic, assign, readwrite) NSTimeInterval duration;
@property (nonatomic, strong, readwrite) NSURL * url;
//@property (nonatomic, strong, readwrite) NSString * extention;
@property (nonatomic, strong, readwrite) NSData * videoData;
@end
